# ODO

## The Serverless Java Framework

### What is it?

With the rise of Serverless environments, a new set of constraints are placed on the authors of Serverless functions
which are not necessarily present in the traditional server based applications.

These constraints are typically the following:

* Execution time is billed, speed is therefore a constraint which has an impact;
* Memory consumption is billed, it is important that your function uses as little as possible;
* Serverless environments put constraints on binary size both on your function binaries and it's dependencies;
* Cold start is an issue, especially in Java environments where the JVM needs time to initialize;
* Actual execution of the function may be throttled with virtual CPU time, therefore any initialization code which still
  needs to execute may run very slowly;
* Serverless functions are typically not portable across different cloud providers.

### How does ODO solve these problems?

1. It is designed to startup in the initialization phase of your function, therefore initializes in full CPU time;
2. It is designed around best practices for development of functions to optimize for cold start;
3. It's complete libraries with all dependencies is _< 256Kb_;
4. It eschews reflection and utilises a pure POJO programming model;
5. It minimizes dependencies by utilising the features available in the modern JVM;
6. It aims to be portable and is designed to write functions that are portable across Cloud providers and also run in
   traditional servlet environments;
7. It still provides developer comforts such as a simple IoC container.

### Documentation

* [Getting Started](doc/getting_started.md)
* [Concepts](doc/concepts.md)
* [IoC Container](doc/ioc_container.md)
* [Creating Services](doc/service_functions.md)
* [AWS Lambda Support](doc/aws_lambda.md)
* [Jetty Support](doc/jetty.md)
* [Nano or MicroService, ODO's HTTP Router](doc/http_router.md)
