# ODO with Jetty

[Home](../index.md)

ODO can also work with Jetty as a Servlet Container.

## Deploying ODO applications with Jetty

1. Include with ODO's Jetty Runtime with the application as a dependency;
2. Package the application;
3. Configure the _**ODO_PROFILE**_ environment variable or pass _**odo.profile**_ as a sytem variable to the desired
   configuration environment;
4. Run _**JettyServer**_ from the command line with the application jar in the classpath.

### Packaging the ODO application for Jetty

In order to utilise an ODO application with Jetty it is required that the ODO's Jetty runtime dependency in packaged
into thee _deployment zip file_. This can be done by including the dependency in your application. For example in
the **_maven pom.xml_**, the following can be included:

```
<dependency>
    <groupId>org.odoframework.</groupId>
    <artifactId>odo-jetty-runtime</artifactId>
</dependency>
```
