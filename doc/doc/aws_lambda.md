# ODO with AWS Lambda

[Home](../index.md)

ODO comes with first class for support for AWS Lambda and, many best practices have been incorporated into the design.

Primarily ODO will enable the following features when running in AWS Lambda:

* Static based initialization of the container and dependencies;
* Small deployment footprint in terms of image size;
* Integration with AWS logging;
* Integration with AWS-XRay.

## Deploying ODO applications to Lambda

1. Package the ODO application together with ODO's Lambda Runtime;
2. Set the Lambda handler to _**org.odoframework.awslambda.RequestHandler**_;
3. Configure the _**ODO_ENV**_ environment variable to the desired configuration environment.

### Packaging the ODO application for AWS Lambda

In order to utilise an ODO application with AWS Lambda, it is required that the ODO's Lambda runtime dependency is
packaged in the _deployment zip file_. This can be done by including the dependency in your application. For example in
the **_maven pom.xml_**, the following can be included:

```xml
<dependency>
    <groupId>org.odoframework.</groupId>
    <artifactId>odo-aws-lambda-runtime</artifactId>
</dependency>
```

