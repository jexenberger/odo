# ODO IoC Container

[Home](../index.md)

[comment]: <> (Consider putting this on the main page. It makes it clear from the get-go what ODO is and does. )
ODO's Inversion of Control or IoC Container is an extremely light-weight framework that provides a Dependency Injection
framework which piggy-backs off the
standard [Java Service Loader](https://docs.oracle.com/javase/9/docs/api/java/util/ServiceLoader.html) to allow dynamic
loading of components.

ODO works by creating instances of components via a _**BeanDefinition**_. Bean definitions define how components are
wired together. A Bean definition has an identity and each definition forms part of an instance of a _**Container**_
instance which instantiates and manages instances of components defined by Bean Defintions.

## Creating Bean Definitions and a Container.

Bean Definitions define how an instance of a component should be constructed and wired together. This bean definition is
then registered into a Container with a unique ID. Instances of a component can then be requested from the container via
the unique ID.

There are the following parts which make up a bean definition namely:

* The unique ID which identifies a bean definition. An ID can be any unique string, however ODO does provide the ability
  to use Class names as object IDs for simplicity. Behind the scenes the Class name used is just converting to string
  using the _Class.getName()_ method;
* The constructor used to create instances of components defined by the Bean definition;
* The values which need to be injected into the component.

### Simple Bean Definitions

The following provides an instance of a String with an ID of _'hello_world'_

```java
    var container = new Container(new Properties());
    BeanDefinition beanDefinition=bean("hello_world").with(()->"Hello world");
    container.register(beanDefinition);
    var instance=container.get("hello_world").orElseThrow();
    assertEquals("Hello world",instance);
```

This code works as follows:

1. A new instance of _**Container**_ is created. The constructor takes an instance of _**java.util.Properties**_ which
   contains configuration properties referenced by components;
2. An instance of _**org.odoframework.container.injection.BeanDefintion**_  instance is created using the static
   method _bean()_ on BeanDefinition. The passed parameter is the ID of the instance, in this case 'hello_world', which
   returns an instance of BeanDefinition. A constructor is then registered for the object using the _with()_ method. In
   this case, a Lambda functions is simply passed, which creates a String containing "Hello world";
3. Now the BeanDefintion must be registered with the container, using the Container's _register()_ method;
4. An instance of 'hello_world' can now be created, by simply calling the Container instance _get()_ method which
   returns a _**java.util.Optional instance**_ if a Bean with that ID is created [comment]: <> (... ID exisits?).

### Injecting other Components and Configuration Properties

This example by itself is not very useful. In practice, more complex components which have one or more collaborators,
would be required.

Suppose there is a Class: Foo. Foo is dependent on Class Bar as a collaborator. Furthermore, Foo needs a configurable
description field. The following shows what class Foo looks like:

```java
public class Foo {

    private final String title;
    private Bar bar;

    public Foo(String title) {
        this.title = title;
    }

    public void setBar(Bar bar) {
        this.bar = bar;
    }

    public void doStuff() {
        System.out.println(title + " is doing stuff with Bar");
        bar.doStuff();
    }
}
```

Foo takes a title as a constructor and has a set method for an instance of Bar. Start by defining the Bar dependency in
the container.

```java
var beanDefinition=BeanDefinition
    .bean(Bar.class)
    .with(()->new Bar());
```

Here the class name, as an ID, is used. New instances are provided by simply calling the default constructor.

Next, a title must be created for the Foo class. In this case, it must be referenced as a configuration property so it
can be declared as an instance of _java.util.Properties_ with a configured title:

```java
    var props=new Properties();
    props.setProperty("foo.title","My cool Foo component");
```

A definition for the Foo component can now be created:

```java
var fooComponent=BeanDefinition
    .bean(Foo.class)
    .with(refs-> new Foo(refs.value("foo.title")))
    .set(Bar.class,Bar.class,(foo,bar) -> foo.setBar(bar));
```

Here, a bean definition is created, with id as Foo.class. The constructor which needs the title is provided by called
the _**value()**_ method on the passed container reference in the Lambda used for the constructor. Finally, the _**
set()**_ method requires that the Bar property is populated on Foo with Bar.class, and to populate it calling _**
Foo.setBar()**_

Lastly, the container and reference Foo must be created.

```java
var container=new Container(props);
    container.register(barComponent);
    container.register(fooComponent);

    var fooInstance=container.get(Foo.class).orElseThrow();
    fooInstance.doStuff();

```

Breaking down the configuration in this manner is quite verbose. Using fluent builders and method references cleans-up
the process:

```java
var props=new Properties();
    props.setProperty("foo.title","My cool Foo component");
    var container=new Container(props);
    container
        .register(bean(Bar.class).with(Bar::new))
        .register(
            bean(Foo.class)
                .with(refs->new Foo(refs.value("foo.title")))
                .set(Bar.class,Foo::setBar)
        );
    var fooInstance=container.get(Foo.class).orElseThrow();
    fooInstance.doStuff();
```

## Creating Applications

Having an IoC container by itself it quite useful, however greater usefulness can be achieved when the container takes
care of some boilerplate code automatically.

Consider writing an AWS Lambda function: Ideally the framework should boostrap the actual function code and make it
simple to invoke the code.

Furthermore, the function should also be provided with the configuration and resources required to execute. ODO provides
the following additional features:

* A simple configuration framework which can handle multiple environments in the same configuration;
* Value added services such as a data sources;
* The ability to bootstrap code loaded from different ODO modules;
* Run functionality in the initialization phase of your function.

ODO provides this functionality by defining an Application.

Fundamentally an Application is little more than a class which extends _**ApplicationBuilder**_. This class however
provides a additional features:

* Load instances of ODO modules such as _**org.odoframework.container.container.ModuleBuilder**_ via
  the [Java Service Provider Interface](https://docs.oracle.com/javase/9/docs/api/java/util/ServiceLoader.html);
* Load configuration settings;
* Run components in the initialisation phase of your Serverless function;
* Helper methods to simplify the creation of Bean Definitions;
* An entry point for ODO to boostrap your Service Function.

### Creating an ApplicationBuilder class

Going back to the Foo Application, it can now be redefined as follows:

```java
public class FooBarApplication extends ApplicationBuilder {
    @Override
    public void build() {
        provide(Bar.class)
            .with(Bar::new);

        provide(Foo.class)
            .with(it -> new Foo(it.value("foo.title")))
            .set(Bar.class, Foo::setBar);
    }
}
```

In the example, abstract _**build()**_ is implemented as a method and used to define the Bean Definitions.
ApplicationBuilder also provides the
_**provide()**_ method, which enables a neat way to define instances of Bean Definitions and automatically registers the
Bean definition with the Application.

### Defining Configuration Properties

It should be noted that no properties object have been defined in the Application to set of the value of _**foo.title**_
. This is because these properties can be extracted into a configuration file.

The configuration for an ODO application works as follows:

1. ODO looks for a file on the classpath _**application.properties**_ which is a standard java properties file;
2. ODO will then look to see if a specific profile has been configured, and then look for another properties file
   called **application-_(name of profile)_.properties**;
3. Any properties loaded from an environment specific properties file will override values in _**
   application.properties**_.

For the example, a file for the _**dev**_ profile will be created. Thus, there will be a properties file on the
classpath called _**application-dev.properties**_ as shown below

```properties
foo.title=Test
```

To set the configuration profile to load, ODO needs to be passed this value. ODO accepts two mechanisms to pass this
value:

|Name|Type|Order of Resolution|
|----|----|-------------------|
|ODO_PROFILE|Environment variable|1|
|odo.profile|Java System property|2|

In the example below, the Java System property is used:

```java
public class ApplicationBuilderTest {

    @Test
    void testApplication() {

        System.setProperty("odo.profile", "dev");
        var application = new FooBarApplication();
        var container = application.getContainer();
        var fooBar = container.get(Foo.class).orElseThrow();
        fooBar.doStuff();

    }
}
```


