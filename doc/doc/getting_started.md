## The Obligatory "hello world" with ODO using AWS Lambda

[Home](../index.md)

### Step 1: Include the ODO dependencies

```xml
 <dependency>
     <groupId>net.odoframework</groupId>
     <artifactId>odo-aws-lambda-runtime</artifactId>
     <version>0.0.4</version>
 </dependency>
```

#### Step 2: Create an Application

```java
public class HelloWorldApplication extends ApplicationBuilder {
    @Override
    public void build() {
         ....
    }
}
```

#### Step 3: Implement the Function

```java
package org.odoframework.helloworld;

import Invocation;
import WebFunction;
import WebRequest;
import WebResponse;

public class HelloWorldService implements WebFunction {

    public WebResponse apply(WebRequest webRequest, Invocation invocation) {
        return ok().body(new StringBuilder(webRequest.getBody()).reverse().toString());
    }

}
```

#### Step 4: Wire Up the Service in the Application

```java
package org.odoframework.helloworld;

import ApplicationBuilder;
import ServiceFunction;

public class HelloWorldApplication extends ApplicationBuilder {
    @Override
    public void build() {
        provide(ServiceFunction.class).with(HelloWorldService::new);
    }
}
```

#### Step 5: Expose via ServiceLocator in META-INF/services/Module

```
org.odoframework.helloworld.HelloWorldApplication
```

#### Step 6: Upload to AWS Lamdda

![AWS Lambda](images/lambda_hello_world.png)

Please note:

* The Lambda Handler is to _**LambdaRequestHandler**_
* ODO requires Java 11 to run
* The uploaded package size with includes the framework and Hello world service is **< 400KB**

#### Step 7: Execute your function

In the example an API Gateway V2 proxy event is used (which is the default in ODO) setting the body to __'hello world'__
![Hello world](images/sample_test.png)

This returns the following result:

![result](images/hello_world_execution.png)

Note The execution time duration which is **< 750ms** from cold start!