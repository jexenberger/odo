## ODO Http Router

[Home](../index.md)

Once of the common complaints raised around using Lambda is deciding on the Granularity of the functions, especially
when it comes to building out API Endpoints.

The point of contention normally comes around doing a more complex endpoint.

For example a typical REST service for managing a single resources (such as Customer) would have several closely related
functions to read and update a resources.

These might typically be as follows:

### Customer Rest Service Endpoints

|Endpoint|HTTP method|Purpose|
|--------|-----------|-------|
|/customers|GET|Returns the collection of customers|
|/customers/{customer id}|GET|Returns a unique instance of a Customer|
|/customers|POST|Create a new instance of a Customer|
|/customers/{customer id}|PUT|Update a unique instance of a Customer|
|/customers/{customer id}|DELETE|Delete a unique instance of a Customer|

### The Nano-Service approach

The Nano-Service approach is the "purist" approach to Serverless development where a single operation of some kind maps
to a single function

If you take this approach to our Customer endpoins you will end up with 5 functions along the following lines:

|Endpoint|HTTP method|Function|
|--------|-----------|--------|
|/customers|GET|QueryCustomersFunction|
|/customers/{customer id}|GET|GetCustomerFunction|
|/customers|POST|CreateCustomerFunction|
|/customers/{customer id}|PUT|UpdateCustomerFunction|
|/customers/{customer id}|DELETE|DeleteCustomerFunction|

### The Micro-Service approach

The Micro-Service approach is effectively where one single function represents an entire Micro-Service, meaning that all
the endpoints would map to a single function.

|Endpoint|HTTP method|Function|
|--------|-----------|--------|
|/customers|GET|CustomerServiceFunction|
|/customers/{customer id}|GET|CustomerServiceFunction|
|/customers|POST|CustomerServiceFunction|
|/customers/{customer id}|PUT|CustomerServiceFunction|
|/customers/{customer id}|DELETE|CustomerServiceFunction|

While it's technically feasible to do this you would have to do pattern matching logic on your URL to accomplish this if
you wrote pure AWS Lambda functions for example.

### ODO approach

We aren't particularly interested in forcing you to do one or another, keeping in mind that both approaches have their
pros and cons.

**Our solution is to make is easy for you to pick either approach and give you the tools to do both.**

|Approach|Solution|
|--------|--------|
|Nano-Service|_**WebFunction**_|
|Nano-Service|_**HttpRouter**_|

### HttpRouter

While we have already covered a standard [Web Function](service_functions.md) we only made mention of ODO's HTTP Router.

The HttpRouter is a simple class which simplifies the process of doing routing logic against multiple URLS and HTTP
method. It follows patterns set by other frameworks to do this.

#### Our Customer Service using HTTP Router

At its core the HttpRouter just extends the standard WebFunction. To use this function all you have to do is extends _**
HttpRouter**_ and implement the _**build()**_ method.

In the build method you can then call any of the following for the appropriate http operation:

|HTTP Method|method|
|--------|--------|
|GET|get()|
|POST|post()|
|PUT|put()|
|DELETE|delete()|
|HEAD|head()|

All the methods take a Lambda with the same parameters as your standard _**WebFunction**_, namely a _**WebRequest**_
object and an _**Invocation**_ Object. Responses can then be returned by calling convenience methods on the _**
HttpRouter**_ instance

Variables in the url can then be extracted using the _**{variableName}**_ syntax

##### CustomerFunction implementing an HttpRouter

```java
public class CustomerFunction extends HttpRouter {


    private final CustomerService customerService;

    public CustomerFunction(CustomerService customerService, Json json) {
        super(json);
        this.customerService = customerService;
    }

    @Override
    public void build() {
        get("/customers", (builder, request) -> ok(customerService.getCustomers(request)));
        get("/customers/{id}", (request, invocation) ->
                request.getPathVariable("id")
                        .flatMap(customerService::getCustomer)
                        .map(this::ok)
                        .orElseGet(() -> notFound("User could not be found"))
        );
        post("/customers", (request, invocation) -> {
            final var customer = request.getBody(Customer.class);
            customerService.create(customer);
            return created("/customers/" + customer.getId()).body(getJson().marshal(customer));
        });
        put("/customers/{id}", (request, invocation) ->
             request
                    .getPathVariable("id")
                    .map(it -> {
                        final var customer = request.getBody(Customer.class);
                        customerService.update(customer);
                        return customer;
                    })
                    .map(this::ok)
                    .orElseGet(() -> notFound("Customer could not be found"))
        );
        delete("/customers/{id}", (request, invocation) ->
                request
                        .getPathVariable("id")
                        .map(it -> {
                            final var customer = request.getBody(Customer.class);
                            customerService.update(customer);
                            return customer;
                        })
                        .map(this::ok)
                        .orElseGet(() -> notFound("Customer could not be found"))
        );
    }


}
```


