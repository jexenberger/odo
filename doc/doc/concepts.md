# ODO Architecture and Concepts

## Overall Objectives

ODO is designed with the following objectives:

* Very fast startup time optimised for Serverless environments;
* Low reflection usage in order to optimise performance in Serverless environments;
* Simple IoC container for quick development;
* Opinionated use of libraries on order to reduce footprint of the overall framework both in size and memory;
* Portability across different Serverless providers as well as the ability to operate in a standard Java Servlet
  environment.

## Concepts

#### 1. Service Function

The service function represents the Serverless function which executes in your ODO application.

This function is any class which implements the _**ServiceFunction**_ interface.

#### 2. IoC Container

The ODO IoC container is a light-weight Dependency Injection container and service locator which is provided by ODO to
do simple wiring of components.

##### 2.1 Bean Definitions

An instance of the IoC container consists of Bean definitions, which is how components are defined inside the container.
Components are defined using a fluent builder pattern to wire together dependencies.

##### 2.2 Modules

Modules are logical unit of packaging for a set of components in the ODO container. Modules can be any class which
implements **_org.odoframework.container.Module_**. The task of a Module is to return a list of Bean Definitions which
defines all the components which make up the Module.

Modules are exposed to the ODO runtime using the
standard [Java Service Provider Interface](https://docs.oracle.com/javase/9/docs/api/java/util/ServiceLoader.html).

ODO supports [JPMS](https://en.wikipedia.org/wiki/Java_Platform_Module_System), therefore modules can be exposed via _
module-info_

##### 2.3 Applications

Applications are used to define the components that make up your Serverless function. Applications are a subtype of ODO
Modules but provide additional functionality:

* ODO starts up your Service function by looking for your an instance of _**ApplicationBuilder**_ from the service
  loader and uses this to create an instance of the IoC container and well as loading all the other modules.
* Your Application instance loads your configuration properties from the classpath and sets the ODO environment.

#### 3. Metrics

ODO provides an abstraction over an underlying metrics provider such as Prometheus or AWS Cloudwatch.

#### 4. Transactions

ODO provides a simple Transaction management that is an abstraction over a transactional resource or manager.

#### 5. Events

ODO provides a simple Eventing mechanism. 

## Architecture

[Home](../index.md)

![ODO Architecture](images/architecture.png)

[comment]: <> (Not a show stopper, but consider not starting each sentence with "This is...")
ODO's architecture consists of a set of Libraries which provide different aspects of functionality.

#### 1. Core Library

The lowest level library and provides a set of utilities which ODO (and the ODO user) can use.

#### 2. IoC Container

ODO's IoC container framework.

#### 3. Service

The library which contains abstractions to an underlying service runtime such as AWS Lambda or Servlets.

#### 4. Provider Runtime Library

Packaged with your Service Function code and integrates the ODO Service library with the underlying runtime provider.
This library is also responsible for Bootstrapping an ODO application into the environment.

#### 5. Metrics provider

Integrates ODO's core Metrics into an underlying metrics provider such as AWS X-Ray.

#### 6. User defined Function code

Your library which your Service Function.

