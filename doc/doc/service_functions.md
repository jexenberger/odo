# Creating Service Functions

[Home](../index.md)

## Concepts

### Service Function interface

A function in ODO is created by implementing the _**ServiceFunction**_ interface.

This function only has one method that needs to be implemented, the _**apply()**_ method. This method takes two
parameters:

|Parameter|Description|
|---------|-----------|
|T        |Data payload of the service|
|context  |An instance of **
Invocation**, this contains context information about the invocation from the [comment]: <> (Sentence not finished)

### InvocationContext

The invocation context is a light wrapper around the runtime constructs for the invocation. In a Servlet environment,
this might be a _**HttpServletRequest**_. In AWS lambda this would be the _**Context**_ instance for the invocation.

The invocation also has a generic ID property, which will either map the underlying invocation ID or generate a unique
one in the case of Servlets.

### Runtime

This is the runtime variable to the underlying FaaS environment of the cloud provider, such as AWS Lambda or Servlets.

### Request Converter and Response Converters.

Since a Service Function is generic, it requires conversion of the input type payload for the function, and conversion
of the function result back to the required runtime type. These converters are registered in the Application and
implement the following interfaces:

|Interface|Purpose|
|---------|-------|
|RequestConverter|Converts incoming request payloads into a target payload for the function|
|org.odoframework.service.ResponseConverters|Converts the response of the function into a target payload from the runtime|

Not all runtimes support converters. The Jetty runtime, which only supports Servlets, can only work with instances
of _**WebFunction**_ which convert _**
HttpServletRequest**_ instances into _**WebRequest**_ and _**WebResponse**_ instances for the function.

### Implementing Web Functions.

A very common use for Functions is to use the Function to create an endpoint for a function call via the Web. For
example, when running in Jetty, this call will be made via a servlet. In the case of AWS Lambda, it might be done
through an API Gateway event.

Also, the Jetty Runtime which only support Servlets is a dedicated Web environment.

To support this common requirement, ODO supports an extended version of the standard ServiceFunction in the form of
a _**WebFunction**_. This interface is a specialization of which takes a _**WebRequest**_ and return an instance of _**
WebResponse**_.

ODO comes with standard converters for each runtime for handling Web based functions. The default configuration for the
AWS Lambda functions assumes that an API Gateway event is handled. The API Gateway event is a web event.

### Http Router

The [_**HttpRouter**_](http_router.md) is an extensions of the standard Web Function which is used for handling complex
URN paths. This class allows for mapping of function calls to specific URI patterns.






