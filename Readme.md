# ODO

## The Serverless Java Framework

### What is it?

With the rise of Serverless environments, a new set of constraints are placed on the authors of Serverless functions
which are not necessarily present in the traditional server based applications.

These constraints are typically the following:

* Execution time is billed, speed is therefore a constraint which has an impact;
* Memory consumption is billed, it is important that your function uses as little as possible;
* Serverless environments put constraints on binary size both on your function binaries and it's dependencies;
* Cold start is an issue, especially in Java environments where the JVM needs time to initialize;
* Actual execution of the function may be throttled with virtual CPU time, therefore any initialization code which still
  needs to execute may run very slowly;
* Serverless functions are typically not portable across different cloud providers.

[comment]: <> (Consider not using the name in each line, eg 1. It is designed to startup...)

### How does ODO solve these problems?

1. ODO is designed to startup in the initialization phase of your function, therefore initializes in full CPU time;
2. ODO is designed around best practices for development of functions to optimize for cold start;
3. Complete ODO libraries with all dependencies is _< 256Kb_;
4. ODO eschews reflection and utilises a pure POJO programming model;
5. ODO minimizes dependencies by utilising the features available in the modern JVM;
6. ODO aims to be portable and is designed to write functions that are portable across Cloud providers and also run in
   traditional servlet environments;
7. ODO still provides developer comforts such as a simple IoC container.

### Documentation

* [Concepts](doc/doc/concepts.md)
* [IoC Container](doc/doc/ioc_container.md)
* [Creating Services](doc/doc/service_functions.md)
* [AWS Lambda Support](doc/doc/aws_lambda.md)
* [Jetty Support](doc/doc/jetty.md)

### The Obligatory "hello world"

#### Step 1: Create an Application

```java
public class HelloWorldApplication extends ApplicationBuilder {
    @Override
    public void build() {
         ....
    }
}
```

#### Step 2: Implement the Function

```java
package org.odoframework.helloworld;

import Invocation;
import WebFunction;
import WebRequest;
import WebResponse;

public class HelloWorldService implements WebFunction {

    public WebResponse apply(WebRequest webRequest, Invocation invocation) {
        return ok().body(new StringBuilder(webRequest.getBody()).reverse().toString());
    }

}
```

#### Step 3: Wire Up the Service in the Application

```java
package org.odoframework.helloworld;

import ApplicationBuilder;
import ServiceFunction;

public class HelloWorldApplication extends ApplicationBuilder {
    @Override
    public void build() {
        provide(ServiceFunction.class).with(HelloWorldService::new);
    }
}
```

#### Step 4: Expose via ServiceLocator in META-INF/services/Module

```
org.odoframework.helloworld.HelloWorldApplication
```

#### Step 5: Upload to AWS Lamdda

![AWS Lambda](doc/doc/images/lambda_hello_world.png)

Please note:

* The Lambda Handler is to _**LambdaRequestHandler**_
* ODO requires Java 11 to run
* The uploaded package size with includes the framework and Hello world service is **< 400KB**

#### Step 6: Execute your function

In the example an API Gateway V2 proxy event is used (which is the default in ODO) setting the body to __'hello world'__
![Hello world](doc/doc/images/sample_test.png)

This returns the following result:

![result](doc/doc/images/hello_world_execution.png)

Note The execution time duration which is **< 750ms** from cold start!
