import net.odoframework.annotations.FrameworkModule;
import net.odoframework.container.Module;
import net.odoframework.service.ServiceModule;
import net.odoframework.service.logging.Slf4jProvider;
import org.slf4j.spi.SLF4JServiceProvider;

@FrameworkModule
module odo.service {


    requires transitive odo.core;
    requires transitive odo.container;

    requires org.slf4j;

    exports net.odoframework.service;
    exports net.odoframework.service.web;
    exports net.odoframework.service.logging;

    opens net.odoframework.service;
    opens net.odoframework.service.web;
    opens net.odoframework.service.logging;

    provides Module with ServiceModule;
    provides SLF4JServiceProvider with Slf4jProvider;


}