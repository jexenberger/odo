package net.odoframework.service;


/**
 * A ProviderRuntime is the binding to the underlying Functional environment.
 * <p>
 * It provides the necessary implementations of interfaces which are needed to perform an invocation.
 * <p>
 * It is the responsibility of the Runtime {@link net.odoframework.container.Module} to provide an instance of this class
 *
 * @param <T> The raw context object used by the underlying functional environment, for example the Context object in the Lambda Request Handler
 */
public interface ProviderRuntime<T> {

    /**
     * The default class used by the runtime provider to convert incoming requests to a desired format
     *
     * @return the default Request Converter for the runtime, will throw {@link IllegalStateException} if not provided
     */
    RequestConverter<?, ?, T> getProviderDefaultRequestConverter();

    /**
     * The default class used by the runtime provider to convert responses to the format required by the Functional environment
     *
     * @return the default Response Converter for the runtime, will throw {@link IllegalStateException} if not provided
     */
    ResponseConverter<?, ?, T> getProviderDefaultResponseConverter();

    /**
     * The default exception handler used by the provider runtime to handle uncaught exceptions for the Functional Environment
     *
     * @return the default Exception handler, will throw {@link IllegalStateException} if not provided
     */
    ExceptionHandler<T, ?> getProviderDefaultExceptionHandler();

    /**
     * Creates an Invocation Context against the functional context create a holder for an instance of T as well as returning an ID for the request
     *
     * @param containerInstance the underlying instance of the T
     * @return an InvocationContext for the invocation, will throw {@link IllegalStateException} if not provided
     */
    InvocationContext<T> createInvocation(T containerInstance);



}
