package net.odoframework.service;


import net.odoframework.util.Decoder;

@FunctionalInterface
public interface RequestConverter<T, K, Z> extends Decoder<T, K, InvocationContext<Z>> {
}
