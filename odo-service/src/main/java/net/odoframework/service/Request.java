package net.odoframework.service;

public interface Request {
    String getBody();

    <T> T getBody(Class<T> type);

}
