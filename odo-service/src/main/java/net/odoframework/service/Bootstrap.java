package net.odoframework.service;


import net.odoframework.container.events.ContainerStartedEvent;
import net.odoframework.container.events.EventPublisher;
import net.odoframework.container.injection.Container;
import net.odoframework.container.metrics.Metrics;
import net.odoframework.container.tx.TxManager;
import net.odoframework.container.util.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.ServiceLoader.Provider;
import java.util.function.BiFunction;


@SuppressWarnings("ALL")
public class Bootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(Bootstrap.class);


    private static final Provider<Bootstrap> BOOTSTRAP = new Provider<>() {
        @Override
        public Class<? extends Bootstrap> type() {
            return Bootstrap.class;
        }

        @Override
        public Bootstrap get() {
            return new Bootstrap(Container.getContainerInstance());
        }
    };

    private static Bootstrap BOOTSTRAP_CACHE = null;
    protected final Metrics metrics;
    protected final ProviderRuntime runtime;
    private final Json json;
    private final Container container;
    private final BiFunction handler;
    private final Optional<TxManager> txManager;
    private final RequestConverter<?, ?, InvocationContext<?>> requestConverter;
    private final ResponseConverter<?, ?, InvocationContext<?>> responseConverter;
    private final ExceptionHandler exceptionHandler;
    public Bootstrap(Container container) {
        var timer = ZonedDateTime.now();
        try {
            this.container = container;

            handler = resolveHandler(container);

            txManager = container.resolve(TxManager.class);

            metrics = container
                    .resolve(Metrics.class)
                    .orElseThrow(() -> new IllegalStateException("No " + TxManager.class.getName() + " provided"));

            runtime = container
                    .resolve(ProviderRuntime.class)
                    .orElseThrow(() -> new IllegalStateException("No " + ProviderRuntime.class.getName() + " provided"));

            requestConverter = container
                    .resolve(RequestConverter.class)
                    .orElse(Objects.requireNonNull(runtime.getProviderDefaultRequestConverter()));

            responseConverter = container
                    .resolve(ResponseConverter.class)
                    .orElse(Objects.requireNonNull(runtime.getProviderDefaultResponseConverter()));

            exceptionHandler = container
                    .resolve(ExceptionHandler.class)
                    .orElse(Objects.requireNonNull(runtime.getProviderDefaultExceptionHandler()));

            json = container.resolve(Json.class).orElseThrow();

        } finally {
            EventPublisher.publish(new ContainerStartedEvent(timer));
        }
    }

    public static BiFunction<?,?,?> resolveHandler(Container container) {
        final BiFunction<?,?,?> handler;
        var functionConfig = container.getValue(ServiceFunction.NAME);
        if (functionConfig.isPresent()) {
            handler = (BiFunction) container
                    .resolve(functionConfig.get())
                    .orElseThrow(() -> new IllegalStateException("Configured Service "+functionConfig.get()+" could not be found"));
        } else {
            handler = (BiFunction<?, ?, ?>) container
                    .resolve(ServiceFunction.NAME)
                    .orElseThrow(() -> new IllegalStateException("No " + ServiceFunction.NAME + " provided"));
        }
        return handler;
    }

    public static Bootstrap getBoostrap() {
        if (BOOTSTRAP_CACHE == null) {
            synchronized (BOOTSTRAP) {
                //double null check locking pattern
                if (BOOTSTRAP_CACHE == null) {
                    BOOTSTRAP_CACHE = BOOTSTRAP.get();
                }
            }
        }
        return BOOTSTRAP_CACHE;
    }

    public Container getContainer() {
        return container;
    }

    public BiFunction<?, ?, ?> getHandler() {
        return handler;
    }

    public Optional<TxManager> getTxManager() {
        return txManager;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public ProviderRuntime getRuntime() {
        return runtime;
    }

    public RequestConverter<?, ?, InvocationContext<?>> getRequestConverter() {
        return requestConverter;
    }

    public ResponseConverter<?, ?, InvocationContext<?>> getResponseConverter() {
        return responseConverter;
    }

    public ExceptionHandler getExceptionHandler() {
        return exceptionHandler;
    }

    public Json getJson() {
        return json;
    }

    @SuppressWarnings("unchecked")
    public Object handleRequest(Object payload, Object context) {
        final BiFunction handler = getHandler();
        var metrics = getMetrics();
        RequestConverter requestConverter = getRequestConverter();
        ResponseConverter responseConverter = getResponseConverter();
        var txManager = getTxManager();
        LOG.trace("ENTERING -> " + handler.getClass().getName());
        return metrics.doSection(getClass().getSimpleName(), () -> {
            final var invocation = runtime.createInvocation(context);
            try {
                ExecutionContext.set(invocation);
                var request = requestConverter.decode(payload, invocation);
                Object response = null;
                if (txManager.isPresent()) {
                    response = txManager
                            .get()
                            .doInTransaction(
                                    ServiceFunction.NAME,
                                    () -> invokeFunction(invocation, request)
                            );
                } else {
                    response = invokeFunction(invocation, request);
                }
                LOG.trace("COMPLETE -> " + handler.getClass().getName());
                return responseConverter.encode(response, invocation);
            } catch (RuntimeException e) {
                LOG.error(e.getMessage(), e);
                return getExceptionHandler().apply(invocation, e);
            } finally {
                ExecutionContext.unset();
                txManager.ifPresent(TxManager::commit);
            }
        });
    }

    private Object invokeFunction(InvocationContext invocation, Object request) {
        var payload = request;
        if (handler instanceof RequestConverter) {
            payload = ((RequestConverter) handler).decode(handler, invocation);
        }
        Object response = null;
        if (handler instanceof ServiceFunction) {
            response = handler.apply(request, invocation);
        } else {
            response = handler.apply(request, invocation.getRequestId());
        }
        if (handler instanceof ResponseConverter) {
            response = ((ResponseConverter) handler).encode(response, invocation);
        }
        return response;
    }


}



