package net.odoframework.service;

import java.util.function.BiFunction;

public interface ExceptionHandler<T, K> extends BiFunction<InvocationContext<T>, Throwable, K> {


}
