package net.odoframework.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultExceptionHandler<T> implements ExceptionHandler<T, Object> {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @Override
    public Object apply(InvocationContext<T> runtime, Throwable throwable) {
        LOG.error(throwable.getMessage(), throwable);
        if (throwable instanceof RuntimeException) {
            throw ((RuntimeException) throwable);
        } else {
            throw new RuntimeException(throwable);
        }
    }
}
