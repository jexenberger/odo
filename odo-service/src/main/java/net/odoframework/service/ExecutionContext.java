package net.odoframework.service;

import java.util.Objects;
import java.util.Optional;

public class ExecutionContext {

    private static final ThreadLocal<InvocationContext<?>> CONTEXT = new ThreadLocal<>();


    public static <T> void set(T context) {
        T instance = Objects.requireNonNull(context);
        CONTEXT.set((InvocationContext<?>) instance);
    }

    public static void unset() {
        CONTEXT.remove();
    }

    public static <T> Optional<T> get() {
        return Optional.ofNullable((T) CONTEXT.get());
    }


}
