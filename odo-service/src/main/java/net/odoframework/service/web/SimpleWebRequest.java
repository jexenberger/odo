package net.odoframework.service.web;


import net.odoframework.container.util.Json;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SimpleWebRequest implements WebRequest {

    private String body;
    private String method;
    private Map<String, List<String>> multiValueHeader = new LinkedHashMap<>();
    private Principal principal;
    private Map<String, String> headers = new LinkedHashMap<>();
    private String path;
    private Map<String, String> pathVariables = new LinkedHashMap<>();
    private final Map<String, String> queryParameters = new LinkedHashMap<>();
    private Json json;

    public SimpleWebRequest(Json json) {
        this.json = json;
    }

    public void setMultiValueHeader(Map<String, List<String>> multiValueHeader) {
        this.multiValueHeader = multiValueHeader;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public void setPathVariables(Map<String, String> pathVariables) {
        this.pathVariables = pathVariables;
    }

    @Override
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setBodyAsObject(Object body) {
        this.body = json.marshal(body);
    }

    @Override
    public Optional<List<String>> getMultiValueHeader(String name) {
        return Optional.ofNullable(multiValueHeader.get(name));
    }

    @Override
    public Optional<Principal> getUserPrincipal() {
        return Optional.ofNullable(this.principal);
    }

    @Override
    public Optional<String> getHeader(String name) {
        return Optional.ofNullable(this.headers.get(name));
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public Optional<String> getQueryParam(String name) {
        return Optional.ofNullable(queryParameters.get(name));
    }

    @Override
    public Optional<List<String>> getMultiValueQueryParam(String name) {
        return Optional.empty();
    }

    @Override
    public Json getJson() {
        return this.json;
    }

    public void setJson(Json json) {
        this.json = json;
    }

    @Override
    public Optional<String> getPathVariable(String name) {
        if (pathVariablesNotSet()) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.pathVariables.get(name));
    }

    @Override
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public boolean matches(String pattern) {
        var pathVariables = URNPath.match(getPath(), pattern);
        if (pathVariablesNotSet() && pathVariables.isPresent()) {
            this.pathVariables = pathVariables.get();
            return true;
        }
        return false;
    }


    public SimpleWebRequest addQueryParam(String name, Object value) {
        if (value != null) {
            this.queryParameters.put(name, value.toString());
        }
        return this;
    }

    @Override
    public boolean pathVariablesNotSet() {
        return this.pathVariables == null || this.pathVariables.isEmpty();
    }

}
