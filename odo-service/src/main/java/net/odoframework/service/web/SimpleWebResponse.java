package net.odoframework.service.web;

import net.odoframework.container.util.Json;

import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleWebResponse implements WebResponse {

    private int statusCode;
    private String message;
    private Map<String, String> headers;
    private String body;
    private final boolean isBase64Encoded = false;
    private final Json json;

    public SimpleWebResponse(Json json) {
        this.json = json;
    }

    @Override
    public SimpleWebResponse status(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @Override
    public SimpleWebResponse addHeader(String name, String value) {
        if (headers == null) {
            headers = new LinkedHashMap<>();
        }
        headers.put(name, value);
        return this;
    }

    @Override
    public SimpleWebResponse body(String body) {
        this.body = body;
        return this;
    }

    @Override
    public Json getJson() {
        return json;
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public SimpleWebResponse message(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String getBody() {
        return body;
    }

    public boolean isBase64Encoded() {
        return isBase64Encoded;
    }

    @Override
    public String toString() {
        return json.marshal(this);
    }
}
