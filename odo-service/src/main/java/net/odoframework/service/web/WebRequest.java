package net.odoframework.service.web;

import net.odoframework.container.util.Json;
import net.odoframework.service.Request;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface WebRequest extends Request {

    Optional<List<String>> getMultiValueHeader(String name);

    Optional<Principal> getUserPrincipal();

    Optional<String> getHeader(String name);

    String getPath();

    Optional<String> getPathVariable(String name);

    String getMethod();

    boolean matches(String pattern);

    Optional<String> getQueryParam(String name);

    Optional<List<String>> getMultiValueQueryParam(String name);

    default Optional<String> getContentType() {
        return getHeader("Content-Type");
    }


    default Optional<Integer> getPathVariableInt(String name) {
        return getPathVariable(name)
                .map(Integer::parseInt);
    }

    default Optional<Long> getPathVariableLong(String name) {
        return getPathVariable(name)
                .map(Long::parseLong);
    }

    default Optional<Double> getPathVariableDouble(String name) {
        return getPathVariable(name)
                .map(Double::parseDouble);
    }

    default Optional<Boolean> getPathVariableBoolean(String name) {
        return getPathVariable(name)
                .map(Boolean::parseBoolean);
    }

    default Optional<Long> getQueryParamAsLong(String name) {
        return getQueryParam(name)
                .map(Long::parseLong);
    }

    default Optional<Boolean> getQueryParamAsBoolean(String name) {
        return getQueryParam(name)
                .map(Boolean::parseBoolean);
    }

    default Optional<Double> getQueryParamAsDouble(String name) {
        return getQueryParam(name)
                .map(Double::parseDouble);
    }

    Json getJson();

    default <T> T getBody(Class<T> type) {
        return getJson().unmarshal(getBody(), type);
    }


    boolean pathVariablesNotSet();
}
