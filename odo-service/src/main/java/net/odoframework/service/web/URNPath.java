package net.odoframework.service.web;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.of;
import static net.odoframework.util.Strings.requireNotBlank;

public class URNPath {

    private final String path;
    private Map<String, String> pathElements;

    public URNPath(String pattern) {
        this.path = requireNotBlank(pattern, "pattern is a required parameter");
    }

    public static String checkAndTrim(String str) {
        var trimmed = requireNotBlank(str, "passed null string").trim();
        if (trimmed.startsWith("/") && trimmed.length() == 1) {
            return "";
        }
        if (trimmed.startsWith("/")) {
            trimmed = trimmed.substring(1);
        }
        if (trimmed.endsWith("/")) {
            trimmed = trimmed.substring(0, trimmed.length() - 1);
        }
        return trimmed;

    }


    public static Optional<Map<String, String>> match(String path, String pattern) {

        var pathElements = checkAndTrim(path).split("/");
        var patternElements = checkAndTrim(pattern).split("/");

        if (pathElements.length != patternElements.length) {
            return Optional.empty();
        }

        Map<String, String> returnMap = null;
        for (int i = 0; i < pathElements.length; i++) {
            var pathElement = pathElements[i].trim();
            var patternElement = patternElements[i].trim();
            final var isVariable = isVariable(patternElement);
            if (!pathElement.equals(patternElement) && !isVariable) {
                return Optional.empty();
            }
            var index = (isVariable)
                    ? patternElement.substring(1, patternElement.length() - 1)
                    : Integer.toString(i);
            if (returnMap == null) {
                returnMap = new LinkedHashMap<>();
            }
            returnMap.put(index, pathElement);
        }
        return of(returnMap);

    }

    private static boolean isVariable(String patternElement) {
        return patternElement.startsWith("{") && patternElement.endsWith("}");
    }


}
