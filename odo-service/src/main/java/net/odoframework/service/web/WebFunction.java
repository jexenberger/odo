package net.odoframework.service.web;

import net.odoframework.container.util.Json;
import net.odoframework.service.ServiceFunction;

public interface WebFunction extends ServiceFunction<WebRequest, WebResponse> {


    default WebResponse response() {
        return new SimpleWebResponse(getJson());
    }

    Json getJson();

    default <T> WebResponse ok() {
        return response().ok();
    }


    default WebResponse created(String locationUrl) {
        return response().created().addHeader("Location", locationUrl);
    }

    default WebResponse userError() {
        return response().userError();
    }

    default WebResponse serverError() {
        return response().serverError();
    }

    default WebResponse notFound() {
        return response().notFound();
    }

    default WebResponse unsupported(String message) {
        return response().status(405).body(message);
    }


}
