package net.odoframework.service.web;

import net.odoframework.container.util.Json;
import net.odoframework.service.Response;
import net.odoframework.util.Strings;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public interface WebResponse extends Response {
    WebResponse status(int statusCode);

    default WebResponse ok() {
        return status(200);
    }

    default WebResponse created() {
        return status(201);
    }

    default WebResponse userError() {
        return status(400);
    }

    default WebResponse serverError() {
        return status(500);
    }

    default WebResponse notFound() {
        return status(401);
    }

    default WebResponse conflict() {
        return status(409);
    }

    default boolean isFailed() {
        return (getStatusCode() > 300);
    }


    default WebResponse addHeader(String name, long value) {
        return addHeader(name, Long.toString(value));
    }

    default WebResponse addHeader(String name, boolean value) {
        return addHeader(name, Boolean.toString(value));
    }

    default WebResponse cors(Set<String> headers, Set<String> origins, Set<String> methods) {
        addHeader("Access-Control-Allow-Headers", String.join(",", headers));
        addHeader("Access-Control-Allow-Origin", String.join(",", origins));
        addHeader("Access-Control-Allow-Methods", String.join(",", methods));
        return this;
    }

    default WebResponse contentType(String contentType) {
        return addHeader("Content-Type", Strings.requireNotBlank(contentType, "Content type is required"));
    }

    default Optional<String> getContentType() {
        return Optional.ofNullable(getHeaders().get("Content-Type"));
    }

    WebResponse addHeader(String name, String value);

    WebResponse body(String body);


    default WebResponse body(Object body) {
        return body(getJson().marshal(body));
    }

    Json getJson();

    int getStatusCode();

    String getMessage();

    Map<String, String> getHeaders();

    WebResponse message(String message);

    String getBody();

}
