package net.odoframework.service.web;

import net.odoframework.container.util.Json;
import net.odoframework.service.InvocationContext;
import net.odoframework.util.Strings;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static java.util.Objects.requireNonNull;

public abstract class HttpRouter implements WebFunction {


    private final Json json;
    private final Map<String, Map<String, BiFunction<WebRequest, Object, WebResponse>>> handlers = new LinkedHashMap<>();

    public HttpRouter(Json json) {
        this.json = requireNonNull(json, "json is a required parameter");
        build();
    }

    @Override
    public WebResponse apply(WebRequest s, InvocationContext<?> invocation) {
        Strings.requireNotBlank(s.getMethod(), "NO METHOD");
        Strings.requireNotBlank(s.getPath(), "NO METHOD");


        var operationHandler = handlers.get(s.getMethod().toUpperCase());
        if (operationHandler == null || operationHandler.isEmpty()) {
            return unsupported("Unsupported Operation");
        }
        for (var function : operationHandler.entrySet()) {
            if (s.matches(function.getKey())) {
                final var apply = function.getValue().apply(s, invocation.getRequestId());
                if (apply == null) {
                    return response().status(405).body("METHOD NOT SUPPORTED");
                }
                return apply;
            }
        }
        return notFound().body("NOT FOUND");
    }

    public String getDefaultContentType() {
        return "application/json";
    }

    public HttpRouter addMapping(String operation, String path, BiFunction<WebRequest, Object, WebResponse> handler) {
        var op = operation.toUpperCase();
        if (!handlers.containsKey(Strings.requireNotBlank(op, "operation is required"))) {
            handlers.put(op, new LinkedHashMap<>());
        }
        handlers.get(op).put(Strings.requireNotBlank(path, "path is required"), requireNonNull(handler, "handler is required"));
        return this;
    }

    public HttpRouter get(String path, BiFunction<WebRequest, Object, WebResponse> handler) {
        return addMapping("GET", path, handler);
    }

    public HttpRouter post(String path, BiFunction<WebRequest, Object, WebResponse> handler) {
        return addMapping("POST", path, handler);
    }

    public HttpRouter put(String path, BiFunction<WebRequest, Object, WebResponse> handler) {
        return addMapping("PUT", path, handler);
    }

    public HttpRouter delete(String path, BiFunction<WebRequest, Object, WebResponse> handler) {
        return addMapping("DELETE", path, handler);
    }

    public HttpRouter head(String path, BiFunction<WebRequest, Object, WebResponse> handler) {
        return addMapping("HEAD", path, handler);
    }

    protected <T> WebResponse withStatus(int status, T body) {
        final var response = response().status(status);
        if (body instanceof String) {
            response.body(body.toString());
        } else {
            response.body(getJson().marshal(body));
        }
        return (Strings.isNotBlank(getDefaultContentType()))
                ? response.contentType(getDefaultContentType())
                : response;
    }


    protected <T> WebResponse conflict(T body) {
        return withStatus(409, body);
    }


    protected <T> WebResponse ok(T body) {
        return withStatus(200, body);
    }

    protected <T> WebResponse userError(T body) {
        return withStatus(400, body);
    }

    protected <T> WebResponse serverError(T body) {
        return withStatus(500, body);
    }

    protected <T> WebResponse notFound(T body) {
        return withStatus(404, body);
    }

    public abstract void build();

    @Override
    public Json getJson() {
        return json;
    }

    public Map<String, Map<String, BiFunction<WebRequest, Object, WebResponse>>> getHandlers() {
        return handlers;
    }
}
