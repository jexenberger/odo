package net.odoframework.service;

import net.odoframework.container.GsonJson;
import net.odoframework.container.ModuleBuilder;
import net.odoframework.container.injection.Conditions;
import net.odoframework.container.injection.Container;
import net.odoframework.container.metrics.Metrics;
import net.odoframework.container.sql.MonitoredDefaultSQLTemplate;
import net.odoframework.container.sql.SimpleDataSource;
import net.odoframework.container.sql.TxDataSource;
import net.odoframework.container.tx.SimpleTransactionManager;
import net.odoframework.container.tx.TxManager;
import net.odoframework.container.util.Json;
import net.odoframework.sql.SQLTemplate;

import javax.sql.DataSource;

import static java.util.function.Predicate.not;

public class ServiceModule extends ModuleBuilder {


    @Override
    public void build() {
        provides(Json.class).with(it -> new GsonJson(it.valueAsBoolean("odo.json.prettyprinting", true)));

        provides(DataSource.class)
                .with(TxDataSource::new)
                .<TxDataSource>after((a, b) -> a.init(b.getContainer().getConfiguration()))
                .condition(
                        not(Conditions.isBeanAlreadyPresent(DataSource.class))
                                .and(Conditions.isConfigPresent(SimpleDataSource.DRIVER)
                                        .and(Conditions.isConfigPresent(SimpleDataSource.URL)))
                );

        provides(SQLTemplate.class)
                .with(it -> new MonitoredDefaultSQLTemplate(it.references(DataSource.class), it.references(Metrics.class)))
                .condition(Conditions.isBeanPresent(DataSource.class));
        provides(TxManager.class)
                .with(SimpleTransactionManager::new);
    }


    private void postContainerCreated(Container container) {

    }
}
