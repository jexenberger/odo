package net.odoframework.service;

/**
 * A holder for the runtime execution context of the underlying runtime environment
 *
 * @param <T> the type of the context object of the underlying Runtime environment
 */
public interface InvocationContext<T> {

    /**
     * A unique identifier to mark the invocation, should be the one provided by the runtime
     *
     * @return a unique ID for the request
     */
    String getRequestId();

    /**
     * The Object representing the underlying Runtime execution context
     *
     * @return the instance of this raw execution context
     */
    T getRequestContext();

    /**
     * The raw payload that was passed in the function invocation
     *
     * @return the raw payload of the function execution
     */
    Object getRawPayload();


}
