package net.odoframework.service;

import java.util.UUID;

public class MockInvocationContext<T> implements InvocationContext<T> {

    private final String requestId;
    private final T body;

    public MockInvocationContext(T body) {
        this.body = body;
        this.requestId = UUID.randomUUID().toString();
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    @Override
    public T getRequestContext() {
        return body;
    }

    @Override
    public Object getRawPayload() {
        return body;
    }
}
