package net.odoframework.service;

import java.util.function.BiFunction;

public interface ServiceFunction<T extends Request, K extends Response> extends BiFunction<T, InvocationContext<?>, K> {

    String NAME = String.join(".", ServiceFunction.class.getPackageName(), "function");

}
