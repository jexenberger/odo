package net.odoframework.service;


import net.odoframework.util.Encoder;

@FunctionalInterface
public interface ResponseConverter<T, K, Z> extends Encoder<T, K, InvocationContext<Z>> {
}
