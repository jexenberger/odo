package net.odoframework.service.logging;

import net.odoframework.container.events.Log;
import net.odoframework.container.events.Log.Level;
import net.odoframework.container.injection.ConfigurationProperties;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Slf4jLoggerFactory implements ILoggerFactory {

    private static final ConcurrentMap<String, Slf4jLogger> loggers = new ConcurrentHashMap<>();
    private static Log.Level DEFAULT_LEVEL;
    private final Properties logProperties;

    public Slf4jLoggerFactory() {
        this.logProperties = ConfigurationProperties.getGlobal(getClass()).findByPrefix("odo.log.", true);
    }

    public static void setDefaultLevel(Level defaultLevel) {
        Slf4jLoggerFactory.DEFAULT_LEVEL = defaultLevel;
    }

    public static Level getDefaultLevel() {
        return DEFAULT_LEVEL;
    }

    @Override
    public Logger getLogger(String name) {
        var logger = loggers.get(name);
        if (logger == null) {
            var level = logProperties.getProperty(name + ".level", Slf4jLoggerFactory.DEFAULT_LEVEL.name());
            var logLevel = Log.Level.valueOf(level.toLowerCase());
            logger = new Slf4jLogger(name, logLevel);
            loggers.put(name, logger);
        }
        return logger;
    }
}
