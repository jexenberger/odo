package net.odoframework.service.logging;

import net.odoframework.container.events.Log;
import net.odoframework.container.events.Log.Level;
import net.odoframework.container.injection.ConfigurationProperties;
import net.odoframework.util.Dates;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.helpers.MessageFormatter;
import org.slf4j.spi.LoggingEventBuilder;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.Arrays;

public class Slf4jLogger implements Logger {

    private final String name;
    private Log.Level level;

    public Slf4jLogger(String name) {
        this.name = name;
    }

    public Slf4jLogger(String name, Level level) {
        this(name);
        this.level = level;
    }

    public synchronized void setLevel(Level level) {
        this.level = level;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public LoggingEventBuilder makeLoggingEventBuilder(org.slf4j.event.Level level) {
        return Logger.super.makeLoggingEventBuilder(level);
    }

    @Override
    public boolean isEnabledForLevel(org.slf4j.event.Level level) {
        return Logger.super.isEnabledForLevel(level);
    }

    public boolean isLevelEnabled(Log.Level level) {
        if (this.level == null) {
            ConfigurationProperties.getGlobal(getClass());
        }
        return level.ordinal() >= this.level.ordinal();
    }


    @Override
    public boolean isTraceEnabled() {
        return isLevelEnabled(Log.Level.trace);
    }

    @Override
    public void trace(String msg) {
        if (isTraceEnabled())
            Log.trace(name, msg);
    }

    @Override
    public void trace(String format, Object arg) {
        trace(format(format, arg));
    }

    @Override
    public void trace(String format, Object arg1, Object arg2) {
        trace(name, format(format, arg1, arg2));
    }

    @Override
    public void trace(String format, Object... arguments) {
        trace(format(format, arguments));
    }

    @Override
    public void trace(String msg, Throwable t) {
        if (isTraceEnabled())
            Log.publishLog(name, msg, level, t);
    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return isTraceEnabled();
    }

    @Override
    public LoggingEventBuilder atTrace() {
        return Logger.super.atTrace();
    }

    @Override
    public void trace(Marker marker, String msg) {
        trace(msg);
    }

    @Override
    public void trace(Marker marker, String format, Object arg) {
        trace(format, arg);
    }

    @Override
    public void trace(Marker marker, String format, Object arg1, Object arg2) {
        trace(format, arg1, arg2);
    }

    @Override
    public void trace(Marker marker, String format, Object... argArray) {
        trace(format, argArray);
    }

    @Override
    public void trace(Marker marker, String msg, Throwable t) {
        trace(msg, t);
    }

    @Override
    public boolean isDebugEnabled() {
        return isLevelEnabled(Level.debug);
    }

    @Override
    public void debug(String msg) {
        if (isDebugEnabled())
            Log.debug(name, msg);
    }

    @Override
    public void debug(String format, Object arg) {
        debug(format(format, arg));
    }

    @Override
    public void debug(String format, Object arg1, Object arg2) {
        debug(format(format, arg1, arg2));
    }

    @Override
    public void debug(String format, Object... arguments) {
        debug(format(format, arguments));
    }

    @Override
    public void debug(String msg, Throwable t) {
        if (isDebugEnabled())
            Log.publishLog(name, msg, level, t);
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return isLevelEnabled(Level.debug);
    }

    @Override
    public void debug(Marker marker, String msg) {
        debug(msg);
    }

    @Override
    public void debug(Marker marker, String format, Object arg) {
        debug(format, arg);
    }

    @Override
    public void debug(Marker marker, String format, Object arg1, Object arg2) {
        debug(format, arg1, arg2);
    }

    @Override
    public void debug(Marker marker, String format, Object... arguments) {
        debug(format, arguments);
    }

    @Override
    public void debug(Marker marker, String msg, Throwable t) {
        debug(msg, t);
    }

    @Override
    public LoggingEventBuilder atDebug() {
        return Logger.super.atDebug();
    }

    @Override
    public boolean isInfoEnabled() {
        return isLevelEnabled(Level.info);
    }

    @Override
    public void info(String msg) {
        if (isInfoEnabled())
            Log.info(name, msg);
    }

    @Override
    public void info(String format, Object arg) {
        info(format(format, arg));
    }

    @Override
    public void info(String format, Object arg1, Object arg2) {
        info(format(format, arg1), arg2);
    }

    @Override
    public void info(String format, Object... arguments) {
        info(format(format, arguments));
    }

    @Override
    public void info(String msg, Throwable t) {
        if (isInfoEnabled())
            Log.publishLog(name, msg, level, t);
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return isInfoEnabled();
    }

    @Override
    public void info(Marker marker, String msg) {
        info(msg);
    }

    @Override
    public void info(Marker marker, String format, Object arg) {
        info(format, arg);
    }

    @Override
    public void info(Marker marker, String format, Object arg1, Object arg2) {
        info(format, arg1, arg2);
    }

    @Override
    public void info(Marker marker, String format, Object... arguments) {
        info(format, arguments);
    }

    @Override
    public void info(Marker marker, String msg, Throwable t) {
        info(msg, t);
    }

    @Override
    public LoggingEventBuilder atInfo() {
        return Logger.super.atInfo();
    }

    @Override
    public boolean isWarnEnabled() {
        return isLevelEnabled(Level.warn);
    }

    @Override
    public void warn(String msg) {
        if (isWarnEnabled())
            Log.warn(name, msg);
    }

    @Override
    public void warn(String format, Object arg) {
        warn(format(format, arg));
    }

    @Override
    public void warn(String format, Object... arguments) {
        warn(format(format, arguments));
    }

    private String format(String format, Object... arguments) {
        var args = Arrays.stream(arguments)
                .map(it -> (it instanceof LocalDateTime) ? Dates.toDate((LocalDateTime) it) : it)
                .map(it -> (it instanceof ZonedDateTime) ? Dates.toDate((ChronoLocalDateTime<?>) it) : it)
                .toArray();

        return MessageFormatter.arrayFormat(format, args).getMessage();
    }

    @Override
    public void warn(String format, Object arg1, Object arg2) {
        warn(format(format, arg1, arg2));
    }

    @Override
    public void warn(String msg, Throwable t) {
        if (isWarnEnabled())
            Log.publishLog(name, msg, level, t);
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
        return isWarnEnabled();
    }

    @Override
    public void warn(Marker marker, String msg) {
        warn(msg);
    }

    @Override
    public void warn(Marker marker, String format, Object arg) {
        warn(format, arg);
    }

    @Override
    public void warn(Marker marker, String format, Object arg1, Object arg2) {
        warn(format, arg1, arg2);
    }

    @Override
    public void warn(Marker marker, String format, Object... arguments) {
        warn(format, arguments);
    }

    @Override
    public void warn(Marker marker, String msg, Throwable t) {
        warn(msg, t);
    }

    @Override
    public LoggingEventBuilder atWarn() {
        return Logger.super.atWarn();
    }

    @Override
    public boolean isErrorEnabled() {
        return isLevelEnabled(Level.error);
    }

    @Override
    public void error(String msg) {
        if (isErrorEnabled())
            Log.error(name, msg, null);
    }

    @Override
    public void error(String format, Object arg) {
        error(format(format, arg));
    }

    @Override
    public void error(String format, Object arg1, Object arg2) {
        error(format(format, arg1, arg2));
    }

    @Override
    public void error(String format, Object... arguments) {
        error(format(format, arguments));
    }

    @Override
    public void error(String msg, Throwable t) {
        if (isErrorEnabled())
            Log.error(name, msg, t);
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return isErrorEnabled();
    }

    @Override
    public void error(Marker marker, String msg) {
        error(msg);
    }

    @Override
    public void error(Marker marker, String format, Object arg) {
        error(format, arg);
    }

    @Override
    public void error(Marker marker, String format, Object arg1, Object arg2) {
        error(format, arg1, arg2);
    }

    @Override
    public void error(Marker marker, String format, Object... arguments) {
        error(format, arguments);
    }

    @Override
    public void error(Marker marker, String msg, Throwable t) {
        error(msg, t);
    }

    @Override
    public LoggingEventBuilder atError() {
        return Logger.super.atError();
    }
}
