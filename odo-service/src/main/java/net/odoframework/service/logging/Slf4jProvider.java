package net.odoframework.service.logging;

import net.odoframework.container.events.Log;
import net.odoframework.util.EnvironmentUtils;
import org.slf4j.ILoggerFactory;
import org.slf4j.IMarkerFactory;
import org.slf4j.helpers.BasicMarkerFactory;
import org.slf4j.helpers.NOPMDCAdapter;
import org.slf4j.spi.MDCAdapter;
import org.slf4j.spi.SLF4JServiceProvider;

public class Slf4jProvider implements SLF4JServiceProvider {

    public static String REQUESTED_API_VERSION = "1.8"; // !final
    private Slf4jLoggerFactory loggerFactory;
    private IMarkerFactory markerFactory;
    private MDCAdapter mdcAdapter;

    public Slf4jProvider() {
        if (Slf4jLoggerFactory.getDefaultLevel() == null) {
            Slf4jLoggerFactory
                    .setDefaultLevel(Log.Level
                            .valueOf(EnvironmentUtils.resolve("ODO_LOG_LEVEL", "odo.log.level")
                                    .orElse("info")));
        }

    }

    @Override
    public ILoggerFactory getLoggerFactory() {
        return loggerFactory;
    }

    @Override
    public IMarkerFactory getMarkerFactory() {
        return markerFactory;
    }

    @Override
    public MDCAdapter getMDCAdapter() {
        return mdcAdapter;
    }

    @Override
    public String getRequesteApiVersion() {
        return REQUESTED_API_VERSION;
    }

    @Override
    public void initialize() {
        loggerFactory = new Slf4jLoggerFactory();
        markerFactory = new BasicMarkerFactory();
        mdcAdapter = new NOPMDCAdapter();
    }
}
