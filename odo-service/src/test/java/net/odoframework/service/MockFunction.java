package net.odoframework.service;

import java.util.Objects;
import java.util.function.BiFunction;

public class MockFunction implements BiFunction<String, String, String> {

    private final String greeting;

    public MockFunction(String greeting) {
        this.greeting = Objects.requireNonNull(greeting);
    }

    @Override
    public String apply(String s, String s2) {
        return new StringBuilder(s).reverse().toString();
    }
}
