package net.odoframework.service;

import static org.mockito.Mockito.mock;

public class MockProviderRuntime implements ProviderRuntime<String> {
    @Override
    public RequestConverter<?, ?, String> getProviderDefaultRequestConverter() {
        return (a, b) -> a;
    }

    @Override
    public ResponseConverter<?, ?, String> getProviderDefaultResponseConverter() {
        return (a, b) -> a;
    }

    @Override
    public ExceptionHandler<String, ?> getProviderDefaultExceptionHandler() {
        return new DefaultExceptionHandler<>();
    }

    @Override
    public InvocationContext<String> createInvocation(String containerInstance) {
        return mock(InvocationContext.class);
    }
}
