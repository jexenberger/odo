package net.odoframework.service;

import net.odoframework.container.Application;
import net.odoframework.container.injection.Container;

public class MockApplication extends Application {

    @Override
    protected void loadManualModules(Container container) {
        loadModule(container, new ServiceModule());
    }

    @Override
    public void build() {
        provides(ProviderRuntime.class).with(MockProviderRuntime::new);
        provides(ServiceFunction.NAME).with(it -> new MockFunction(it.value("greeting")));
    }
}
