package net.odoframework.service;

import net.odoframework.container.injection.ConfigurationProperties;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BootstrapTest {

    static {
        ConfigurationProperties.resetGlobal();
        ConfigurationProperties.getGlobal(DefaultExceptionHandler.class);
    }


    @Test
    void create() {
        var result = new Bootstrap(new MockApplication().getContainer());
        assertNotNull(result.getExceptionHandler());
        assertNotNull(result.getRequestConverter());
        assertNotNull(result.getResponseConverter());
        assertNotNull(result.getJson());
        assertNotNull(result.getRuntime());
        assertNotNull(result.getMetrics());
        assertNotNull(result.getTxManager());
        assertNotNull(result.getHandler());
    }


    @Test
    void handleRequest() {
        var bootstrap = new Bootstrap(new MockApplication().getContainer());
        var response = bootstrap.handleRequest("test", "test");
        assertEquals("tset", response);

    }

    @Test
    void handleAlternativeConfig() {
        var bootstrap = new Bootstrap(new AlternativeConfigApplication().getContainer());
        var response = bootstrap.handleRequest("test", "test");
        assertEquals("tset", response);
    }
}
