package net.odoframework.service.web;

import net.odoframework.container.GsonJson;
import net.odoframework.service.InvocationContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HttpRouterTest {

    private GsonJson json;
    private SimpleWebRequest request;
    private MockRouter router;

    @BeforeEach
    void setUp() {
        json = new GsonJson();
        router = new MockRouter(json);
        request = new SimpleWebRequest(json);
        request.setBody("Test");
    }

    @Test
    void getAll() {
        request.setMethod("GET");
        request.setPath("/test");
        var result = router.apply(request, mock(InvocationContext.class));
        assertEquals("GET ALL TEST", result.getBody());
        assertEquals(200, result.getStatusCode());
        assertEquals("application/json", result.getHeaders().get("Content-Type"));
    }

    @Test
    void get() {
        request.setMethod("GET");
        request.setPath("/test/123");
        var result = router.apply(request, mock(InvocationContext.class));
        assertEquals("123", result.getBody());
        assertEquals(200, result.getStatusCode());
        assertEquals("plain/text", result.getHeaders().get("Content-Type"));
    }


    @Test
    void post() {
        final var invocation = mock(InvocationContext.class);
        when(invocation.getRequestId()).thenReturn(UUID.randomUUID().toString());
        request.setMethod("POST");
        request.setPath("/test");
        var result = router.apply(request, invocation);
        assertEquals("TEST", result.getBody());
        assertEquals(201, result.getStatusCode());
        assertEquals("/test/qwerty", result.getHeaders().get("Location"));
    }

    @Test
    void put() {
        request.setMethod("PUT");
        request.setPath("/test/123");
        var result = router.apply(request, mock(InvocationContext.class));
        assertEquals("Updated 123", result.getBody());
        assertEquals(200, result.getStatusCode());
    }

    @Test
    void delete() {
        request.setMethod("DELETE");
        request.setPath("/test/123");
        var result = router.apply(request, mock(InvocationContext.class));
        assertEquals("Deleted 123", result.getBody());
        assertEquals(200, result.getStatusCode());
    }
}
