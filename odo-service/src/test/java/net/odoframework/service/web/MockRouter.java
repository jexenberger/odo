package net.odoframework.service.web;

import net.odoframework.container.util.Json;

import java.util.Optional;

public class MockRouter extends HttpRouter {

    public MockRouter(Json json) {
        super(json);
    }

    @Override
    public void build() {
        get("/test", (req, context) -> ok("GET ALL TEST"));
        get("/test/{id}", (req, context) -> ok(req.getPathVariableLong("id").get()).contentType("plain/text"));
        post("/test", (req, context) -> created("/test/qwerty").body("TEST"));
        put("/test/{id}", (req, context) -> ok("Updated " + req.getPathVariableLong("id").get()));
        delete("/test/{id}", (req, context) -> ok("Deleted " + req.getPathVariableLong("id").get()));
    }
}
