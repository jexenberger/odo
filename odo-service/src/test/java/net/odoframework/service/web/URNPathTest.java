package net.odoframework.service.web;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class URNPathTest {


    @Test
    void match() {
        var result = URNPath.match("hello/world", "hello/{id}");
        assertTrue(result.isPresent());
        assertEquals("world", result.get().get("id"));
        assertEquals("hello", result.get().get("0"));
    }

    @Test
    void matchTrailing() {
        var result = URNPath.match("hello/world/", "hello/{id}");
        assertTrue(result.isPresent());
        assertEquals("world", result.get().get("id"));
        assertEquals("hello", result.get().get("0"));
    }

    @Test
    void matchLeading() {
        var result = URNPath.match("/hello/world", "/hello/{id}");
        assertTrue(result.isPresent());
        assertEquals("world", result.get().get("id"));
        assertEquals("hello", result.get().get("0"));
    }

    @Test
    void matchLeadingAndTrailing() {
        var result = URNPath.match("/hello/world/", "/hello/{id}/");
        assertTrue(result.isPresent());
        assertEquals("world", result.get().get("id"));
        assertEquals("hello", result.get().get("0"));
    }

    @Test
    void notMatch() {
        var result = URNPath.match("hello/world/qwerty", "hello/{id}");
        assertFalse(result.isPresent());
    }

    @Test
    void notMatchSameElements() {
        var result = URNPath.match("hello/world", "hello/qwerty");
        assertFalse(result.isPresent());
    }
}

