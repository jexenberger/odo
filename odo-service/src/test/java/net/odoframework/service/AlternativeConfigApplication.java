package net.odoframework.service;

import net.odoframework.container.Application;
import net.odoframework.util.configuration.Configuration;
import net.odoframework.util.configuration.SimpleMapSource;

import java.util.HashMap;
import java.util.Properties;

public class AlternativeConfigApplication extends Application {

    @Override
    protected Configuration loadConfiguration() {
        final var properties = new HashMap<String, String>();
        properties.put(ServiceFunction.NAME, MockFunction.class.getName());
        properties.put("greeting", "alternative");
        return new Configuration(new SimpleMapSource(properties));
    }

    @Override
    public void build() {
        provides(ProviderRuntime.class).with(MockProviderRuntime::new);
        provides(MockFunction.class).with(it -> new MockFunction(it.value("greeting")));
    }


}
