package net.odoframework.service;

import net.odoframework.container.injection.ConfigurationProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class DefaultExceptionHandlerTest {

    static {
        ConfigurationProperties.getGlobal(DefaultExceptionHandlerTest.class).reset();
        ConfigurationProperties.resetGlobal();
    }

    @Test
    void apply() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            new DefaultExceptionHandler<String>().apply(mock(InvocationContext.class), new RuntimeException("Borked"));
        });
    }

    @Test
    void applyChecked() {
        var result = Assertions.assertThrows(RuntimeException.class, () -> {
            new DefaultExceptionHandler<String>().apply(mock(InvocationContext.class), new Exception("Borked"));
        });
        assertEquals("Borked", result.getCause().getMessage());
    }
}
