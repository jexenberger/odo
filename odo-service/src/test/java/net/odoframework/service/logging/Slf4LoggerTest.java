package net.odoframework.service.logging;

import net.odoframework.container.events.EventPublisher;
import net.odoframework.container.events.Log;
import net.odoframework.container.events.Log.Level;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Marker;
import org.slf4j.helpers.BasicMarkerFactory;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

public class Slf4LoggerTest {

    private final Slf4jLogger logger = new Slf4jLogger("test", Level.trace);
    private final Marker marker = new BasicMarkerFactory().getMarker("test");

    @BeforeEach
    void setUp() {
        EventPublisher.clearLogBuffer();
        EventPublisher.clearHandlers();
    }

    @AfterEach
    void tearDown() {
        logger.setLevel(Level.trace);
        EventPublisher.clearLogBuffer();
        EventPublisher.clearHandlers();
    }

    @Test
    void logDebug() {

        final var called = new AtomicInteger(0);
        EventPublisher.handler(Log.class, (Consumer<Log>) it -> {
            called.incrementAndGet();
            System.out.println(it.getMessage());
            System.out.println(it.getError());
        });
        assertTrue(logger.isDebugEnabled(marker));
        assertTrue(logger.isDebugEnabled());
        callDebug(marker);
        assertEquals(10, called.get());
        logger.setLevel(Level.off);
        assertFalse(logger.isDebugEnabled(marker));
        assertFalse(logger.isDebugEnabled());
        callDebug(marker);
        assertEquals(10, called.get());
    }

    private void callDebug(Marker marker) {
        logger.debug("test");
        logger.debug("test", new RuntimeException("test"));
        logger.debug("{0}", 1);
        logger.debug("{0} {1}", 1, 2);
        logger.debug("{0,time} {1}", LocalDateTime.now(), Slf4LoggerTest.class);
        logger.debug(marker, "{0} {1}", 1, 2);
        logger.debug(marker, "test");
        logger.debug(marker, "{0}", 1);
        logger.debug(marker, "{0} {1}", 1, 2);
        logger.debug(marker, "{0} {1} {2}", 1, 2, 3);
    }

    @Test
    void logTrace() {
        final var called = new AtomicInteger(0);
        EventPublisher.handler(Log.class, (Consumer<Log>) it -> {
            called.incrementAndGet();
            System.out.println(it.getMessage());
            System.out.println(it.getError());
        });
        assertTrue(logger.isTraceEnabled(marker));
        assertTrue(logger.isTraceEnabled());
        callTrace(marker);
        assertEquals(10, called.get());
        logger.setLevel(Level.off);
        assertFalse(logger.isTraceEnabled(marker));
        assertFalse(logger.isTraceEnabled());
        callTrace(marker);
        assertEquals(10, called.get());
    }


    private void callTrace(Marker marker) {
        logger.trace("test");
        logger.trace("test", new RuntimeException("test"));
        logger.trace("{0}", 1);
        logger.trace("{0} {1}", 1, 2);
        logger.trace("{0,time} {1}", LocalDateTime.now(), Slf4LoggerTest.class);
        logger.trace(marker, "{0} {1}", 1, 2);
        logger.trace(marker, "test");
        logger.trace(marker, "{0}", 1);
        logger.trace(marker, "{0} {1}", 1, 2);
        logger.trace(marker, "{0} {1} {2}", 1, 2, 3);
    }


    @Test
    void logError() {
        final var called = new AtomicInteger(0);
        EventPublisher.handler(Log.class, (Consumer<Log>) it -> {
            called.incrementAndGet();
            System.out.println(it.getMessage());
            System.out.println(it.getError());
        });
        assertTrue(logger.isErrorEnabled(marker));
        assertTrue(logger.isErrorEnabled());
        callError(marker);
        assertEquals(10, called.get());
        logger.setLevel(Level.off);
        assertFalse(logger.isErrorEnabled(marker));
        assertFalse(logger.isErrorEnabled());
        callError(marker);
        assertEquals(10, called.get());
    }


    private void callError(Marker marker) {
        logger.error("test");
        logger.error("test", new RuntimeException("test"));
        logger.error("{0}", 1);
        logger.error("{0} {1}", 1, 2);
        logger.error("{0,time} {1}", LocalDateTime.now(), Slf4LoggerTest.class);
        logger.error(marker, "{0} {1}", 1, 2);
        logger.error(marker, "test");
        logger.error(marker, "{0}", 1);
        logger.error(marker, "{0} {1}", 1, 2);
        logger.error(marker, "{0} {1} {2}", 1, 2, 3);
    }


    @Test
    void logInfo() {
        final var called = new AtomicInteger(0);
        EventPublisher.handler(Log.class, (Consumer<Log>) it -> {
            called.incrementAndGet();
            System.out.println(it.getMessage());
            System.out.println(it.getError());
        });
        assertTrue(logger.isInfoEnabled(marker));
        assertTrue(logger.isInfoEnabled());
        callInfo(marker);
        assertEquals(10, called.get());
        logger.setLevel(Level.off);
        assertFalse(logger.isInfoEnabled(marker));
        assertFalse(logger.isInfoEnabled());
        callInfo(marker);
        assertEquals(10, called.get());
    }


    private void callInfo(Marker marker) {
        logger.info("test");
        logger.info("test", new RuntimeException("test"));
        logger.info("{0}", 1);
        logger.info("{0} {1}", 1, 2);
        logger.info("{0,time} {1}", LocalDateTime.now(), Slf4LoggerTest.class);
        logger.info(marker, "{0} {1}", 1, 2);
        logger.info(marker, "test");
        logger.info(marker, "{0}", 1);
        logger.info(marker, "{0} {1}", 1, 2);
        logger.info(marker, "{0} {1} {2}", 1, 2, 3);
    }

    @Test
    void logWarn() {
        final var called = new AtomicInteger(0);
        EventPublisher.handler(Log.class, (Consumer<Log>) it -> {
            called.incrementAndGet();
            System.out.println(it.getMessage());
            System.out.println(it.getError());
        });
        assertTrue(logger.isWarnEnabled(marker));
        assertTrue(logger.isWarnEnabled());
        callWarn(marker);
        assertEquals(10, called.get());
        logger.setLevel(Level.off);
        assertFalse(logger.isWarnEnabled(marker));
        assertFalse(logger.isWarnEnabled());
        callWarn(marker);
        assertEquals(10, called.get());
    }


    private void callWarn(Marker marker) {
        logger.warn("test");
        logger.warn("test", new RuntimeException("test"));
        logger.warn("{0}", 1);
        logger.warn("{0} {1}", 1, 2);
        logger.warn("{0,time} {1}", LocalDateTime.now(), Slf4LoggerTest.class);
        logger.warn(marker, "{0} {1}", 1, 2);
        logger.warn(marker, "test");
        logger.warn(marker, "{0}", 1);
        logger.warn(marker, "{0} {1}", 1, 2);
        logger.warn(marker, "{0} {1} {2}", 1, 2, 3);
    }
}
