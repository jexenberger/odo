package net.odoframework.container.sql;

import net.odoframework.util.configuration.Configuration;
import net.odoframework.util.configuration.SimpleMapSource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

public class SimpleDataSourceTest {

    final String driver = "org.hsqldb.jdbc.JDBCDriver";
    final String url = "jdbc:hsqldb:mem:test";
    final String user = "sa";
    final String password = "";
    final String timeout = "5";
    private SimpleDataSource source;

    @BeforeEach
    void setUp() {
        source = new SimpleDataSource();
    }

    @AfterEach
    void tearDown() {
        source.shutdown();
    }

    @Test
    void create() {
        source.create(driver, url, user, password, 5);
        assertTrue(source.isInitialised());
    }

    @Test
    void connect() throws Exception {
        source.create(driver, url, user, password, 5);
        source.shutdown();
        assertFalse(source.isInitialised());
        var conn = source.getConnection();
        assertTrue(source.isInitialised());
        assertFalse(conn.isClosed());
    }

    @Test
    void connectNotConfigured() throws Exception {
        assertThrows(IllegalStateException.class, () -> source.getConnection());
    }

    @Test
    void configure() {
        var p = new HashMap<String, String>();
        p.put(SimpleDataSource.DRIVER, driver);
        p.put(SimpleDataSource.URL, url);
        p.put(SimpleDataSource.USER, user);
        p.put(SimpleDataSource.PASSWORD, password);
        p.put(SimpleDataSource.TIMEOUT, timeout);
        p.put(SimpleDataSource.PREFIX + ".autoReconnect", Boolean.toString(true));

        source.init(new Configuration(new SimpleMapSource(p)));
        assertTrue(source.isInitialised());
    }

    @Test
    void handleNoConfiguration() {
        var p = new HashMap<String, String>();
        source.init(new Configuration());
        assertFalse(source.isInitialised());
        p.put(SimpleDataSource.DRIVER, driver);
        p.put(SimpleDataSource.URL, url);
        p.put(SimpleDataSource.USER, user);
        p.put(SimpleDataSource.PASSWORD, password);
        p.put(SimpleDataSource.TIMEOUT, timeout);
        source.init(new Configuration(new SimpleMapSource(p)));
        assertTrue(source.isInitialised());
    }
}
