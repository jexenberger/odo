package net.odoframework.container.injection;

public class MyCoolComponent {


    public String test;

    public ReferencedComponent referencedComponent;

    public AnotherReferencedComponent anotherReferencedComponent;

    public ThirdComponent thirdComponent;


    public MyCoolComponent(String test, AnotherReferencedComponent component) {
        this.test = test;
        this.anotherReferencedComponent = component;
    }

    public void setReferencedComponent(ReferencedComponent referencedComponent) {
        this.referencedComponent = referencedComponent;
    }

    public void setThirdComponent(ThirdComponent thirdComponent) {
        this.thirdComponent = thirdComponent;
    }
}
