package net.odoframework.container.injection;

public class Startup implements Runnable {

    public boolean run = false;

    public Startup() {
    }

    @Override
    public void run() {
        run = true;
    }
}
