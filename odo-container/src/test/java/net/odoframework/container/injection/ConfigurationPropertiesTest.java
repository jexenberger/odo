package net.odoframework.container.injection;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ConfigurationPropertiesTest {


    @Test
    void findByPrefix() {
        var config = new ConfigurationProperties();
        config.loadConfig(ConfigurationProperties.class);

        var result = config.findByPrefix("test", false);
        assertEquals(2, result.size());
        assertEquals("test1", result.getProperty("test.1"));
        assertEquals("test2", result.getProperty("test.2"));
        assertFalse(result.containsKey("x.1"));
    }
}
