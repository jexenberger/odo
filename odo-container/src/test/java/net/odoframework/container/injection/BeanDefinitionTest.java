package net.odoframework.container.injection;


import net.odoframework.util.configuration.Configuration;
import net.odoframework.util.configuration.SimpleMapSource;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BeanDefinitionTest {

    @Test
    void get() {

        final var configuration = new Configuration(new SimpleMapSource(Map.of(
                "odo.env", "test"
        )));
        var container = new Container(configuration);


        container
                .register(BeanDefinition.bean(ReferencedComponent.class))
                .register(BeanDefinition.bean(AnotherReferencedComponent.class).with(AnotherReferencedComponent::new))
                .register(BeanDefinition.bean("third").with(ThirdComponent::new));
        var result = new BeanDefinition<MyCoolComponent>("test").with(it -> new MyCoolComponent(
                it.value("odo.env"),
                it.references(AnotherReferencedComponent.class))
        )
                .set(ReferencedComponent.class, MyCoolComponent::setReferencedComponent)
                .set("third", MyCoolComponent::setThirdComponent)
                .apply(container);
        assertNotNull(result);
        assertEquals("test", result.test);
        assertNotNull(result.referencedComponent);
        assertNotNull(result.anotherReferencedComponent);
        assertNotNull(result.thirdComponent);
    }

    @Test
    void getNamed() {
        var container = new Container(new Configuration());
        container.register(BeanDefinition.<NamedComponent>bean("aComponent").with(NamedComponent.class));
        container.resolve("aComponent").orElseThrow();
    }
}
