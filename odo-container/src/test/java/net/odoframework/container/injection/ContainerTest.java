package net.odoframework.container.injection;

import net.odoframework.container.events.EventPublisher;
import net.odoframework.util.configuration.Configuration;
import net.odoframework.util.configuration.SimpleMapSource;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static net.odoframework.container.injection.BeanDefinition.bean;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ContainerTest {

    @Test
    void simpleBean() {
        var container = new Container(new Configuration());
        var beanDefinition = bean("hello_world").with(() -> "Hello world");
        container.register(
                beanDefinition
        );
        var instance = container.resolve("hello_world").orElseThrow();
        assertEquals("Hello world", instance);
    }

    @Test
    void getValue() {

        var props = new SimpleMapSource(
                Map.of("test", "test")
        );

        var container = new Container(new Configuration(props));
        var test = container.getValue("test").orElseThrow();
        assertEquals("test", test);
    }

    @Test
    void getWithRunnable() {
        var container = new Container(new Configuration());
        container.register(bean(Startup.class));
        var startup = container.resolve(Startup.class).orElseThrow();
        assertTrue(startup.run);
    }

    @Test
    void eventHandler() {
        final var testEvent = "It is the mighty hello world";
        final var called = new AtomicBoolean(false);
        var container = new Container(new Configuration());
        BeanDefinition<Consumer<String>> beanDefinition = BeanDefinition.<Consumer<String>>bean("hello_world").with(() -> (it) -> {
            called.set(true);
            assertEquals(testEvent, it);
        });
        container.registerEventHandler(String.class, beanDefinition);
        container.resolve(EventPublisher.class).orElseThrow().accept(testEvent);
        assertTrue(called.get());
    }

    @Test
    void registerDoubleBean() {
        final var testEvent = "It is the mighty hello world";
        final var called = new AtomicBoolean(false);
        var container = new Container(new Configuration());
        container.register(BeanDefinition.<Consumer<?>>bean(EventPublisher.class.getName()).with(() -> (it) -> {
            called.set(true);
            assertEquals(testEvent, it);
        }));
        var publisher = (Consumer<Object>) container.resolve(EventPublisher.class).orElseThrow();
        publisher.accept(testEvent);
    }
}
