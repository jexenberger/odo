package net.odoframework.container.injection;

import net.odoframework.container.ModuleBuilder;
import net.odoframework.util.configuration.Configuration;
import net.odoframework.util.configuration.SimpleMapSource;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Properties;

import static net.odoframework.container.injection.BeanDefinition.bean;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ModuleBuilderTest extends ModuleBuilder {


    @Test
    void testFooBarVerbose() {
        var barComponent = BeanDefinition.bean(Bar.class).with(Bar::new);
        var props = new SimpleMapSource(Map.of(
        "foo.title", "My cool Foo component"));


        var fooComponent = BeanDefinition
                .bean(Foo.class)
                .with(refs -> new Foo(refs.value("foo.title")))
                .set(Bar.class, Foo::setBar);

        var container = new Container(new Configuration(props));
        container.register(barComponent);
        container.register(fooComponent);

        var fooInstance = container.resolve(Foo.class).orElseThrow();
        fooInstance.doStuff();

    }

    @Test
    void testFooBarFluent() {
        var props = new SimpleMapSource(Map.of(
                "foo.title", "My cool Foo component"));
        var container = new Container(new Configuration(props));
        container
                .register(
                        bean(Bar.class)
                                .with(Bar::new)
                )
                .register(
                        bean(Foo.class)
                                .with(refs -> new Foo(refs.value("foo.title")))
                                .set(Bar.class, Foo::setBar)
                );
        var fooInstance = container.resolve(Foo.class).orElseThrow();
        fooInstance.doStuff();

    }

    @Override
    public void build() {


        register(bean(ReferencedComponent.class));
        register(bean(AnotherReferencedComponent.class).with(AnotherReferencedComponent::new));
        register(bean("third").with(ThirdComponent::new));
        register(bean(MyCoolComponent.class).with(it ->
                new MyCoolComponent(
                        it.value("odo.profile"),
                        it.references(AnotherReferencedComponent.class)
                )
        )
                .set(ReferencedComponent.class, MyCoolComponent::setReferencedComponent)
                .set("third", MyCoolComponent::setThirdComponent));

    }

    @Test
    void get() {

        var props = new SimpleMapSource(Map.of(
                "odo.profile", "test"));
        final var config = new Configuration(props);
        var root = new Container(config);
        var container = apply(root, config);
        var bean = container.resolve(MyCoolComponent.class).orElseThrow();
        assertEquals("test", bean.test);
        assertNotNull(bean.referencedComponent);
        assertNotNull(bean.anotherReferencedComponent);
        assertNotNull(bean.thirdComponent);
    }


    private void postContainerCreated(Container container) {

    }
}
