package net.odoframework.container.injection;

public class Foo {

    private final String title;
    private Bar bar;

    public Foo(String title) {
        this.title = title;
    }

    public void setBar(Bar bar) {
        this.bar = bar;
    }

    public void doStuff() {
        System.out.println(title + " is doing stuff with Bar");
        bar.doStuff();
    }
}
