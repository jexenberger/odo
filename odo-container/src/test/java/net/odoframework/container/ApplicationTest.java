package net.odoframework.container;

import net.odoframework.container.injection.Foo;
import org.junit.jupiter.api.Test;

public class ApplicationTest {

    @Test
    void testApplication() {

        System.setProperty("odo.profile", "dev");
        var application = new FooBarApplication();
        var container = application.getContainer();
        var fooBar = container.resolve(Foo.class).orElseThrow();
        fooBar.doStuff();

    }
}
