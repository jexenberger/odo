package net.odoframework.container;


import net.odoframework.container.injection.Bar;
import net.odoframework.container.injection.Foo;

public class FooBarApplication extends Application {
    @Override
    public void build() {
        provides(Bar.class)
                .with(Bar::new);

        provides(Foo.class)
                .with(it -> new Foo(it.value("foo.title")))
                .set(Bar.class, Foo::setBar);
    }
}
