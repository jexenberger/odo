package net.odoframework.container.events;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LogTest {

    @Test
    void logs() {

        final var called = new AtomicInteger(0);
        EventPublisher.clearLogBuffer();
        EventPublisher.clearHandlers();
        EventPublisher.handler(Log.class, it -> {
            System.out.println(it);
            called.incrementAndGet();
        });

        //this is to make sure it all behaves without breaking
        Log.info(LogTest.class, "info");
        Log.error(LogTest.class, "error", new RuntimeException("test"));
        Log.warn(LogTest.class, "warn");
        Log.trace(LogTest.class, "trace");
        Log.debug(LogTest.class, "debug");

        assertEquals(5, called.get());
    }
}
