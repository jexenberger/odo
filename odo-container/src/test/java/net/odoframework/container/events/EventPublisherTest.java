package net.odoframework.container.events;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EventPublisherTest {

    @Test
    void accept() {
        final var called = new AtomicBoolean(false);
        var eventPublisher = new EventPublisher();
        eventPublisher.addHandler(String.class, (String e) -> {
            called.set(true);
            System.out.println(e);
            assertEquals("hello world", e);
        });
        eventPublisher.accept("hello world");
        assertTrue(called.get());

    }
}
