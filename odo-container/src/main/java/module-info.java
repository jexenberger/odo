import net.odoframework.annotations.FrameworkModule;
import net.odoframework.container.Module;

@FrameworkModule
module odo.container {
    requires java.sql;
    requires jakarta.inject;
    requires transitive odo.core;
    requires transitive com.google.gson;

    exports net.odoframework.container;
    exports net.odoframework.container.injection;
    exports net.odoframework.container.metrics;
    exports net.odoframework.container.sql;
    exports net.odoframework.container.tx;
    exports net.odoframework.container.util;
    exports net.odoframework.container.events;


    opens net.odoframework.container;
    opens net.odoframework.container.metrics;
    opens net.odoframework.container.sql;
    opens net.odoframework.container.tx;
    opens net.odoframework.container.injection;
    opens net.odoframework.container.util;
    opens net.odoframework.container.events;


    uses Module;

}