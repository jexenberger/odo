package net.odoframework.container.injection;

import java.util.function.Function;

@FunctionalInterface
public interface Constructor<T> extends Function<Container, T> {


    //slight hack to allow this to be a functional interface
    default Container getContainer() {
        return null;
    }

    @SuppressWarnings("unchecked")
    default T get(String name) {
        return (T) getContainer()
                .resolve(name)
                .orElseThrow(() -> new IllegalArgumentException(name + " is not registered in container"));
    }

}
