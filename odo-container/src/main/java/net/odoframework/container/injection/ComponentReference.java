package net.odoframework.container.injection;

import static java.util.Objects.requireNonNull;

public class ComponentReference<T> extends BaseRef<T> {


    public ComponentReference(String name) {
        super(name);
    }

    public ComponentReference(Class<?> type) {
        this(requireNonNull(type, "type is a required parameter").getName());
    }


    @Override
    @SuppressWarnings("unchecked")
    public T get(Container container) {
        return (T) container
                .resolve(getName())
                .orElseThrow(() -> new IllegalArgumentException(getName() + " not registered in container"));
    }
}
