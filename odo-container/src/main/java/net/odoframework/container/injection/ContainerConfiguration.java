package net.odoframework.container.injection;

import net.odoframework.container.Module;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class ContainerConfiguration {

    private ConfigurationProvider defaultProvider;
    private Module rootModule;
    private Map<String, ConfigurationProvider> providers;

    public ContainerConfiguration(Module rootModule) {
        this.rootModule = Objects.requireNonNull(rootModule);
        providers = new LinkedHashMap<>();
        defaultProvider = new ClasspathConfigurationProvider(rootModule);
        register(defaultProvider);
    }

    private ContainerConfiguration register(ConfigurationProvider provider) {
        if (providers == null) {
            providers = new LinkedHashMap<>();
        }
        providers.put(provider.getIdentifier(), provider);
        return this;
    }

    public Optional<String> get(String key, String defaultValue) {

        if (key.contains("::")) {
            var parts = key.split("::");
            var provider = parts[0];
            var configKey = parts[1];
            var configProvider = providers.get(provider);
            if (configProvider == null) {
                throw new IllegalArgumentException(key + " resolves to unregistered provider " + provider);
            }
        }
        return null;
    }
}

    /*

    private Optional<String> interpolate(String value, String defaultValue) {
        var theValue = (value != null) ? value.trim() : "";
        if (theValue.startsWith("${") && theValue.endsWith("}")) {
            theValue = theValue.substring(2);
            theValue = theValue.substring(0, theValue.length()-1);
            if (theValue.contains("|")) {
                var parts = theValue.split(CONFIG_OR);
                return get(parts[0], defaultValue)
                        .or(of(parts[1]));
                if (parts.length > 1 && !p.containsKey(parts[0].trim())) {
                    theValue = p.getProperty(parts[1].trim(), parts[1].trim());
                } else {
                    theValue = p.getProperty(parts[0],"").trim();
                }
            } else {
                theValue = p.getProperty(theValue, "");
            }
        } else {
            return get(theValue, defaultValue);
        }
    }


    private void interpolate(Properties p) {
        var newProperties = new Properties();
        p.forEach((key, value) -> {
            var theValue = (value != null) ? value.toString().trim() : "";
            if (theValue.startsWith("${") && theValue.endsWith("}")) {
                theValue = theValue.substring(2);
                theValue = theValue.substring(0, theValue.length()-1);
                if (theValue.contains("|")) {
                    var parts = theValue.split(CONFIG_OR);
                    if (parts.length > 1 && !p.containsKey(parts[0].trim())) {
                        theValue = p.getProperty(parts[1].trim(), parts[1].trim());
                    } else {
                        theValue = p.getProperty(parts[0],"").trim();
                    }
                } else {
                    theValue = p.getProperty(theValue, "");
                }
            }
            newProperties.setProperty(key.toString(), theValue);
        });
        return newProperties;
    }*/
