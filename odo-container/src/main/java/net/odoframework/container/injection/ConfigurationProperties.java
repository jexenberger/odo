package net.odoframework.container.injection;

import net.odoframework.container.events.Log;
import net.odoframework.util.ConfigLoader;
import net.odoframework.util.EnvironmentUtils;

import java.util.Properties;

import static java.util.Objects.requireNonNull;
import static net.odoframework.util.Strings.isNotBlank;

public class ConfigurationProperties {

    public static final String ODO_PROFILE = "ODO_PROFILE";
    public static final String SYS_ODO_PROFILE = "odo.profile";
    public static final String APPLICATION_PROPERTIES = "/application.properties";
    public static final String ODO_CONFIGURATION = "odo.configuration";

    private final Properties properties;



    public static synchronized ConfigurationProperties getGlobal(Class<?> source) {
        ConfigurationProperties configuration = (ConfigurationProperties) System.getProperties().get(ODO_CONFIGURATION);
        if (configuration == null) {
            configuration = new ConfigurationProperties();
            configuration.loadConfig(source);
            System.getProperties().put(ODO_CONFIGURATION, configuration);
        }
        return configuration;
    }

    public static synchronized void resetGlobal() {
        System.getProperties().remove(ODO_CONFIGURATION);
    }


    public ConfigurationProperties() {
        properties = new Properties();
    }

    public synchronized Properties loadConfig(Class<?> source) {
        var config = getConfig();
        if (!config.isEmpty()) {
            return config;
        }
        requireNonNull(source.getModule());
        var properties = new Properties();
        var actualEnv = EnvironmentUtils.resolve(ODO_PROFILE, SYS_ODO_PROFILE).orElse(null);
        if (isNotBlank(actualEnv)) {
            Log.debug(ConfigurationProperties.class, "SET ODO ENVIRONMENT TO '" + actualEnv + "'");
            var envPropertiesFile = "/application-" + actualEnv.trim() + ".properties";
            Log.debug(ConfigurationProperties.class, "LOADING -> " + envPropertiesFile);
            config.putAll(ConfigLoader.loadProperties(APPLICATION_PROPERTIES, envPropertiesFile));
        } else {
            config.putAll(ConfigLoader.loadProperties(APPLICATION_PROPERTIES));
            Log.warn(ConfigurationProperties.class, "NO ENVIRONMENT CONFIGURED FOR ODO");
        }
        System.getenv().forEach((key, value) -> properties.put("env." + key, value));
        config.putAll(System.getProperties());
        return getConfig();
    }

    public Properties getConfig() {
        return properties;
    }

    public void reset() {
        properties.clear();
    }

    public synchronized static void setConfig(Properties p) {
        System.getProperties().put(ODO_CONFIGURATION, p);
    }


    public  Properties findByPrefix(String prefix, boolean stripPrefix) {
        if (getConfig() == null) {
            loadConfig(ConfigurationProperties.class);
        }
        return ConfigLoader.findByPrefix(getConfig(), prefix, stripPrefix);
    }


}
