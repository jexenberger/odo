package net.odoframework.container.injection;

import net.odoframework.container.Ref;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static net.odoframework.container.injection.References.ref;
import static net.odoframework.util.Strings.requireNotBlank;


public class BeanDefinition<T> implements Function<Container, T> {

    public static final Map<Class, java.lang.reflect.Constructor> CACHED_CONSTRUCTORS = new HashMap<>();

    private Function<ContainerWrapper, T> constructor;
    private final Set<BiConsumer<ContainerWrapper, T>> initializers;
    private BiConsumer<T, ContainerWrapper> postConstruction;
    private final String name;
    private boolean transactional = false;
    private Predicate<Container> condition = (it) -> true;
    private boolean singleton = true;


    public BeanDefinition(String name) {
        this.name = requireNotBlank(name, "name is a required parameter");
        initializers = new LinkedHashSet<>();
    }

    public BeanDefinition(Class<T> type) {
        this(requireNonNull(type, "type is required").getName());
        if (!type.isInterface() && !type.isAnnotation() && !Modifier.isAbstract(type.getModifiers())) {
            this.constructor = constructorFunction(type);
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> java.lang.reflect.Constructor<T> resolveConstructor(Class<T> type) {
        if (!CACHED_CONSTRUCTORS.containsKey(type)) {
            try {
                CACHED_CONSTRUCTORS.put(type, type.getConstructor());
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(e);
            }
        }
        return (java.lang.reflect.Constructor<T>) CACHED_CONSTRUCTORS.get(type);
    }

    public static <T> BeanDefinition<T> bean(String name) {
        return new BeanDefinition<>(name);
    }

    public static <T> BeanDefinition<T> bean(Class<T> type) {
        return new BeanDefinition<>(type);
    }

    public BeanDefinition<T> with(Class<T> type) {
        this.constructor = constructorFunction(type);
        return this;
    }

    public BeanDefinition<T> with(Supplier<T> supplier) {
        this.constructor = wrapper -> supplier.get();
        return this;
    }

    public BeanDefinition<T> with(Function<ContainerWrapper, T> constructor) {
        this.constructor = requireNonNull(constructor, "constructor function is required");
        return this;
    }

    public BeanDefinition<T> initWith(BiConsumer<ContainerWrapper, T> initializer) {
        this.initializers.add(Objects.requireNonNull(initializer, "initializer is a required parameter"));
        return this;
    }

    public BeanDefinition<T> singleton() {
        this.singleton = true;
        return this;
    }

    public BeanDefinition<T> prototype() {
        this.singleton = false;
        return this;
    }

    private Function<ContainerWrapper, T> constructorFunction(Class<T> type) {
        return container -> {
            try {
                return resolveConstructor(type).newInstance();
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        };
    }

    public String getName() {
        return name;
    }

    public boolean isTransactional() {
        return transactional;
    }


    public BeanDefinition<T> condition(Predicate<Container> condition) {
        this.condition = requireNonNull(condition, "condition is a required attribute");
        return this;
    }

    @SuppressWarnings("unchecked")
    public <Z extends T> BeanDefinition<T> after(BiConsumer<Z, ContainerWrapper> consumer) {
        this.postConstruction = (BiConsumer<T, ContainerWrapper>) requireNonNull(consumer, "consumer is a required parameter");
        return this;
    }

    @SuppressWarnings("unchecked")
    public T apply(final Container container) {
        final var containerWrapper = new ContainerWrapper(container);
        var instance = constructor.apply(containerWrapper);
        if (instance == null) {
            throw new IllegalStateException(getName() + " constructor returned a null object");
        }
        for (BiConsumer<ContainerWrapper, T> initializer : initializers) {
            initializer.accept(containerWrapper, instance);
        }
        if (postConstruction != null) {
            postConstruction.accept(instance, containerWrapper);
        }
        return instance;
    }

    @SuppressWarnings("unchecked")
    public <K> BeanDefinition<T> set(Ref<K> ref, BiConsumer<T, K> consumer) {
        this.initWith((wrapper, instance) -> {
            var value = (Supplier<K>) ref.apply(wrapper.getContainer());
            consumer.accept(instance, value.get());
        });
        return this;
    }

    public <K> BeanDefinition<T> set(String name, BiConsumer<T, K> consumer) {
        return set(ref(name), consumer);
    }

    public <K> BeanDefinition<T> set(Class<K> type, BiConsumer<T, K> consumer) {
        return set(ref(type), consumer);
    }

    public Predicate<Container> getCondition() {
        return condition;
    }

    public BeanDefinition<T> register(Container container) {
        Objects.requireNonNull(container, "container is a required parameter").register(this);
        return this;
    }

    public boolean isSingleton() {
        return singleton;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BeanDefinition)) return false;
        BeanDefinition<?> that = (BeanDefinition<?>) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
