package net.odoframework.container.injection;

import java.util.UUID;

import static java.util.Objects.requireNonNull;

public class LiteralRef<T> extends BaseRef<T> {

    private final T value;

    public LiteralRef(String name, T value) {
        super(name);
        this.value = requireNonNull(value, "value cannot be null");
    }

    public LiteralRef(T value) {
        this(UUID.randomUUID().toString(), value);
    }

    @Override
    public T get(Container container) {
        return value;
    }
}
