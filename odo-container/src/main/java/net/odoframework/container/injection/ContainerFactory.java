package net.odoframework.container.injection;

import net.odoframework.util.configuration.Configuration;

import java.util.Objects;
import java.util.function.Function;

public class ContainerFactory {

    private static Function<Configuration, Container> FACTORY = Container::new;


    public static final void setFactory(Function<Configuration, Container> factory) {
        FACTORY = Objects.requireNonNull(factory);
    }

    public static Container create(Configuration p) {
        return FACTORY.apply(p);
    }

}
