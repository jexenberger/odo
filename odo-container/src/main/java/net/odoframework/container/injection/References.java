package net.odoframework.container.injection;

public class References {


    static <T> ComponentReference<T> ref(Class<T> type) {
        return new ComponentReference<>(type.getName());
    }

    static <T> ComponentReference<T> ref(String name) {
        return new ComponentReference<>(name);
    }

}
