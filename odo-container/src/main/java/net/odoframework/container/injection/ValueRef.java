package net.odoframework.container.injection;

@SuppressWarnings("ALL")
public class ValueRef extends BaseRef<String> {

    private String defaultValue;

    public ValueRef(String name, String defaultValue) {
        super(name);
        this.defaultValue = defaultValue;
    }

    @Override
    public String get(Container container) {
        return container.getValue(getName(), this.defaultValue).orElse(null);
    }


}
