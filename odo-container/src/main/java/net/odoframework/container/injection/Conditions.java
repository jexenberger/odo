package net.odoframework.container.injection;


import net.odoframework.container.events.Log;

import java.util.Objects;
import java.util.function.Predicate;

public class Conditions {


    public static Predicate<Container> isBeanAlreadyPresent(String name) {
        return (it) -> {
            var value = it.getDefinitions(name);
            var check = (value != null && value.size() > 1);
            if (check) {
                Log.debug(Conditions.class, name + " is not registered in the container");
            }
            return check;
        };
    }

    public static Predicate<Container> isBeanAlreadyPresent(Class<?> type) {
        return isBeanAlreadyPresent(type.getName());
    }


    public static Predicate<Container> isBeanPresent(String name) {
        return (it) -> it.contains(name);
    }

    public static Predicate<Container> isBeanPresent(Class<?> type) {
        return isBeanPresent(type.getName());
    }

    public static Predicate<Container> configEquals(String key, String value) {
        return it -> it.getValue(key)
                .filter(val -> Objects.equals(val, value))
                .isPresent();
    }

    public static Predicate<Container> isConfigPresent(String key) {
        return it -> {
            final var value = it.getValue(key);
            if (value.isEmpty()) {
                Log.debug(Conditions.class, key + " is not configured as a value");
            }
            return value.isPresent();
        };
    }


}
