package net.odoframework.container.injection;

import jakarta.inject.Provider;
import net.odoframework.beans.types.Converters;
import net.odoframework.container.tx.TxManager;
import net.odoframework.util.Resource;

import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static net.odoframework.util.Strings.requireNotBlank;

public class ContainerWrapper {

    private final Container container;

    public ContainerWrapper(Container container) {
        this.container = requireNonNull(container, "container is a required parameter");
    }

    public Container getContainer() {
        return container;
    }

    public final String value(ConfigurationReference ref) {
        return this.container
                .getValue(requireNonNull(ref).getName())
                .orElseThrow(() -> new IllegalArgumentException(ref.getName() + " is not a valid configuration property"));
    }

    public final String value(String name) {
        return this.container
                .getValue(requireNotBlank(name, "name is a required parameter"))
                .orElse(null);
    }

    public final String value(String name, String defaultValue) {
        return this.container
                .getValue(requireNotBlank(name, "name is a required parameter"))
                .orElse(defaultValue);
    }

    public final int valueAsInt(String name, int defaultValue) {
        return this.container
                .getValueInteger(requireNotBlank(name, "name is a required parameter"))
                .orElse(defaultValue);
    }

    public final long valueAsLong(String name, long defaultValue) {
        return this.container
                .getValueLong(requireNotBlank(name, "name is a required parameter"))
                .orElse(defaultValue);
    }

    public final Resource valueAsResource(String name, Resource resource) {
        return this.getContainer()
                .getValue(name)
                .map(Converters.STRING_TO_RESOURCE)
                .orElse(resource);
    }

    public final double valueAsDouble(String name, double defaultValue) {
        return this.container
                .getValueDouble(requireNotBlank(name, "name is a required parameter"))
                .orElse(defaultValue);
    }

    public final boolean valueAsBoolean(String name, boolean defaultValue) {
        return this.container
                .getValueBoolean(requireNotBlank(name, "name is a required parameter"))
                .orElse(defaultValue);
    }

    @SuppressWarnings("unchecked")
    public final <T> T references(ComponentReference<T> ref) {
        return (T) this.container
                .resolve(requireNonNull(ref).getName())
                .orElseThrow(() -> new IllegalArgumentException(ref.getName() + " is not a valid configuration property"));
    }

    @SuppressWarnings("unchecked")
    public final <T> T references(String name) {
        return (T) this.container
                .resolve(requireNonNull(References.ref(name)).getName())
                .orElseThrow(() -> new IllegalArgumentException(name + " is not a valid registered bean"));
    }


    @SuppressWarnings("unchecked")
    public final <T> T references(Class<T> type) {
        return references(requireNonNull(type).getName());
    }

    public final <T> Optional<T> get(ComponentReference<T> ref) {
        return this.container
                .resolve(requireNonNull(ref).getName());
    }

    public final <T> Optional<T> get(String name) {
        return this.container
                .resolve(requireNonNull(References.ref(name)).getName());
    }


    public final <T> Optional<T> get(Class<T> type) {
        return references(requireNonNull(type).getName());
    }

    @SuppressWarnings("unchecked")
    public <T> Provider<T> lazyReference(String name) {
        return () -> (T) container.resolve(name).orElseThrow(() -> new IllegalArgumentException(name + " is not a valid registered bean"));


    }

    @SuppressWarnings("unchecked")
    public <T> Provider<T> lazyReference(Class<T> type) {
        return lazyReference(type.getName());


    }

    public Provider<Optional<TxManager>> getLazyBean(Class<TxManager> txManagerClass) {
        return container.getLazyBean(txManagerClass);
    }
}
