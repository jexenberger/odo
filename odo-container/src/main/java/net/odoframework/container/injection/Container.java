package net.odoframework.container.injection;

import jakarta.inject.Provider;
import jakarta.inject.Singleton;
import net.odoframework.container.Application;
import net.odoframework.container.Module;
import net.odoframework.container.events.EventPublisher;
import net.odoframework.container.events.Log;
import net.odoframework.container.metrics.MetricsService;
import net.odoframework.container.tx.TxResource;
import net.odoframework.container.tx.TxScope;
import net.odoframework.util.Strings;
import net.odoframework.util.configuration.Configuration;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.*;

public class Container {


    private static Container SINGLETON_CONTAINER = null;
    private static Supplier<Container> CONTAINER_SUPPLIER = null;
    private final Map<String, Stack<BeanDefinition<?>>> instances;
    private final Map<String, Object> singletons;
    private final Configuration configuration;
    private Set<String> startedBeans;


    public Container(Configuration configuration) {
        instances = new LinkedHashMap<>();
        singletons = new LinkedHashMap<>();
        this.configuration = ofNullable(configuration).orElse(new Configuration());
        register(new BeanDefinition<Consumer<?>>(EventPublisher.class.getName()).with(EventPublisher::getInstance).singleton());
    }

    public static void setContainerBuilder(Supplier<Container> supplier) {
        CONTAINER_SUPPLIER = requireNonNull(supplier, "container supplier is a required parameter");
        getContainerInstance();
    }

    public static synchronized Container getContainerInstance() {
        if (SINGLETON_CONTAINER == null) {
            if (CONTAINER_SUPPLIER != null) {
                SINGLETON_CONTAINER = CONTAINER_SUPPLIER.get();
            }

            SINGLETON_CONTAINER = getModuleContainer();
        }
        return SINGLETON_CONTAINER;
    }

    public static void setContainerInstance(Container singletonContainer) {
        SINGLETON_CONTAINER = singletonContainer;
    }

    public static synchronized Container getModuleContainer() {
        return ServiceLoader.load(Module.class)
                .stream()
                .peek(it -> Log.trace(Container.class, "FOUND MODULE -> " + it.type().getName()))
                .filter(it -> Application.class.isAssignableFrom(it.type()))
                .peek(it -> Log.debug(Container.class, "LOADING -> " + it.type().getName()))
                .map(ServiceLoader.Provider::get)
                .map(it -> ((Application) it).getContainer())
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Could not find an instance of " + Application.class.getName() + " via loaded modules"));
    }

    public <T> Optional<T> resolve(Class<T> type) {
        return resolve(requireNonNull(type.getName(), "type is a required parameter"));
    }

    public <T extends Consumer<?>> Container registerEventHandler(Class<?> eventType, BeanDefinition<T> beanDefinition) {
        register(beanDefinition);
        //throw should technically never happen
        resolve(EventPublisher.class).orElseThrow().addHandler(eventType, () -> (Consumer<?>) resolve(beanDefinition.getName()).orElseThrow());
        return this;
    }

    public boolean contains(Class<?> name) {
        return instances.containsKey(name.getName());
    }

    public boolean contains(String name) {
        return instances.containsKey(name);
    }

    Collection<BeanDefinition<?>> getDefinitions(String name) {
        return instances.get(name);
    }

    @SuppressWarnings("unchecked")
    public <T> Optional<T> resolve(String name) {
        return MetricsService.getMetrics().doSection(name, () -> {
            final var componentList = instances.get(Strings.requireNotBlank(name, "name is a required parameter"));
            if (componentList == null || componentList.empty()) {
                return empty();
            }
            BeanDefinition<?> component = null;
            for (BeanDefinition<?> beanDefinition : componentList) {
                if (beanDefinition.getCondition().test(this)) {
                    component = beanDefinition;
                }
            }
            if (component == null) {
                return empty();
            }
            if (component.isTransactional()) {
                final var finalComponent = component;
                try {
                    return of(TxScope.define(component.getName(), () -> createInstance(name, finalComponent)));
                } finally {
                    TxScope.endScope();
                }
            }
            return of(createInstance(component.getName(), component));
        });

    }

    @SuppressWarnings("unchecked")
    private <T> T createInstance(String name, BeanDefinition<?> component) {
        if (singletons.containsKey(name)) {
            return (T) singletons.get(name);
        }
        var instance = (T) component.apply(this);
        final var singleton = isSingleton(component, instance);
        if (singleton) {
            singletons.put(name, instance);
        }
        runStart(name, instance, singleton);
        if (instance instanceof TxResource) {
            TxScope.addResource((TxResource) instance);
        }
        if (instance instanceof Runnable) {
            runStart(name, instance, singleton);
        }
        return instance;
    }

    private <T> boolean isSingleton(BeanDefinition<?> component, T instance) {
        return component.isSingleton() || instance.getClass().isAnnotationPresent(Singleton.class);
    }

    private <T> void runStart(String name, T instance, boolean isSingleton) {
        if (this.startedBeans == null) {
            this.startedBeans = new HashSet<>();
        }
        if (instance instanceof Runnable && !this.startedBeans.contains(name)) {
            Log.debug(Container.class, "Running " + name + " as startup");
            ((Runnable) instance).run();
            this.startedBeans.add(name);
        }
        if (instance instanceof Runnable && !isSingleton) {
            Log.trace(Container.class, "Running " + name + " as startup in prototype mode");
            ((Runnable) instance).run();
        }
    }

    public Container register(BeanDefinition<?> beanDefinition) {
        requireNonNull(beanDefinition, "componentDefinition is a required parameter");
        Log.debug(Container.class, "Registering bean: " + Strings.requireNotBlank(beanDefinition.getName(), "name is required on the component"));
        if (this.instances.containsKey(beanDefinition.getName())) {
            Log.trace(Container.class, "Container already contains bean: " + beanDefinition.getName());
        } else {
            this.instances.put(beanDefinition.getName(), new Stack<>());
        }
        this.instances.get(beanDefinition.getName()).push(beanDefinition);
        return this;
    }

    public Optional<String> getValue(String name) {
        return Optional.ofNullable(this.configuration.value(Strings.requireNotBlank(name, "name parameter cannot be null")));
    }

    public Optional<Integer> getValueInteger(String name) {
        return Optional
                .ofNullable(this.configuration.valueAsInt(Strings.requireNotBlank(name, "name parameter cannot be null")));
    }

    public Optional<Boolean> getValueBoolean(String name) {
        return Optional
                .ofNullable(this.configuration.valueAsBoolean(Strings.requireNotBlank(name, "name parameter cannot be null")));
    }

    public Optional<String> getValue(String name, String defaultValue) {
        return Optional.ofNullable(this.configuration.value(Strings.requireNotBlank(name, "name parameter cannot be null"), defaultValue));
    }

    //hack for Lambda
    private Container getContainer() {
        return this;
    }

    public <T> Provider<Optional<T>> getLazyBean(String name) {
        return () -> getContainer().resolve(name);
    }

    public <T> Provider<Optional<T>> getLazyBean(Class<T> type) {
        return getLazyBean(requireNonNull(type, "type is a required parameter").getName());
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public Optional<Long> getValueLong(String value) {
        return getValue(value).map(Long::parseLong);
    }

    public Optional<Double> getValueDouble(String value) {
        return getValue(value).map(Double::parseDouble);
    }
}
