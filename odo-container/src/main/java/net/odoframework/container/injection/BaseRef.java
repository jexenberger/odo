package net.odoframework.container.injection;

import net.odoframework.container.Ref;

import java.util.Objects;

import static net.odoframework.util.Strings.requireNotBlank;

public abstract class BaseRef<T> implements Ref<T> {

    private final String name;

    public BaseRef(String name) {
        this.name = requireNotBlank(name, "name is a required parameter");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseRef)) return false;
        BaseRef<?> baseRef = (BaseRef<?>) o;
        return name.equals(baseRef.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
