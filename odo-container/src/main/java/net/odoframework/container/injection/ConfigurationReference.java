package net.odoframework.container.injection;

public class ConfigurationReference extends BaseRef<String> {

    public ConfigurationReference(String name) {
        super(name);
    }

    @Override
    public String get(Container container) {
        return container.getValue(getName()).orElseThrow(() -> new IllegalArgumentException(getName() + " is not found in configuration"));
    }
}
