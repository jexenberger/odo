package net.odoframework.container.injection;

import net.odoframework.container.Module;
import net.odoframework.container.events.Log;
import net.odoframework.util.ConfigLoader;
import net.odoframework.util.EnvironmentUtils;

import java.lang.reflect.AnnotatedElement;
import java.util.Optional;
import java.util.Properties;

import static java.util.Objects.requireNonNull;
import static net.odoframework.util.Strings.isNotBlank;

public class ClasspathConfigurationProvider extends ConfigurationProvider{

    public static final String ODO_PROFILE = "ODO_PROFILE";
    public static final String SYS_ODO_PROFILE = "odo.profile";
    public static final String APPLICATION_PROPERTIES = "/application.properties";

    private  Properties properties;


    public ClasspathConfigurationProvider(Module rootModule) {
        super(rootModule);
        loadConfig(rootModule.getClass());

    }

    @Override
    public Optional<String> get(String key) {
        return Optional.empty();
    }

    @Override
    public String getIdentifier() {
        return "classpath";
    }

    public synchronized Properties loadConfig(AnnotatedElement source) {
        if (properties != null) {
            return properties;
        }
        var config = new Properties();
        var actualEnv = EnvironmentUtils.resolve(ODO_PROFILE, SYS_ODO_PROFILE).orElse(null);
        if (isNotBlank(actualEnv)) {
            Log.debug(ConfigurationProperties.class, "SET ODO ENVIRONMENT TO '" + actualEnv + "'");
            var envPropertiesFile = "/application-" + actualEnv.trim() + ".properties";
            Log.debug(ConfigurationProperties.class, "LOADING -> " + envPropertiesFile);
            config.putAll(ConfigLoader.loadProperties(APPLICATION_PROPERTIES, envPropertiesFile));
        } else {
            config.putAll(ConfigLoader.loadProperties(APPLICATION_PROPERTIES));
            Log.warn(ConfigurationProperties.class, "NO ENVIRONMENT CONFIGURED FOR ODO");
        }
        System.getenv().forEach((key, value) -> properties.put("env." + key, value));
        config.putAll(System.getProperties());
        properties = config;
        return properties;
    }
}
