package net.odoframework.container.injection;

import net.odoframework.container.Module;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

abstract class ConfigurationProvider {

    private Module rootModule;

    public ConfigurationProvider(Module rootModule) {
        this.rootModule = requireNonNull(rootModule);
    }

    public Module getRootModule() {
        return rootModule;
    }

    abstract Optional<String> get(String key);

    abstract String getIdentifier();


    protected void reload() {
        throw new UnsupportedOperationException("reload not supported");
    }


}
