package net.odoframework.container.util;

import java.util.Map;

/**
 * JSON marshalling and unmarshalling service that is used by ODO both internally and can be used by client applications
 */
public interface Json {

    /**
     * Marshals an instance of an object to a {@link String}
     *
     * @param instance to marshal to string, will throw {@link NullPointerException} if the parameter is null
     * @return and instance of the instance parameter in a JSON string
     */
    String marshal(Object instance);

    /**
     * Converts a json string into an instance of the given target type
     *
     * @param json   String of JSON to convert
     * @param target type of the target to convert the json to
     * @param <T>    the return type of the class
     * @return an instance of the converted json string
     */
    <T> T unmarshal(String json, Class<T> target);

    /**
     * convenience method to convert json to a {@link Map} instance
     *
     * @param json the json to convert to a Map
     * @return the unmarshalled json in a Map
     */
    @SuppressWarnings("unchecked")
    default Map<String, Object> unmarshalToMap(String json) {
        return unmarshal(json, Map.class);
    }

}
