package net.odoframework.container.tx;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Map;

public class RollbackFailedException extends RuntimeException {
    private final Map<TxResource, Exception> causes;

    public RollbackFailedException(String message, Map<TxResource, Exception> causes) {
        super(message);
        this.causes = causes;
    }


    @Override
    public void printStackTrace() {
        printStackTrace(System.out);
    }

    @Override
    public void printStackTrace(PrintWriter writer) {
        this.printStackTrace(writer);
        for (Map.Entry<TxResource, Exception> txResourceExceptionEntry : causes.entrySet()) {
            writer.println(txResourceExceptionEntry.getKey().getName() + " :");
            txResourceExceptionEntry.getValue().printStackTrace(writer);
        }
    }

    @Override
    public void printStackTrace(PrintStream s) {
        this.printStackTrace(s);
        for (Map.Entry<TxResource, Exception> txResourceExceptionEntry : causes.entrySet()) {
            s.println(txResourceExceptionEntry.getKey().getName() + " :");
            txResourceExceptionEntry.getValue().printStackTrace(s);
        }
    }
}
