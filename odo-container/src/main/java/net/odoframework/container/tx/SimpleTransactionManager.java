package net.odoframework.container.tx;

import jakarta.inject.Singleton;

import java.util.Optional;
import java.util.UUID;

@Singleton
public class SimpleTransactionManager extends TxManager {

    private static final ThreadLocal<Transaction> ACTIVE_TX = new ThreadLocal<>();

    @Override
    public Optional<Transaction> getActiveTransaction() {
        return Optional.ofNullable(ACTIVE_TX.get());
    }

    @Override
    public Transaction begin() {
        if (ACTIVE_TX.get() != null) {
            throw new IllegalStateException("Transaction already in progress");
        }
        var tx = new Transaction(UUID.randomUUID().toString());
        ACTIVE_TX.set(tx);
        return tx;
    }


    public void commit() {
        SimpleTransactionManager.ACTIVE_TX.remove();
    }

    @Override
    public void rollback() {
        SimpleTransactionManager.ACTIVE_TX.remove();
    }

}
