package net.odoframework.container.tx;

import net.odoframework.util.Pair;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import static net.odoframework.util.Pair.cons;

public class TxScope {

    private static final Map<String, TxScope> TX_SCOPES = new ConcurrentHashMap<>();
    private static final ThreadLocal<Pair<String, TxScope>> SCOPE = new ThreadLocal<>();
    private final Set<TxResource> resources = new LinkedHashSet<>();

    public static Optional<Pair<String, TxScope>> getCurrentScope() {
        return Optional.ofNullable(SCOPE.get());
    }

    public synchronized static TxScope startScope(String name) {
        var scope = new TxScope();
        TX_SCOPES.put(name, scope);
        SCOPE.set(cons(name, scope));
        return scope;
    }

    public synchronized static void endScope() {
        SCOPE.remove();
    }

    public static <T> T define(String name, Supplier<T> runnable) {

        var scope = (SCOPE.get() != null)
                ? SCOPE.get().getRight()
                : startScope(name);
        final var result = runnable.get();
        return result;
    }

    public static void addResource(final TxResource txResource) {
        getCurrentScope().ifPresent(it -> it.getRight().add(txResource));
    }

    public static Optional<TxScope> get(String name) {
        return Optional.ofNullable(TX_SCOPES.get(name));
    }

    public void add(TxResource resource) {
        this.resources.add(resource);
    }

    public Set<TxResource> getResources() {
        return Collections.unmodifiableSet(resources);
    }
}
