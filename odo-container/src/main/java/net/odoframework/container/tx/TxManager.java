package net.odoframework.container.tx;


import java.util.Optional;
import java.util.function.Supplier;

public abstract class TxManager {

    public abstract Optional<Transaction> getActiveTransaction();

    public abstract Transaction begin();

    public <T> T doInTransaction(Class<?> type, Supplier<T> handler) {
        return doInTransaction(type.getName(), handler);
    }

    public <T> T doInTransaction(String name, Supplier<T> handler) {
        var scope = TxScope.get(name).orElse(null);
        if (scope == null) {
            return handler.get();
        }
        var tx = getActiveTransaction().orElse(begin());
        scope.getResources().forEach(tx::enlist);
        try {
            final var result = handler.get();
            if (result instanceof FailureAware && ((FailureAware) result).isFailed()) {
                tx.markRollback();
            }
            return result;
        } catch (RuntimeException e) {
            tx.markRollback();
            throw e;
        } finally {
            if (!tx.isMarkedRollback()) {
                tx.commit();
            } else {
                tx.rollback();
            }
        }

    }


    public abstract void commit();

    public abstract void rollback();
}
