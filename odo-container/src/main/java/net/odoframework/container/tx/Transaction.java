package net.odoframework.container.tx;


import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class Transaction {

    private final String id;
    private final boolean active;
    private boolean isMarkedRollback = false;

    private final Set<TxResource> txResources;

    public Transaction(String id) {
        this.id = id;
        this.active = false;
        this.txResources = new LinkedHashSet<>();
    }

    public boolean isActive() {
        return active;
    }

    public void enlist(TxResource txResource) {
        this.txResources.add(Objects.requireNonNull(txResource));
        txResource.begin(this);
    }

    public void commit() {
        if (this.isMarkedRollback) {
            return;
        }
        for (var txResource : txResources) {
            txResource.commit(this);
        }


    }

    public void markRollback() {
        this.isMarkedRollback = true;
    }

    public void rollback() {
        this.isMarkedRollback = true;
        var rollbackFailures = new LinkedHashMap<TxResource, Exception>();
        for (var txResource : txResources) {
            try {
                txResource.rollback(this);
            } catch (Exception e) {
                rollbackFailures.put(txResource, e);
            }
        }
        if (!rollbackFailures.isEmpty()) {
            throw new RollbackFailedException("TRANSACTION FAILED TO ROLLBACK", rollbackFailures);
        }
    }

    public boolean isMarkedRollback() {
        return isMarkedRollback;
    }
}
