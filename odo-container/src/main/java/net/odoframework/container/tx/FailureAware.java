package net.odoframework.container.tx;

public interface FailureAware {

    boolean isFailed();

}
