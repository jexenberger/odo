package net.odoframework.container.tx;

public interface TxResource {


    String getName();

    void begin(Transaction tx);

    void commit(Transaction tx);

    void rollback(Transaction tx);
}
