package net.odoframework.container;

import net.odoframework.container.injection.Container;
import net.odoframework.container.injection.LiteralRef;
import net.odoframework.container.injection.ValueRef;

import java.util.function.Function;
import java.util.function.Supplier;

public interface Ref<T> extends Function<Container, Supplier<T>> {
    static Ref<String> value(String name, String defaultValue) {
        return new ValueRef(name, defaultValue);
    }

    static Ref<String> value(String name) {
        return new ValueRef(name, null);
    }

    static <T> Ref<T> literal(T value) {
        return new LiteralRef<>(value);
    }

    String getName();

    T get(Container container);

    @Override
    default Supplier<T> apply(Container container) {
        return () -> this.get(container);
    }


}
