package net.odoframework.container;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jakarta.inject.Singleton;
import net.odoframework.container.util.Json;
import net.odoframework.util.Strings;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * {@link Gson} service proder for {@link Json}
 */
@Singleton
public class GsonJson implements Json, Runnable {

    private Gson gson;
    private boolean prettyPrint = true;

    public GsonJson() {
        this(true);
    }

    public GsonJson(boolean prettyPrint) {
        this.prettyPrint = prettyPrint;
        final var builder = new GsonBuilder();
        if (this.prettyPrint) {
            builder.setPrettyPrinting();
        }
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer());
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer());
        builder.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer());
        builder.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer());
        gson = builder.create();
    }

    @Override
    public String marshal(Object instance) {
        return gson.toJson(Objects.requireNonNull(instance, "instance is a required parameter"));
    }

    @Override
    public <T> T unmarshal(String json, Class<T> target) {
        return gson.fromJson(
                Strings.requireNotBlank(json, "json is a required parameter"),
                Objects.requireNonNull(target, "target is a required parameter")
        );
    }

    @Override
    public void run() {
        gson = (prettyPrint) ? new GsonBuilder().setPrettyPrinting().create() : new GsonBuilder().create();
    }
}
