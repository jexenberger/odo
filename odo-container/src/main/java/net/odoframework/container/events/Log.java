package net.odoframework.container.events;

import net.odoframework.container.injection.ConfigurationProperties;

import java.util.Objects;

public class Log extends TimestampEvent {

    private final Level level;
    private final String loggerName;
    private final Object message;
    private final Throwable error;
    Log(Level level, String loggerName, Object message, Throwable error) {
        super();
        this.level = level;
        this.loggerName = loggerName;
        this.message = message;
        this.error = error;
    }

    public static void trace(Class<?> logger, Object message) {
        trace(logger.getName(), message);
    }

    public static void trace(String logger, Object message) {
        publishLog(logger, message, Level.trace, null);
    }

    public static void debug(String logger, Object message) {
        publishLog(logger, message, Level.debug, null);
    }

    public static void debug(Class<?> logger, Object message) {
        debug(logger.getName(), message);
    }

    public static void info(String logger, Object message) {
        publishLog(logger, message, Level.info, null);
    }

    public static void info(Class<?> logger, Object message) {
        info(logger.getName(), message);
    }

    public static void warn(String logger, Object message) {
        publishLog(logger, message, Level.warn, null);
    }

    public static void warn(Class<?> logger, Object message) {
        warn(logger.getName(), message);
    }

    public static void error(String logger, Object message, Throwable error) {
        publishLog(logger, message, Level.error, error);
    }

    public static void error(Class<?> logger, Object message, Throwable error) {
        error(logger.getName(), message, error);
    }

    public static void publishLog(String logger, Object message, Level level, Throwable error) {
        EventPublisher.publish(new Log(level, Objects.requireNonNull(logger), message, error));
    }

    public Level getLevel() {
        return level;
    }

    public String getLoggerName() {
        return loggerName;
    }

    public Object getMessage() {
        return message;
    }

    public String getMessageAsString() {
        return (getMessage() != null) ? getMessage().toString() : "";
    }

    public Throwable getError() {
        return error;
    }

    public enum Level {
        trace,
        debug,
        info,
        warn,
        error,
        off;

        private final String upperCaseName;

        Level() {
            upperCaseName = name().toUpperCase();
        }

        public static Level getDefaultLevel() {
            return Level.warn;
        }

        public String getUpperCaseName() {
            return upperCaseName;
        }

    }

    @Override
    public String toString() {
        return "Log{" +
                "level=" + level +
                ", loggerName='" + loggerName + '\'' +
                ", message=" + message +
                ", error=" + error +
                '}';
    }
}
