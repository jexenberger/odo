package net.odoframework.container.events;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class ContainerStartedEvent extends TimestampEvent {

    private final ZonedDateTime startedTime;

    public ContainerStartedEvent(ZonedDateTime startedTime) {
        this.startedTime = Objects.requireNonNull(startedTime, "started time is required");
    }


    public long getElapsedTime() {
        return Duration.between(startedTime, getTimestamp()).get(ChronoUnit.MILLIS);
    }
}
