package net.odoframework.container.events;

import net.odoframework.util.Strings;

public class ModuleLoadedEvent extends TimestampEvent {

    private final String name;
    private final String application;

    public ModuleLoadedEvent(String name, String application) {
        super();
        this.name = Strings.requireNotBlank(name, "module name is required");
        this.application = Strings.requireNotBlank(application, "application name is required");
    }

    public String getName() {
        return name;
    }

    public String getApplication() {
        return application;
    }
}
