package net.odoframework.container.events;

import java.time.ZonedDateTime;

public abstract class TimestampEvent {

    private final ZonedDateTime timestamp;

    public TimestampEvent() {
        timestamp = ZonedDateTime.now();
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }
}
