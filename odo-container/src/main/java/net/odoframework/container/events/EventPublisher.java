package net.odoframework.container.events;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class EventPublisher implements Consumer<Object> {

    private static final EventPublisher EVENT_PUBLISHER = new EventPublisher();

    private final ConcurrentMap<Class<?>, List<Supplier<Consumer<?>>>> eventHandlers;

    private final List<Log> logBuffer = new ArrayList<>();


    public EventPublisher() {
        eventHandlers = new ConcurrentHashMap<>();
    }

    public static EventPublisher getInstance() {
        return EVENT_PUBLISHER;
    }

    public static void publish(Object event) {
        getInstance().accept(event);
    }

    public static void handler(Class<?> eventType, Supplier<Consumer<?>> eventHandler) {
        getInstance().addHandler(eventType, eventHandler);
    }

    public static void handler(Class<?> eventType, Consumer<?> eventHandler) {
        getInstance().addHandler(eventType, eventHandler);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void accept(Object o) {
        final var check = new AtomicBoolean(false);
        eventHandlers.forEach((clazz, handler) -> {
            if (clazz.equals(o.getClass())) {
                for (var consumer : handler) {
                    Consumer supplier = consumer.get();
                    if (o instanceof Log && !logBuffer.isEmpty()) {
                        synchronized (logBuffer) {
                            logBuffer.forEach(supplier::accept);
                            logBuffer.clear();
                        }
                    }
                    supplier.accept(o);
                    check.set(true);
                }
            }
        });
        if (o instanceof Log && !check.get()) {
            logBuffer.add((Log) o);
        }

    }

    public static synchronized void clearHandlers() {
        EVENT_PUBLISHER.eventHandlers.clear();
    }

    public static synchronized void clearLogBuffer() {
        EVENT_PUBLISHER.clear();
    }

    private synchronized void clear() {
        logBuffer.clear();
    }

    public void addHandler(Class<?> eventType, Supplier<Consumer<?>> eventHandler) {
        var handlers = eventHandlers.computeIfAbsent(eventType, it -> new ArrayList<>(3));
        handlers.add(eventHandler);
    }

    public void addHandler(Class<?> eventType, Consumer<?> eventHandler) {
        addHandler(eventType, () -> eventHandler);
    }

}
