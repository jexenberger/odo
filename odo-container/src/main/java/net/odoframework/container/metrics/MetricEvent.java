package net.odoframework.container.metrics;

import net.odoframework.container.events.TimestampEvent;
import net.odoframework.util.Strings;

public class MetricEvent extends TimestampEvent {

    private final String name;
    private final String description;
    private final long timeTaken;

    public MetricEvent(String name, String description, long timeTaken) {
        this.name = Strings.requireNotBlank(name, "name is required");
        this.description = Strings.requireNotBlank(description, "description");
        this.timeTaken = timeTaken;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public long getTimeTaken() {
        return timeTaken;
    }
}
