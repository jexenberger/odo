package net.odoframework.container.metrics;

import net.odoframework.container.events.EventPublisher;
import net.odoframework.util.Timer;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

public class MetricsService {


    private static Metrics METRICS = new Metrics() {
        @Override
        public <T> T doSection(String name, Supplier<T> handler) {
            var holder = new AtomicReference<T>();
            var timeTaken = Timer.timeTaken(() -> {
                holder.set(handler.get());
            });
            EventPublisher.publish(new MetricEvent(name, name + " metric event", timeTaken));
            return holder.get();
        }
    };

    public static Metrics getMetrics() {
        return METRICS;
    }

    public static void setMetrics(Metrics metrics) {
        METRICS = Objects.requireNonNull(metrics, "metrics is a required parameter");
    }


}
