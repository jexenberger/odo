package net.odoframework.container.metrics;

import java.util.function.Supplier;

public interface Metrics {


    <T> T doSection(String name, Supplier<T> handler);


}
