package net.odoframework.container;

import net.odoframework.container.injection.Container;
import net.odoframework.util.configuration.Configuration;

import java.util.Collections;
import java.util.Properties;
import java.util.Set;
import java.util.function.BiFunction;

/**
 * The Module interface represents a logic grouping of common {@link net.odoframework.container.injection.BeanDefinition} objects which need to be added to the container.
 * <p>
 * Modules are loaded using the {@linkplain java.util.ServiceLoader Java Service Loader} by registering a net.odoframework.container.Module file in META-INF/services or via your module-info.java file in JPMS environments
 * <p>
 * The module is an instance of a {@link BiFunction} and the {@link BiFunction#apply} method is called taking the Container as a parameter.
 * <p>
 * Module authors should not implement this class but rather implement the {@link ModuleBuilder} or {@link Application} classes
 */
public interface Module extends BiFunction<Container, Configuration, Container> {

    /**
     * returns the precedence for when the module needs to be loaded. the lower the returned number the earlier the load precedence.
     * <p>
     * Precedence values below zero are reserved for odo system modules.
     *
     * @return the precedence number to return, default is 0
     */
    default int getPrecedence() {
        return 0;
    }


    default Set<Module> getDependencies() {
        return Collections.emptySet();
    }
}
