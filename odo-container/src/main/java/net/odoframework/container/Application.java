package net.odoframework.container;

import net.odoframework.container.events.EventPublisher;
import net.odoframework.container.events.Log;
import net.odoframework.container.events.ModuleLoadedEvent;
import net.odoframework.container.injection.ConfigurationProperties;
import net.odoframework.container.injection.Container;
import net.odoframework.container.injection.ContainerFactory;
import net.odoframework.util.ListBackedSet;
import net.odoframework.util.configuration.ApplicationPropertiesConfigurationSource;
import net.odoframework.util.configuration.Configuration;

import java.util.Comparator;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class creates a root container for an ODO application. There should at least be one instance of an ApplicationBuilder as a module via the {@link ServiceLoader}
 */
public abstract class Application extends ModuleBuilder {

    private Set<String> loadModules;
    private static Application LOADED_APPLICATION;
    private boolean testMode = false;
    private Configuration configurationProperties;

    public static Application getLoadedApplication() {
        return LOADED_APPLICATION;
    }

    public Application() {
        LOADED_APPLICATION = this;
        load();
    }

    private void load() {
        var config = loadConfiguration();
        if (config.valueAsBoolean("odo.test", false)) {
            testMode = true;
        }
        var container = ContainerFactory.create(config);
        loadModules = new ListBackedSet<>(4);
        apply(container, config);
    }

    protected Configuration loadConfiguration() {
        configurationProperties = new Configuration(new ApplicationPropertiesConfigurationSource());
        return configurationProperties;
    }


    protected void loadManualModules(Container container) {

    }


    protected void loadModule(Container container, Module module) {
        if (!loadModules.contains(module.getClass().getName())) {
            loadModules.add(module.getClass().getName());
            module.apply(container, container.getConfiguration());
        }
    }


    @Override
    protected final void beforeContainerPopulated(Container container) {
        loadManualModules(container);
        final var modules = ServiceLoader
                .load(Module.class);
        Log.debug(Application.class, "DETECTED " + modules.stream().count() + " from the System module loader");
        //check system modules
        var sanitizedModules = modules
                .stream()
                .filter(it -> !it.type().getName().equals(this.getClass().getName()))
                .collect(Collectors.toList());
        for (var moduleProvider : sanitizedModules) {
            var module = moduleProvider.get();
            if (module.getClass().getName().equals(this.getClass().getName())) {
                continue;
            }
            if (!module.getClass().getPackageName().startsWith("net.odoframework") && module.getPrecedence() < 0) {
                throw new IllegalStateException("Module " + module.getClass().getName() + " odo reserves module preferences below 0");
            }
        }

        sanitizedModules
                .stream()
                .sorted(Comparator.comparing(a -> a.get().getPrecedence()))
                .map(ServiceLoader.Provider::get)
                .peek(it -> Log.debug(getClass(), "LOADING MODULE -> " + it.getClass().getName()))
                .peek(it -> it.getDependencies().forEach(dep -> loadModule(container, dep)))
                .peek(it -> loadModule(container, it))
                .forEach(it -> EventPublisher.publish(new ModuleLoadedEvent(it.getClass().getName(), getClass().getName())));
    }


    @Override
    protected final void afterContainerPopulated(Container container) {
        if (!testMode) {
            for (String startupBean : getStartupBeans()) {
                //this works by getting an instance of the bean and then if the component is Runnable it will call run
                container
                        .resolve(startupBean)
                        .map(bean -> {
                            if (!(bean instanceof Runnable)) {
                                throw new IllegalStateException(startupBean + " must be an instance of Runnable to run in startup");
                            }
                            return bean;
                        })
                        .orElseThrow(() -> new IllegalStateException(startupBean + " does not exist but is configured to run in startup"));
            }
        }
        postContainerCreated(container);
    }

    protected void postContainerCreated(Container container) {

    }
}
