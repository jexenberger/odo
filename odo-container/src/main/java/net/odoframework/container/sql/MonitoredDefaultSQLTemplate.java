package net.odoframework.container.sql;

import jakarta.inject.Singleton;
import net.odoframework.container.metrics.Metrics;
import net.odoframework.sql.*;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Singleton
public class MonitoredDefaultSQLTemplate extends DefaultSQLTemplate {

    private final Metrics metrics;

    public MonitoredDefaultSQLTemplate(DataSource dataSource, Metrics metrics) {
        super(dataSource);
        this.metrics = metrics;
    }

    @Override
    public void execute(DBStatement sql, SQLConsumer<ResultSet> rsConsumer) {
        metrics.doSection(SQLTemplate.class.getSimpleName() + ".execute", () -> {
            super.execute(sql, rsConsumer);
            return null;
        });
    }

    @Override
    public int execute(DBStatement sql) {
        return metrics.doSection(SQLTemplate.class.getSimpleName() + ".execute", () -> super.execute(sql));
    }

    @Override
    public <T> Stream<T> stream(DBStatement sql, SQLFunction<ResultSet, T> row) {
        return metrics.doSection(SQLTemplate.class.getSimpleName() + ".stream", () -> super.stream(sql, row));
    }

    @Override
    public <T> T doInTransaction(Supplier<T> supplier) {
        return metrics.doSection(SQLTemplate.class.getSimpleName() + ".doInTransaction", () -> super.doInTransaction(supplier));
    }
}
