package net.odoframework.container.sql;

import jakarta.inject.Singleton;
import net.odoframework.sql.SQLConnectionWrapper;
import net.odoframework.util.ConfigLoader;
import net.odoframework.util.configuration.Configuration;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

import static java.util.Objects.requireNonNull;
import static net.odoframework.util.Strings.*;

@Singleton
public class SimpleDataSource implements DataSource {

    static final String PREFIX = "odo.sql";
    public static final String DRIVER = PREFIX + ".driver";
    public static final String URL = PREFIX + ".url";
    static final String USER = PREFIX + ".user";
    static final String PASSWORD = PREFIX + ".password";
    static final String TIMEOUT = PREFIX + ".timeout";
    private static final Logger LOG = Logger.getLogger(SimpleDataSource.class.getName());
    private String user;
    private String password;
    private String url;
    private Connection connection;
    private PrintWriter log;
    private int timeout = 5;
    private String driver;
    private boolean suppressClose;

    public SimpleDataSource() {
    }

    public SimpleDataSource(String user, String password, String url, int timeout, boolean suppressClose) {
        this.user = requireNonNull(user, "user is a required parameter");
        this.password = password;
        this.url = requireNonNull(url, "url is a required parameter");
        this.timeout = timeout;
        this.suppressClose = suppressClose;
    }

    public void create(String driver, String url, String user, String password, int timeout) {
        requireNotBlank(driver, "driver is a required parameter for SimpleDataSource");
        requireNotBlank(url, "url is a required parameter for SimpleDataSource");
        try {
            LOG.fine(() -> "Loading JDBC DRIVER -> " + driver);
            final Class<?> driverType = Class.forName(driver);
            var driverInstance = (Driver) driverType.getDeclaredConstructor().newInstance();
            DriverManager.registerDriver(driverInstance);
            LOG.fine(() -> "Registered JDBC DRIVER -> " + driver);
        } catch (Exception e) {
            throw new IllegalArgumentException(driver + " does not exist");
        }
        LOG.fine(() -> "Using DB USER -> " + user);
        this.user = user;
        this.password = password;
        LOG.fine(() -> "Using JDBC URL -> " + url);
        this.url = requireNotBlank(url);
        this.timeout = timeout;
        this.driver = driver;
        LOG.fine(() -> "Setting Login Timeout to -> " + this.timeout);
        DriverManager.setLoginTimeout(this.timeout);
        connect(url, user, password);
    }

    private void connect(String url, String user, String password) {
        LOG.fine(() -> "Connecting to DB....");
        log("Connecting to DB....");
        try {
            var connectionProps = new Properties();
            connectionProps.put("user", this.user);
            connectionProps.put("password", this.password);
            final var connection = DriverManager.getConnection(url, connectionProps);
            this.connection = new SQLConnectionWrapper(connection, suppressClose);
            LOG.fine(() -> "Connected!");
            log("Connected!");
        } catch (SQLException throwables) {
            final var errorMessage = throwables.getErrorCode() + ":" + throwables.getMessage();
            LOG.severe(() -> errorMessage);
            log(errorMessage);
            throw new IllegalStateException(throwables);
        }
    }

    private void log(String errorMessage) {
        if (log != null) log.println(errorMessage);
    }

    public boolean isSuppressClose() {
        return suppressClose;
    }

    public synchronized void setSuppressClose(boolean suppressClose) {
        this.suppressClose = suppressClose;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (!isConfigured()) {
            throw new IllegalStateException("Datasource is not configured call create() or init()");
        }
        if (!isInitialised()) {
            create(this.driver, this.url, this.user, this.password, this.timeout);
        }
        return this.connection;
    }

    private boolean isConfigured() {
        return isNotBlank(this.driver) && isNotBlank(this.url);
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        closeExisting();
        connect(this.url, username, password);
        return this.connection;
    }

    private void closeExisting() throws SQLException {
        if (this.isInitialised()) {
            this.connection.close();
        }
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return this.log;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        this.log = out;
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return DriverManager.getLoginTimeout();
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        DriverManager.setLoginTimeout(seconds);
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException("Not supported");
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }


    public void init(Configuration config) {
        LOG.fine(() -> "Loading DB parameters");
        var driver = config.value(DRIVER);
        var url = config.value(URL);
        var user = config.value(USER, "");
        var password = config.value(PASSWORD, "");
        var timeout = config.valueAsInt(TIMEOUT, 5);
        if (isBlank(driver)) {
            LOG.fine(() -> "No URL loaded, not loading DataSource");
            return;
        }
        create(driver, url, user, password, timeout);
    }

    public void shutdown() {
        try {
            this.closeExisting();
        } catch (SQLException e) {
            //break in the fabric of space and time
            e.printStackTrace();
        }
    }

    public boolean isInitialised() {
        try {
            return this.connection != null && !this.connection.isClosed();
        } catch (SQLException e) {
            return false;
        }
    }

}
