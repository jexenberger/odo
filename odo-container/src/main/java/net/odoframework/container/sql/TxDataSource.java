package net.odoframework.container.sql;

import jakarta.inject.Singleton;
import net.odoframework.container.tx.Transaction;
import net.odoframework.container.tx.TxResource;

import java.sql.SQLException;

@Singleton
public class TxDataSource extends SimpleDataSource implements TxResource {
    @Override
    public String getName() {
        return TxDataSource.class.getSimpleName();
    }

    @Override
    public void begin(Transaction tx) {
        try {
            getConnection().setAutoCommit(false);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void commit(Transaction tx) {
        try {
            getConnection().commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void rollback(Transaction tx) {
        try {
            getConnection().rollback();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
