package net.odoframework.container;

import net.odoframework.container.events.EventPublisher;
import net.odoframework.container.injection.BeanDefinition;
import net.odoframework.container.injection.Container;
import net.odoframework.container.metrics.Metrics;
import net.odoframework.container.metrics.MetricsService;
import net.odoframework.util.configuration.Configuration;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;

import static net.odoframework.util.Strings.requireNotBlank;

/**
 * The module builder is a utility class which provides convenience methods for registering {@link BeanDefinition} instances for the Module
 */
public abstract class ModuleBuilder implements Module {

    private static final Set<String> startupBeans = new LinkedHashSet<>();
    private Container container;

    public ModuleBuilder() {
    }

    /**
     * Set of startup beans registered by the {@link ModuleBuilder}
     *
     * @return
     */
    public static Set<String> getStartupBeans() {
        return startupBeans;
    }

    /**
     * returns the set of the modules that are dependant for this Module
     *
     * @return a set of the dependant modules
     */
    @Override
    public Set<Module> getDependencies() {
        return Collections.emptySet();
    }

    /**
     * Register an instance of a {@link BeanDefinition} to be loaded as part of the container
     *
     * @param beanDefinition the bean definition to inject
     * @return self, allowing for fluent builders
     */
    protected ModuleBuilder register(BeanDefinition<?> beanDefinition) {
        this.container.register(beanDefinition);
        return this;
    }

    /**
     * Creates an instance of a {@link BeanDefinition} class which has the name of the parameter
     *
     * @param name the name to register in the container
     * @param <T>  the return type of the {@link BeanDefinition} instance
     * @return the newly created {@link BeanDefinition} instance
     */
    public <T> BeanDefinition<T> provides(String name) {
        final var beanDefinition = new BeanDefinition<T>(name);
        this.register(beanDefinition);
        return beanDefinition;
    }

    /**
     * Registered an Event handler
     *
     * @param eventType
     * @param beanDefinition
     * @param <T>
     * @return
     */
    public <K, T extends Consumer<K>> ModuleBuilder registerEventHandler(Class<K> eventType, BeanDefinition<T> beanDefinition) {
        register(beanDefinition);
        this.container.resolve(EventPublisher.class).orElseThrow().addHandler(eventType, () -> (Consumer<?>) container.resolve(beanDefinition.getName()).orElseThrow());
        return this;
    }

    /**
     * Creates an instance of a {@link BeanDefinition} class which has the name of the passed type
     *
     * @param type the type to use as a name to register in the container
     * @param <T>  the return type of the {@link BeanDefinition} instance
     * @return the newly created {@link BeanDefinition} instance
     */
    public <T> BeanDefinition<T> provides(Class<T> type) {
        final var beanDefinition = new BeanDefinition<T>(type);
        this.register(beanDefinition);
        return beanDefinition;
    }

    /**
     * returns the container associated with this Builder
     *
     * @return
     */
    public Container getContainer() {
        return container;
    }

    @Override
    public final Container apply(Container container, Configuration properties) {
        this.container = container;
        provides(Metrics.class).with(MetricsService::getMetrics);
        beforeContainerPopulated(container);
        build();
        afterContainerPopulated(container);
        return this.container;
    }

    /**
     * Can be used to implement custom logic <b>before</b> the {@link ModuleBuilder#build()} method is invoked
     *
     * @param container the container which will be populated with {@link BeanDefinition} instances
     */
    protected void beforeContainerPopulated(Container container) {

    }

    /**
     * Can be used to implement custom logic <b>after</b> the {@link ModuleBuilder#build()} method is invoked
     *
     * @param container the container which will be populated with {@link BeanDefinition} instances
     */
    protected void afterContainerPopulated(Container container) {

    }

    /**
     * This is used to register a bean which must be run when all the modules have been loaded
     *
     * @param bean the {@link BeanDefinition} instance to run during startup
     * @param <T>  The returned type of the bean must implement {@link Runnable}
     * @return ModuleBuilder instance to allow for fluent builder
     */
    public <T extends Runnable> ModuleBuilder addStartupBean(BeanDefinition<T> bean) {
        register(bean);
        startupBeans.add(requireNotBlank(bean.getName()));
        return this;
    }

    /**
     * This is used to register a bean which must be run when all the modules have been loaded
     *
     * @param type creates a {@link BeanDefinition} with the parameter as a name and adds it as a startup bean
     * @param <T>  The returned type of the bean must implement {@link Runnable}
     * @return ModuleBuilder instance to allow for fluent builder
     */
    public <T extends Runnable> ModuleBuilder addStartupBean(Class<T> type) {
        addStartupBean(BeanDefinition.bean(type));
        startupBeans.add(type.getName());
        return this;
    }

    /**
     * This method is implemented and called as a hook to add {@link BeanDefinition} instances to the module
     */
    public abstract void build();


}
