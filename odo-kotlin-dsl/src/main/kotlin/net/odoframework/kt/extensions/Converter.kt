package net.odoframework.kt.extensions

import java.math.BigDecimal
import java.math.BigInteger

object Converter {

    inline fun <reified T> convert(value: Any?, defaultValue: T? = null): T? {
        return when (T::class.qualifiedName) {
            String::class.qualifiedName -> value?.toString() as? T?
            Int::class.qualifiedName -> value?.toString()?.toInt() as? T?
            Byte::class.qualifiedName -> value?.toString()?.toByte() as? T?
            Short::class.qualifiedName -> value?.toString()?.toShort() as? T?
            Boolean::class.qualifiedName -> value?.toString().toBoolean() as? T?
            Long::class.qualifiedName -> value.toString().toLong() as? T?
            Double::class.qualifiedName -> value.toString().toDouble() as? T?
            Float::class.qualifiedName -> value.toString().toFloat() as? T?
            BigDecimal::class.qualifiedName -> value.toString().toBigDecimal() as? T?
            BigInteger::class.qualifiedName -> value.toString().toBigInteger() as? T?
            else -> value as? T?
        }
    }
}