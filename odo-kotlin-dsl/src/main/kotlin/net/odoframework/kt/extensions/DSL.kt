package net.odoframework.kt.extensions

import net.odoframework.container.ModuleBuilder
import net.odoframework.container.injection.BeanDefinition
import net.odoframework.container.injection.Container
import net.odoframework.container.injection.ContainerWrapper
import net.odoframework.container.util.Json
import net.odoframework.service.web.SimpleWebRequest
import net.odoframework.service.web.SimpleWebResponse
import net.odoframework.service.web.WebResponse
import java.util.*
import java.util.function.Function
import kotlin.reflect.KClass

fun <T : Any> Optional<T>.unwrap(): T? = orElse(null)

fun <K : Any, T : Any> BeanDefinition<T>.set(type: KClass<K>, setter: T.(K) -> Unit): BeanDefinition<T> {
    return this.set(type.java) { a: T, b: K ->
        setter(a, b)
    }
}

inline fun <reified T : Any> Json.toObject(json: String, type: KClass<T> = T::class) = this.unmarshal(json, type.java)



inline fun <reified T : Any> ContainerWrapper.configuration(name:String, type: KClass<T> = T::class): T {
    return value(name)?.let { Converter.convert<T>(it) as T } ?: throw IllegalStateException("$name property cannot be found")
}

fun <T : Any> bean(name:String, constructor: BeanDefinition<T>.(ContainerWrapper) -> T): BeanDefinition<T> {
    val bean = BeanDefinition.bean<T>(name)
    bean.with(Function { bean.constructor(it) })
    return bean
}


infix fun <T : Any> BeanDefinition<T>.inject(init: T.(ContainerWrapper) -> Unit): BeanDefinition<T> {
    this.initWith { wrapper, instance ->
        instance.init(wrapper)
    }
    return this
}


inline fun <reified T> ContainerWrapper.value(property: String, defaultValue: T? = null): T? {
    return Converter.convert<T>(this.value(property)) ?: defaultValue
}


inline fun <reified T> ModuleBuilder.provide(
    name: String,
    crossinline builder: BeanDefinition<T>.(ContainerWrapper) -> T
): BeanDefinition<T> {
    val beanDefinition = provides<T>(name)
    return beanDefinition.with(Function {
        beanDefinition.builder(it)
    })
}

fun request(json:Json, init : SimpleWebRequest.()-> Unit): SimpleWebRequest {
    val request = SimpleWebRequest(json)
    request.init()
    return request
}

fun get(json:Json, init : SimpleWebRequest.()-> Unit): SimpleWebRequest {
    val request = SimpleWebRequest(json)
    request.method = "GET"
    request.init()
    return request
}

fun post(json:Json, init : SimpleWebRequest.()-> Unit): SimpleWebRequest {
    val request = SimpleWebRequest(json)
    request.method = "POST"
    request.init()
    return request
}

fun put(json:Json, init : SimpleWebRequest.()-> Unit): SimpleWebRequest {
    val request = SimpleWebRequest(json)
    request.method = "PUT"
    request.init()
    return request
}

inline fun <reified T> WebResponse.toObject() : T {
    return this.json.toObject(this.body)
}
inline fun <reified T> Json.toObject(body: String) : T {
    return this.unmarshal(body, T::class.java)
}



inline fun <reified T> ModuleBuilder.provide(crossinline builder: BeanDefinition<T>.(ContainerWrapper) -> T): BeanDefinition<T> {
    val beanDefinition = provides(T::class.java)
    return beanDefinition.with(Function {
        beanDefinition.builder(it)
    })
}

operator fun <T : Any> Container.get(name: String): T? = this.resolve<T>(name).unwrap()

inline fun <reified T : Any> Container.get(): T? {
    return this.resolve<T>(T::class.qualifiedName).unwrap()
}

inline fun <reified T : Any> ContainerWrapper.get(): T = this.references(T::class.java)

operator fun <T : Any> ContainerWrapper.get(name: String): T = this.references(name)

