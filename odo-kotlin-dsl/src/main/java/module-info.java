import net.odoframework.annotations.FrameworkModule;

@FrameworkModule
module odo.kotlin.dsl {
    requires kotlin.stdlib.jdk8;
    requires transitive odo.container;
    requires transitive odo.service;
    exports net.odoframework.kt.extensions;
}