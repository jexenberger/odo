package net.odoframework.extensions

import net.odoframework.container.Application
import net.odoframework.kt.extensions.get
import net.odoframework.kt.extensions.inject
import net.odoframework.kt.extensions.provide
import net.odoframework.kt.extensions.value

class AModule : Application() {
    override fun build() {
        provide<String> { "hello world" }
        provide<Bar> { Bar() }
        provide<Foo> { Foo(it.value("foo.title")!!, it.get()) } inject {
            aProperty = it.get()
            aBool = it.value<Boolean>("foo.bool")!!
            aDouble = it.value<Double>("foo.double")!!
            anInt = it.value<Int>("foo.int")!!
            aLong = it.value<Long>("foo.long")!!
        }
    }
}