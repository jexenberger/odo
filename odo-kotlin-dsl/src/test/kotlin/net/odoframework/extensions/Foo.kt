package net.odoframework.extensions

class Foo(val title: String, val bar: Bar) {

    var aProperty: String = ""
    var aBool: Boolean = false
    var anInt: Int = -1
    var aLong: Long = -1
    var aDouble: Double = -1.0

    fun doStuff() {
        println("${title} + is calling Bar ")
        bar.doStuff()
    }

}