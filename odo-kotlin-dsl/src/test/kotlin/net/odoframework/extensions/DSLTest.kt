package net.odoframework.extensions

import net.odoframework.kt.extensions.get
import kotlin.test.assertEquals

class DSLTest {

    @org.junit.jupiter.api.Test
    internal fun provider() {
        val module = AModule()

        val fooBar = module.container.get<Foo>()!!
        fooBar.doStuff()

        assertEquals("hello world", fooBar.aProperty)
        assertEquals(true, fooBar.aBool)
        assertEquals(1, fooBar.anInt)
        assertEquals(9999999999L, fooBar.aLong)
        assertEquals(1.1, fooBar.aDouble)


    }
}