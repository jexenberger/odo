package net.odoframework.jetty.runtime;

import net.odoframework.container.Application;
import net.odoframework.container.injection.Container;
import net.odoframework.service.ServiceModule;

public class TestApplication extends Application {

    public static void main(String[] args) throws Exception {
        JettyServer.main(new String[]{TestApplication.class.getName()});
    }

    @Override
    protected void loadManualModules(Container container) {
        loadModule(container, new ServiceModule());
        loadModule(container, new JettyModule());
    }

    @Override
    public void build() {

    }
}

