import net.odoframework.annotations.FrameworkModule;
import net.odoframework.container.Module;
import net.odoframework.jetty.runtime.JettyModule;
import org.apache.logging.log4j.spi.Provider;

@FrameworkModule
module odo.jetty.runtime {

    requires transitive java.scripting;

    requires transitive odo.core;
    requires transitive odo.container;
    requires transitive odo.service;

    requires transitive jakarta.inject;

    requires transitive org.eclipse.jetty.servlet;
    requires transitive org.eclipse.jetty.server;
    requires transitive jetty.servlet.api;

    requires transitive org.slf4j;
    requires transitive org.apache.logging.log4j;

    requires transitive com.google.gson;


    exports net.odoframework.jetty.runtime;

    opens net.odoframework.jetty.runtime;


    provides Module with JettyModule;

    uses Provider;


}