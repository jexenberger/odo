package net.odoframework.jetty.runtime;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.ResponseConverter;
import net.odoframework.service.web.WebResponse;
import net.odoframework.util.Pair;
import net.odoframework.util.Strings;

import java.io.IOException;

public class HttpServletWebResponseConverter implements ResponseConverter<WebResponse, HttpServletResponse, Pair<HttpServletRequest, HttpServletResponse>> {

    @Override
    public HttpServletResponse encode(WebResponse response, InvocationContext<Pair<HttpServletRequest, HttpServletResponse>> context) {
        var httpServletResponse = context.getRequestContext().getRight();
        httpServletResponse.setStatus(response.getStatusCode());
        httpServletResponse.setHeader("x-odo-request-id", context.getRequestId());
        response.getHeaders().forEach(httpServletResponse::setHeader);
        response.getContentType().ifPresent(httpServletResponse::setContentType);
        if (Strings.isNotBlank(response.getBody())) {
            httpServletResponse.setContentLength(response.getBody().length());
            try {
                httpServletResponse.getWriter().write(response.getBody());
                httpServletResponse.getWriter().flush();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        return httpServletResponse;
    }
}
