package net.odoframework.jetty.runtime;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.odoframework.service.ExceptionHandler;
import net.odoframework.service.InvocationContext;
import net.odoframework.util.Exceptions;
import net.odoframework.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DefaultExceptionHandler implements ExceptionHandler<Pair<HttpServletRequest, HttpServletResponse>, HttpServletResponse> {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @Override
    public HttpServletResponse apply(InvocationContext<Pair<HttpServletRequest, HttpServletResponse>> runtimeContext, Throwable throwable) {
        final var response = runtimeContext.getRequestContext().getRight();
        try {
            final var writer = response.getWriter();
            writer.write(Exceptions.toString(throwable));
            writer.flush();
            response.sendError(500, Exceptions.toString(throwable));
        } catch (IOException e) {
            //tear in the fabric of space and time
            LOG.error(e.getMessage(), e);
        }
        return response;
    }
}
