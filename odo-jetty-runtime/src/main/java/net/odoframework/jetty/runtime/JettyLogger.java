package net.odoframework.jetty.runtime;

import net.odoframework.container.events.Log;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.slf4j.helpers.MessageFormatter;

import java.util.function.Consumer;

public class JettyLogger implements Consumer<Log> {

    public Level getLevel(Log log) {
        switch (log.getLevel()) {
            case trace:
                return Level.TRACE;
            case debug:
                return Level.DEBUG;
            case info:
                return Level.INFO;
            case error:
                return Level.ERROR;
            case warn:
                return Level.WARN;
            default:
                return Level.OFF;
        }
    }


    @Override
    public void accept(Log log) {
        var logger = LogManager.getLogger(log.getLoggerName());
        if (log.getError() != null) {
            logger.error(log.getError());
        } else {
            logger.log(getLevel(log), log.getMessageAsString());
        }
    }
}
