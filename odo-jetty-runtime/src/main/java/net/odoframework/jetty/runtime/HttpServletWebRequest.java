package net.odoframework.jetty.runtime;

import jakarta.servlet.http.HttpServletRequest;
import net.odoframework.container.util.Json;
import net.odoframework.service.web.URNPath;
import net.odoframework.service.web.WebRequest;
import net.odoframework.util.Strings;

import java.io.IOException;
import java.io.StringWriter;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Optional.*;

public class HttpServletWebRequest implements WebRequest {

    private final HttpServletRequest request;
    private final Json json;
    private String body;
    private Map<String, String> pathVariables;

    public HttpServletWebRequest(HttpServletRequest request, Json json) {
        this.request = request;
        this.json = json;
    }

    @Override
    public String getBody() {
        if (body == null) {
            final var stringWriter = new StringWriter();
            try {
                request.getReader().transferTo(stringWriter);
                body = stringWriter.toString();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        return body;
    }

    @Override
    public <T> T getBody(Class<T> type) {
        return json.unmarshal(getBody(), type);
    }

    @Override
    public Optional<List<String>> getMultiValueHeader(String name) {
        var header = request.getHeader(name);
        if (Strings.isBlank(header)) {
            return Optional.empty();
        }
        if (!header.contains(",")) {
            return Optional.empty();
        }
        return of(Arrays.stream(header.split(",")).map(String::trim).collect(Collectors.toList()));
    }

    @Override
    public Optional<Principal> getUserPrincipal() {
        return ofNullable(request.getUserPrincipal());
    }

    @Override
    public Optional<String> getHeader(String name) {
        return ofNullable(request.getHeader(name));
    }

    @Override
    public String getPath() {
        return request.getPathInfo();
    }

    @Override
    public Optional<String> getPathVariable(String name) {
        if (pathVariablesNotSet()) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.pathVariables.get(name));
    }

    @Override
    public String getMethod() {
        return request.getMethod();
    }

    @Override
    public Optional<String> getQueryParam(String name) {

        return Arrays.stream(
                request
                        .getQueryString()
                        .split("&"))
                .map(it -> it.split("="))
                .filter(it -> Objects.equals(name, it[0].trim()))
                .map(it -> it[1].trim())
                .findFirst();
    }

    @Override
    public Optional<List<String>> getMultiValueQueryParam(String name) {
        var result = Arrays.stream(
                request
                        .getQueryString()
                        .split("&"))
                .map(it -> it.split("="))
                .filter(it -> Objects.equals(name, it[0].trim()))
                .map(it -> it[1].trim())
                .collect(Collectors.toList());
        return result.isEmpty() ? empty() : of(result);
    }

    @Override
    public Json getJson() {
        return json;
    }


    @Override
    public boolean matches(String pattern) {
        var pathVariables = URNPath.match(getPath(), pattern);
        if (pathVariablesNotSet() && pathVariables.isPresent()) {
            this.pathVariables = pathVariables.get();
            return true;
        }
        return false;
    }

    public boolean pathVariablesNotSet() {
        return this.pathVariables == null || this.pathVariables.isEmpty();
    }

}
