package net.odoframework.jetty.runtime;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.RequestConverter;
import net.odoframework.util.Pair;

import java.io.IOException;
import java.io.StringWriter;

public class DefaultRequestConverter implements RequestConverter<HttpServletRequest, String, Pair<HttpServletRequest, HttpServletResponse>> {

    @Override
    public String decode(HttpServletRequest request, InvocationContext<Pair<HttpServletRequest, HttpServletResponse>> context) {
        final var stringWriter = new StringWriter();
        try {
            request.getReader().transferTo(stringWriter);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
