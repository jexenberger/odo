package net.odoframework.jetty.runtime;

import jakarta.servlet.Servlet;
import net.odoframework.container.ModuleBuilder;
import net.odoframework.container.Ref;
import net.odoframework.container.events.EventPublisher;
import net.odoframework.container.events.Log;
import net.odoframework.container.injection.Conditions;
import net.odoframework.container.injection.Container;
import net.odoframework.container.util.Json;
import net.odoframework.service.ProviderRuntime;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;


public class JettyModule extends ModuleBuilder {

    public static final String ODO_JETTY_SSL_KEYSTORE = "odo.jetty.ssl.keystore";

    static {
        EventPublisher.handler(Log.class, new JettyLogger());
    }

    @Override
    public void build() {

        provides(SslContextFactory.Server.class).with(SslContextFactory.Server::new)
                .set(Ref.value(ODO_JETTY_SSL_KEYSTORE), SslContextFactory::setKeyStorePath)
                .set(Ref.value("odo.jetty.ssl.keystore.password", "changeit"), SslContextFactory::setKeyStorePassword)
                .set(Ref.value("odo.jetty.ssl.truststore"), SslContextFactory::setTrustStorePath)
                .set(Ref.value("odo.jetty.ssl.truststore.password"), SslContextFactory::setTrustStorePassword)
                .condition(Conditions.isConfigPresent(ODO_JETTY_SSL_KEYSTORE));

        provides(Servlet.class).with(RouterServlet::new);

        provides(ThreadPool.class)
                .with(it -> new QueuedThreadPool(
                        it.valueAsInt("odo.jetty.maxthreads", 100),
                        it.valueAsInt("odo.jetty.minthreads", 10),
                        it.valueAsInt("odo.jetty.idletimeout", 120)
                ));


        provides(ProviderRuntime.class).with(it -> new JettyProviderRuntime(it.references(Json.class)));

        int defaultPort = getContainer().getValue(ODO_JETTY_SSL_KEYSTORE).map(it -> 8443).orElse(8080);

        addStartupBean(
                provides(JettyServer.class)
                        .with(it -> new JettyServer(
                                it.lazyReference(Servlet.class),
                                it.valueAsInt("odo.jetty.port", defaultPort),
                                it.references(ThreadPool.class),
                                it.getContainer().resolve(SslContextFactory.Server.class)
                        ))
        );


    }

    private void postContainerCreated(Container container) {

    }
}
