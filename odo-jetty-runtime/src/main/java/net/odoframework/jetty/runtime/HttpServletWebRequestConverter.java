package net.odoframework.jetty.runtime;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.odoframework.container.util.Json;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.RequestConverter;
import net.odoframework.service.web.WebRequest;
import net.odoframework.util.Pair;

public class HttpServletWebRequestConverter implements RequestConverter<HttpServletRequest, WebRequest, Pair<HttpServletRequest, HttpServletResponse>> {

    private final Json json;

    public HttpServletWebRequestConverter(Json json) {
        this.json = json;
    }

    @Override
    public WebRequest decode(HttpServletRequest request, InvocationContext<Pair<HttpServletRequest, HttpServletResponse>> context) {
        return new HttpServletWebRequest(request, this.json);
    }
}
