package net.odoframework.jetty.runtime;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.odoframework.container.util.Json;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.ResponseConverter;
import net.odoframework.service.web.SimpleWebResponse;
import net.odoframework.service.web.WebResponse;
import net.odoframework.util.Pair;

public class DefaultResponseConverter implements ResponseConverter<Object, HttpServletResponse, Pair<HttpServletRequest, HttpServletResponse>> {

    private final HttpServletWebResponseConverter responseConverter;
    private final Json json;

    public DefaultResponseConverter(Json json) {
        this.responseConverter = new HttpServletWebResponseConverter();
        this.json = json;
    }


    @Override
    public HttpServletResponse encode(Object o, InvocationContext<Pair<HttpServletRequest, HttpServletResponse>> context) {
        if (o == null) {
            return responseConverter.encode(new SimpleWebResponse(json).status(200), context);
        }
        if (o instanceof WebResponse) {
            return responseConverter.encode((WebResponse) o, context);
        } else if (o instanceof String) {
            return responseConverter.encode(new SimpleWebResponse(json).status(200).body(o.toString()), context);
        } else {
            return responseConverter.encode(new SimpleWebResponse(json).status(200).body(json.marshal(o)), context);
        }
    }
}
