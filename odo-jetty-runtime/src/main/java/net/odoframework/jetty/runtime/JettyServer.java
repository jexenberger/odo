package net.odoframework.jetty.runtime;

import jakarta.inject.Provider;
import jakarta.servlet.Servlet;
import net.odoframework.container.Application;
import net.odoframework.container.injection.Container;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;

public class JettyServer implements Runnable {


    private static final Logger LOG = LoggerFactory.getLogger(JettyServer.class.getName());

    private Server server;

    private final Provider<Servlet> routerServlet;
    private final ThreadPool threadPool;
    private final int port;
    private final Optional<SslContextFactory.Server> ssl;


    public JettyServer(Provider<Servlet> routerServlet, int port, ThreadPool threadPool, Optional<SslContextFactory.Server> ssl) {
        this.routerServlet = Objects.requireNonNull(routerServlet);
        if (port < 0) {
            throw new IllegalArgumentException("port and threadPoolSize must be greater than 0");
        }
        this.port = port;
        this.threadPool = Objects.requireNonNull(threadPool);
        this.ssl = ssl;
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                server.stop();
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }));
    }

    public static void main(String[] args) throws Exception {
        if (args.length > 0) {
            var className = args[0].trim();
            final Class<Application> applicationClass = (Class<Application>) Class.forName(className);
            var application = applicationClass.getConstructor().newInstance();
            LOG.info("** STARTED APPLICATION " + applicationClass.getName());

        } else {
            Container.getModuleContainer();
            LOG.info("** STARTED APPLICATION ");
        }
    }

    public void start() {
        server = new Server(threadPool);
        ServerConnector connector = ssl.map(it -> new ServerConnector(server, it)).orElse(new ServerConnector(server));
        connector.setPort(port);
        server.setConnectors(new Connector[]{connector});
        var servletHandler = new ServletHandler();
        servletHandler.addServletWithMapping(new ServletHolder(this.routerServlet.get()), "/*");
        server.setHandler(servletHandler);
        try {
            server.start();
        } catch (Exception e) {
            try {
                if (server.isFailed()) {
                    server.stop();
                    server.join();
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void run() {
        start();
    }


}
