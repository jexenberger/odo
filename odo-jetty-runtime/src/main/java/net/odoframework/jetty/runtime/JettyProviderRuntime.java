package net.odoframework.jetty.runtime;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.odoframework.container.util.Json;
import net.odoframework.service.*;
import net.odoframework.util.Pair;

public class JettyProviderRuntime implements ProviderRuntime<Pair<HttpServletRequest, HttpServletResponse>> {

    private final Json json;

    public JettyProviderRuntime(Json json) {
        this.json = json;
    }

    @Override
    public RequestConverter<?, ?, Pair<HttpServletRequest, HttpServletResponse>> getProviderDefaultRequestConverter() {
        return new HttpServletWebRequestConverter(json);
    }

    @Override
    public ResponseConverter<?, HttpServletResponse, Pair<HttpServletRequest, HttpServletResponse>> getProviderDefaultResponseConverter() {
        return new HttpServletWebResponseConverter();
    }

    @Override
    public ExceptionHandler<Pair<HttpServletRequest, HttpServletResponse>, HttpServletResponse> getProviderDefaultExceptionHandler() {
        return new DefaultExceptionHandler();
    }

    @Override
    public InvocationContext<Pair<HttpServletRequest, HttpServletResponse>> createInvocation(Pair<HttpServletRequest, HttpServletResponse> containerInstance) {
        return new ServletInvocationContext(containerInstance.getLeft(), containerInstance.getRight());
    }

}
