package net.odoframework.jetty.runtime;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.odoframework.service.InvocationContext;
import net.odoframework.util.Pair;

import java.util.UUID;

import static net.odoframework.util.Pair.cons;

public class ServletInvocationContext implements InvocationContext<Pair<HttpServletRequest, HttpServletResponse>> {

    private final Pair<HttpServletRequest, HttpServletResponse> context;
    private final String id;

    public ServletInvocationContext(HttpServletRequest request, HttpServletResponse response) {
        this.context = cons(request, response);
        id = String.join(":", request.getRequestURI(), UUID.randomUUID().toString());

    }

    @Override
    public String getRequestId() {
        return id;
    }

    @Override
    public Pair<HttpServletRequest, HttpServletResponse> getRequestContext() {
        return context;
    }

    @Override
    public Object getRawPayload() {
        return context.getLeft();
    }
}
