import net.odoframework.annotations.FrameworkModule;

@FrameworkModule
module odo.kotlin.service.dsl {
    requires transitive odo.service;
    requires kotlin.stdlib;
    exports net.odoframework.service.extensions;
}