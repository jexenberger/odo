package net.odoframework.service.extensions

import net.odoframework.beans.types.ConverterRegistry
import net.odoframework.service.web.HttpRouter
import net.odoframework.service.web.WebRequest
import net.odoframework.service.web.WebResponse
import kotlin.reflect.KClass


inline fun <reified T : Any> WebRequest.body(type: KClass<T> = T::class) = getBody(type.java)!!

inline fun <reified T : Any> WebRequest.fromPath(name: String): T {
    return getPathVariable(name)?.let {
        if (it.isEmpty) throw IllegalArgumentException("$name is not found")
        return ConverterRegistry
            .get(String::class.java, T::class.java)
            .orElseThrow { IllegalArgumentException("${T::class.java.name} is not supported") }
            .apply(it.get())
    } ?: throw IllegalArgumentException("$name is not found")
}

inline fun HttpRouter.get(path: String, crossinline handler: (WebRequest) -> WebResponse): HttpRouter {
    return get(path) { request, _ ->
        handler(request)
    }
}


inline fun <reified T : Any> HttpRouter.put(
    path: String,
    crossinline handler: (T, WebRequest) -> WebResponse
): HttpRouter {
    return put(path) { request, _ ->
        val body = request.body(T::class)
        handler(body, request)
    }
}

inline fun <reified T : Any> HttpRouter.put(
    path: String,
    crossinline handler: (T, WebRequest, Any) -> WebResponse
): HttpRouter {
    return put(path) { request, invocation ->
        val body = request.body(T::class)
        handler(body, request, invocation)
    }
}


inline fun <reified T : Any> HttpRouter.post(
    path: String,
    crossinline handler: (T, WebRequest) -> WebResponse
): HttpRouter {
    return post(path) { request, _ ->
        val body = request.body(T::class)
        handler(body, request)
    }
}

inline fun <reified T : Any> HttpRouter.post(path: String, crossinline handler: (T) -> WebResponse): HttpRouter {
    return post(path) { request, _ ->
        val body = request.body(T::class)
        handler(body)
    }
}

inline fun <reified T : Any> HttpRouter.post(
    path: String,
    crossinline handler: (T, WebRequest, Any) -> WebResponse
): HttpRouter {
    return post(path) { request, invocation ->
        val body = request.body(T::class)
        handler(body, request, invocation)
    }
}

fun WebResponse.setCors(
    headers: Set<String> = emptySet(),
    origins: Set<String> = emptySet(),
    methods: Set<String> = emptySet()
): WebResponse {
    val actualHeaders = if (headers.isEmpty()) setOf("*") else headers
    val actualOrigins = if (origins.isEmpty()) setOf("*") else origins
    val actualMethods = if (methods.isEmpty()) setOf("*") else methods
    return cors(actualHeaders, actualOrigins, actualMethods)
}

