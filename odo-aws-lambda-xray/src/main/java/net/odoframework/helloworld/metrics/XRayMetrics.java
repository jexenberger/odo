package net.odoframework.helloworld.metrics;

import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.exceptions.SegmentNotFoundException;
import com.amazonaws.xray.exceptions.SubsegmentNotFoundException;
import jakarta.inject.Singleton;
import net.odoframework.container.metrics.Metrics;
import net.odoframework.container.metrics.MetricsService;

import java.util.function.Supplier;
import java.util.logging.Logger;

@Singleton
public class XRayMetrics implements Metrics {


    public static final Logger LOG = Logger.getLogger(XRayMetrics.class.getName());

    public XRayMetrics() {
        MetricsService.setMetrics(this);
    }

    public static void safe(Runnable t) {
        try {
            t.run();
        } catch (SegmentNotFoundException | SubsegmentNotFoundException e) {
            //do nothing, it wont work outside Lambda
        }
    }

    @Override
    public <T> T doSection(String name, Supplier<T> handler) {
        LOG.fine("Starting X-Ray Segment: " + name);
        try {

            safe(() -> AWSXRay.beginSubsegment(name));
            return handler.get();
        } catch (RuntimeException e) {
            safe(() -> AWSXRay.getCurrentSubsegment().addException(e));
            throw e;
        } finally {
            safe(() -> AWSXRay.endSubsegment());
        }
    }
}
