package net.odoframework.helloworld.metrics;

import net.odoframework.container.ModuleBuilder;
import net.odoframework.container.injection.Container;
import net.odoframework.container.metrics.Metrics;
import net.odoframework.container.metrics.MetricsService;

public class OdoAwsLambdaXrayModule extends ModuleBuilder {


    private static final XRayMetrics METRICS = new XRayMetrics();

    static {
        MetricsService.setMetrics(METRICS);
    }


    @Override
    public int getPrecedence() {
        return -888888888;
    }

    @Override
    public void build() {
        provides(Metrics.class).with(() -> METRICS);
    }

    private void postContainerCreated(Container container) {

    }
}
