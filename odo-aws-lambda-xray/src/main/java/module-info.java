import net.odoframework.annotations.FrameworkModule;

@FrameworkModule
module odo.aws.lambda.xray {
    requires odo.aws.lambda.runtime;
    requires aws.xray.recorder.sdk.core;

    requires odo.core;
    requires odo.container;

    requires static lombok;

}