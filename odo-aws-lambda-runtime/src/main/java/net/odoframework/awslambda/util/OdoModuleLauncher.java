package net.odoframework.awslambda.util;

import net.odoframework.awslambda.AWSLambdaRuntimeContext;
import net.odoframework.awslambda.runtime.MapContext;
import net.odoframework.container.injection.Container;
import net.odoframework.container.util.Json;
import net.odoframework.http.Http;
import net.odoframework.service.ServiceFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.UUID;

public class OdoModuleLauncher {

    private static final Logger LOG = LoggerFactory.getLogger(OdoModuleLauncher.class);

    private static final Container CONTAINER = Container.getContainerInstance();
    private static Json JSON = null;

    static {
        LOG.debug("Starting ODO Launcher Cold-Start");
        JSON = CONTAINER.resolve(Json.class).orElseThrow(() -> new IllegalStateException("no instance of " + Http.class));
    }


    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        var handler = (ServiceFunction) CONTAINER.resolve(args[0].trim()).orElseThrow();
        var data = args[1];
        var response = handler.apply(data, new AWSLambdaRuntimeContext(data, new MapContext(Map.of("requestId", UUID.randomUUID()))));
        var responseJson = JSON.marshal(response);
        LOG.debug(responseJson);
    }

}
