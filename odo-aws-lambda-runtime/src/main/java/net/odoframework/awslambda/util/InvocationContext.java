package net.odoframework.awslambda.util;

import com.amazonaws.services.lambda.runtime.Context;

import java.util.UUID;

public class InvocationContext {

    public static final ThreadLocal<Context> CONTEXT = new ThreadLocal<>();

    public static final void set(Context context) {
        CONTEXT.set(context);
    }

    public static final Context get() {
        return CONTEXT.get();
    }

    public static final void unset() {
        CONTEXT.remove();
    }

    public static String getInvocationId() {
        var ctx = CONTEXT.get();
        return (ctx != null)
                ? ctx.getAwsRequestId()
                : UUID.randomUUID().toString();
    }


}
