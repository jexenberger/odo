package net.odoframework.awslambda.web;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.container.util.Json;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.RequestConverter;
import net.odoframework.service.web.WebRequest;

import java.util.Map;
import java.util.Objects;

public class ApiGatewayV2RequestConverter implements RequestConverter<Map<String, Object>, WebRequest, Context> {

    private final Json json;

    public ApiGatewayV2RequestConverter(Json json) {
        this.json = Objects.requireNonNull(json);
    }

    @Override
    public WebRequest decode(Map<String, Object> payload, InvocationContext<Context> context) {
        return new ApiGatewayV2Request(this.json, payload);
    }

}
