package net.odoframework.awslambda.web;

import javax.security.auth.Subject;
import java.security.Principal;
import java.util.Map;
import java.util.Objects;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class CognitoPrincipal implements Principal {

    private final Map<String, ?> cognitoDetails;

    public CognitoPrincipal(Map<String, ?> cognitoDetails) {
        this.cognitoDetails = requireNonNull(cognitoDetails, "required identity map");
    }

    @Override
    public String getName() {
        return getUser();
    }

    public String getUser() {
        return (String) cognitoDetails.get("user");
    }

    public String getCognitoIdentityPoolId() {
        return (String) cognitoDetails.get("cognitoIdentityPoolId");
    }

    public String getAccountId() {
        return (String) cognitoDetails.get("accountId");
    }

    public String getCognitoIdentityId() {
        return (String) cognitoDetails.get("cognitoIdentityId");
    }

    public String getCaller() {
        return (String) cognitoDetails.get("caller");
    }

    public String getApiKey() {
        return (String) cognitoDetails.get("apiKey");
    }

    public String getSourceIp() {
        return (String) cognitoDetails.get("sourceIp");
    }

    public String getCognitoAuthenticationType() {
        return (String) cognitoDetails.get("cognitoAuthenticationType");
    }

    public String getCognitoAuthenticationProvider() {
        return (String) cognitoDetails.get("cognitoAuthenticationProvider");
    }

    public String getUserArn() {
        return (String) cognitoDetails.get("userArn");
    }

    public String getUserAgent() {
        return (String) cognitoDetails.get("userAgent");
    }

    public String getAccessKey() {
        return (String) cognitoDetails.get("accessKey");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CognitoPrincipal)) return false;
        CognitoPrincipal that = (CognitoPrincipal) o;
        return cognitoDetails.equals(that.cognitoDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cognitoDetails);
    }

    @Override
    public boolean implies(Subject subject) {
        return ofNullable(subject.getPrincipals(CognitoPrincipal.class))
                .map(this::equals)
                .orElse(false);
    }
}
