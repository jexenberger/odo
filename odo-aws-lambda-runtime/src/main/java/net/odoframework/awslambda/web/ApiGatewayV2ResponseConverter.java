package net.odoframework.awslambda.web;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.container.util.Json;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.ResponseConverter;
import net.odoframework.service.web.WebResponse;
import net.odoframework.util.ListBackedMap;

import java.util.Map;

public class ApiGatewayV2ResponseConverter implements ResponseConverter<WebResponse, Map<String, Object>, Context> {

    private final Json json;

    public ApiGatewayV2ResponseConverter(Json json) {
        this.json = json;
    }

    public Map<String, Object> encode(WebResponse webResponse, InvocationContext<Context> context) {
        var responseMap = new ListBackedMap<String, Object>(4);
        responseMap.put("statusCode", webResponse.getStatusCode());
        responseMap.put("isBase64Encoded", false);
        responseMap.put("headers", webResponse.getHeaders());
        responseMap.put("body", webResponse.getBody());
        return responseMap;
    }
}
