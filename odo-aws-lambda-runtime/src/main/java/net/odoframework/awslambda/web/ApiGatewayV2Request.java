package net.odoframework.awslambda.web;

import lombok.Data;
import net.odoframework.container.util.Json;
import net.odoframework.service.web.URNPath;
import net.odoframework.service.web.WebRequest;
import net.odoframework.util.Strings;

import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.*;

@Data
public class ApiGatewayV2Request implements WebRequest {

    private Json json;
    private Map<String, String> pathVariables;
    private Map<String, ?> request;

    public ApiGatewayV2Request(Json json, Map<String, Object> request) {
        this.json = Objects.requireNonNull(json);
        this.request = Objects.requireNonNull(request);
    }

    @Override
    public String getBody() {
        var body = (String) request.get("body");
        if (Strings.isNotBlank(body) && isBase64Encoded()) {
            body = new String(Base64.getDecoder().decode(body), StandardCharsets.UTF_8);
        }
        return body;
    }

    @Override
    public <T> T getBody(Class<T> type) {
        var body = getBody();
        if (body != null) {
            return getJson().unmarshal(body, type);
        }
        return null;
    }

    private boolean isBase64Encoded() {
        final Object isBase64Encoded = request.get("isBase64Encoded");
        if (isBase64Encoded == null) {
            return false;
        }
        return (boolean) isBase64Encoded;
    }

    @Override
    public Optional<List<String>> getMultiValueHeader(String name) {
        return Optional.empty();
    }

    @Override
    public Optional<Principal> getUserPrincipal() {
        return Optional.of(new CognitoPrincipal((Map<String, ?>) getRequestContext().get("principal")));
    }

    public Map<String, ?> getRequestContext() {
        return (Map<String, ?>) request.get("requestContext");
    }

    @Override
    public Optional<String> getHeader(String name) {
        return Optional.ofNullable(getHeaders().get(name));
    }

    @Override
    public String getPath() {
        return (String) request.get("path");
    }

    private Map<String, String> getHeaders() {
        return (Map<String, String>) request.get("headers");
    }

    @Override
    public Optional<String> getPathVariable(String name) {
        if (pathVariablesNotSet()) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.pathVariables.get(name));
    }

    @Override
    public String getMethod() {
        return (String) request.get("httpMethod");
    }

    @Override
    public boolean matches(String pattern) {
        var pathVariables = URNPath.match(getPath(), pattern);
        if (pathVariablesNotSet() && pathVariables.isPresent()) {
            this.pathVariables = pathVariables.get();
            return true;
        }
        return false;
    }

    @Override
    public boolean pathVariablesNotSet() {
        return this.pathVariables == null || this.pathVariables.isEmpty();
    }

    @Override
    public Optional<String> getQueryParam(String name) {
        return Optional.ofNullable(getQueryParameters().get(name));
    }

    private Map<String, String> getQueryParameters() {
        return (Map<String, String>) request.get("queryStringParameters");
    }

    @Override
    public Optional<List<String>> getMultiValueQueryParam(String name) {
        return Optional.ofNullable(getMultiValueQueryParameters().get(name));
    }

    private Map<String, List<String>> getMultiValueQueryParameters() {
        return (Map<String, List<String>>) request.get("multiValueQueryStringParameters");
    }

    public Json getJson() {
        return json;
    }

    void setJson(Json json) {
        this.json = json;
    }
}
