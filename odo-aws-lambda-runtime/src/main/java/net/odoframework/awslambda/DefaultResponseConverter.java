package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.awslambda.web.ApiGatewayV2ResponseConverter;
import net.odoframework.container.util.Json;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.ResponseConverter;
import net.odoframework.service.web.WebResponse;

public class DefaultResponseConverter implements ResponseConverter<Object, Object, Context> {

    private final ApiGatewayV2ResponseConverter responseConverter;

    public DefaultResponseConverter(Json json) {
        responseConverter = new ApiGatewayV2ResponseConverter(json);
    }

    @Override
    public Object encode(Object value, InvocationContext<Context> context) {
        if (value instanceof WebResponse) {
            return responseConverter.encode((WebResponse) value, context);
        }
        return value;
    }
}
