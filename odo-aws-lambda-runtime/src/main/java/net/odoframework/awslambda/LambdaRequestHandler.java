package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import net.odoframework.service.Bootstrap;

public abstract class LambdaRequestHandler<T, K> extends BaseHandler implements RequestHandler<T, K> {

    private static final Bootstrap BOOTSTRAP;

    static {
        BOOTSTRAP = Bootstrap.getBoostrap();
    }

    @Override
    @SuppressWarnings("unchecked")
    public K handleRequest(T input, Context context) {
        return (K) BOOTSTRAP.handleRequest(input, context);
    }
}
