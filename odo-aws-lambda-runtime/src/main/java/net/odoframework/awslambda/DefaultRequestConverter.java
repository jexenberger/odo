package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.awslambda.web.ApiGatewayV2Request;
import net.odoframework.container.util.Json;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.RequestConverter;
import net.odoframework.service.web.WebFunction;

import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;

public class DefaultRequestConverter implements RequestConverter<Object, Object, Context> {

    private final BiFunction<?, ?, ?> handler;
    private final Json json;

    public DefaultRequestConverter(BiFunction<?, ?, ?> handler, Json json) {
        this.handler = Objects.requireNonNull(handler, "handler is required");
        this.json = Objects.requireNonNull(json);
    }

    @Override
    public Object decode(Object format, InvocationContext<Context> context) {
        if (handler instanceof WebFunction) {
            Map<String, Object> payload = null;
            if (format instanceof String) {
                payload = json.unmarshalToMap(format.toString());
            } else {
                payload = (Map<String, Object>) format;
            }
            return new ApiGatewayV2Request(json, payload);
        }
        return format;
    }
}
