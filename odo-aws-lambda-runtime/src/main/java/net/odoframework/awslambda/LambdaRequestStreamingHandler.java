package net.odoframework.awslambda;


import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import net.odoframework.util.IO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

@SuppressWarnings("unchecked")
public class LambdaRequestStreamingHandler extends BaseHandler implements RequestStreamHandler {


    public LambdaRequestStreamingHandler() {
    }


    @Override
    public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
        final var stringData = new ByteArrayOutputStream();
        IO.pipe(input, stringData);
        final var result = BOOTSTRAP.handleRequest(stringData.toString(Charset.defaultCharset()), context);
        if (result != null) {
            if (result instanceof String) {
                output.write(result.toString().getBytes(Charset.defaultCharset()));
            } else {
                output.write(BOOTSTRAP.getJson().marshal(result).getBytes(Charset.defaultCharset()));
            }
            output.flush();
        }
    }
}

