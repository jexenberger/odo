package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.container.util.Json;
import net.odoframework.service.*;

import java.util.function.BiFunction;

public class AWSProviderRuntime implements ProviderRuntime<Context> {


    private final BiFunction<?, ?, ?> handler;
    private final Json json;

    public AWSProviderRuntime(BiFunction<?, ?, ?> handler, Json json) {
        this.handler = handler;
        this.json = json;
    }

    @Override
    public RequestConverter<?, Object, Context> getProviderDefaultRequestConverter() {
        return new DefaultRequestConverter(handler, json);
    }

    @Override
    public ResponseConverter<?, Object, Context> getProviderDefaultResponseConverter() {
        return new DefaultResponseConverter(json);
    }

    @Override
    public ExceptionHandler<Context, Object> getProviderDefaultExceptionHandler() {
        return new DefaultExceptionHandler(handler, json);
    }

    @Override
    public InvocationContext<Context> createInvocation(Context containerInstance) {
        return new AWSLambdaRuntimeContext(containerInstance.getAwsRequestId(), containerInstance);
    }

}
