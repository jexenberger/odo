package net.odoframework.awslambda;

import net.odoframework.service.Bootstrap;

public class BaseHandler {

    protected static final Bootstrap BOOTSTRAP;

    static {
        BOOTSTRAP = Bootstrap.getBoostrap();
    }

}
