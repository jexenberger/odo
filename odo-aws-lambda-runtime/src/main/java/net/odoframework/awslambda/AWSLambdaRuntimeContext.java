package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.odoframework.service.InvocationContext;

@Data
@AllArgsConstructor
public class AWSLambdaRuntimeContext implements InvocationContext<Context> {

    private Object rawPayload;
    private Context requestContext;

    @Override
    public String getRequestId() {
        return getRequestContext().getAwsRequestId();
    }

}
