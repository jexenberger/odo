package net.odoframework.awslambda.runtime;

import com.amazonaws.services.lambda.runtime.*;

import java.util.Map;

public class MapContext implements Context {

    private final Map<String, ?> context;

    public MapContext(Map<String, ?> context) {
        this.context = context;
    }

    @Override
    public String getAwsRequestId() {
        return (String) context.get("requestId");
    }

    @Override
    public String getLogGroupName() {
        return null;
    }

    @Override
    public String getLogStreamName() {
        return null;
    }

    @Override
    public String getFunctionName() {
        return System.getenv("_HANDLER");
    }

    @Override
    public String getFunctionVersion() {
        return null;
    }

    @Override
    public String getInvokedFunctionArn() {
        return null;
    }

    @Override
    public CognitoIdentity getIdentity() {
        return new MapCognitoIdentity((Map<String, ?>) context.get("identity"));
    }

    @Override
    public ClientContext getClientContext() {
        return null;
    }

    @Override
    public int getRemainingTimeInMillis() {
        return 0;
    }

    @Override
    public int getMemoryLimitInMB() {
        return 0;
    }

    @Override
    public LambdaLogger getLogger() {
        return LambdaRuntime.getLogger();
    }
}
