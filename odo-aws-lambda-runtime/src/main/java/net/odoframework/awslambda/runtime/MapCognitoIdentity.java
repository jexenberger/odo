package net.odoframework.awslambda.runtime;

import com.amazonaws.services.lambda.runtime.CognitoIdentity;

import java.util.Map;

public class MapCognitoIdentity implements CognitoIdentity {

    private final Map<String, ?> context;

    public MapCognitoIdentity(Map<String, ?> context) {
        this.context = context;
    }

    @Override
    public String getIdentityId() {
        return (String) context.get("identityId");
    }

    @Override
    public String getIdentityPoolId() {
        return (String) context.get("identityPoolId");
    }
}
