package net.odoframework.awslambda;

import net.odoframework.awslambda.logging.AWSLogFormatter;
import net.odoframework.container.Module;
import net.odoframework.container.ModuleBuilder;
import net.odoframework.container.events.EventPublisher;
import net.odoframework.container.events.Log;
import net.odoframework.container.injection.ConfigurationProperties;
import net.odoframework.container.injection.ContainerWrapper;
import net.odoframework.container.util.Json;
import net.odoframework.service.Bootstrap;
import net.odoframework.service.ProviderRuntime;
import net.odoframework.service.ServiceFunction;
import net.odoframework.service.ServiceModule;
import net.odoframework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.function.BiFunction;

public class OdoAwsLambdaModule extends ModuleBuilder {


    private static final Logger LOG;

    static {
        var properties = new ConfigurationProperties().loadConfig(OdoAwsLambdaModule.class);
        var useJson = Boolean.valueOf(properties.getProperty("odo.logging.json", "false"));
        EventPublisher.handler(Log.class, new AWSLogFormatter(useJson));
        LOG = LoggerFactory.getLogger(OdoAwsLambdaModule.class);
    }

    @Override
    public int getPrecedence() {
        return -999999999;
    }

    @Override
    public Set<Module> getDependencies() {
        return Set.of(new ServiceModule());
    }

    @Override
    public void build() {
        provides(ProviderRuntime.class).with(it -> {
            final BiFunction<?, ?, ?> handler = Bootstrap.resolveHandler(it.getContainer());
            return new AWSProviderRuntime(handler, it.references(Json.class));
        });
    }

    public static BiFunction<?, ?, ?> resolveFunction(ContainerWrapper it) {
        var functionConfig = it.value(ServiceFunction.NAME);
        BiFunction<?,?,?> handler = null;
        if (Strings.isNotBlank(functionConfig)) {
            handler =  it.references(functionConfig);
        } else {
            handler = it.references(ServiceFunction.NAME);
        }
        return handler;
    }


}
