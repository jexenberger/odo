package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.awslambda.web.ApiGatewayV2ResponseConverter;
import net.odoframework.container.util.Json;
import net.odoframework.service.ExceptionHandler;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.web.SimpleWebResponse;
import net.odoframework.service.web.WebFunction;
import net.odoframework.util.Exceptions;

import java.util.function.BiFunction;

public class DefaultExceptionHandler implements ExceptionHandler<Context, Object> {

    private final net.odoframework.service.DefaultExceptionHandler defaultExceptionHandler;
    private final BiFunction<?, ?, ?> handler;
    private final ApiGatewayV2ResponseConverter responseConverter;
    private final Json json;

    public DefaultExceptionHandler(BiFunction<?, ?, ?> handler, Json json) {
        this.handler = handler;
        this.json = json;
        this.responseConverter = new ApiGatewayV2ResponseConverter(json);
        this.defaultExceptionHandler = new net.odoframework.service.DefaultExceptionHandler();
    }

    @Override
    public Object apply(InvocationContext<Context> context, Throwable throwable) {
        if (handler instanceof WebFunction) {
            return this.responseConverter.encode(new SimpleWebResponse(json).serverError().body(Exceptions.toString(throwable)), context);
        } else {
            return this.defaultExceptionHandler.apply(context, throwable);
        }
    }
}
