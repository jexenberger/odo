package net.odoframework.awslambda.logging;

import com.amazonaws.services.lambda.runtime.LambdaRuntime;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.odoframework.awslambda.util.InvocationContext;
import net.odoframework.container.events.Log;
import net.odoframework.container.events.Log.Level;
import net.odoframework.util.Exceptions;
import net.odoframework.util.Strings;
import org.slf4j.LoggerFactory;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

public class AWSLogFormatter implements Consumer<Log> {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ssZ")
            .withZone(ZoneId.systemDefault());
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private boolean json = true;

    public AWSLogFormatter(boolean json) {
        this.json = json;
    }

    String format(Log log) {
        if (json) {
            return formatJson(log);
        } else {
            return formatLine(log);
        }
    }


    public String formatLine(Log log) {
        var line = String.join(" ", DATE_TIME_FORMATTER.format(log.getTimestamp()), InvocationContext.getInvocationId(), log.getLevel().getUpperCaseName(), log.getMessageAsString());
        if (log.getError() != null) {
            return String.join("\n", line, Exceptions.toString(Exceptions.getRoot(log.getError())));
        }
        return line;
    }

    private String formatJson(Log log) {
        var object = new JsonObject();
        object.add("id", new JsonPrimitive(InvocationContext.getInvocationId()));
        if (Strings.isBlank(log.getLoggerName())) {
            object.add("name", new JsonPrimitive(log.getLoggerName()));
        }
        object.add("level", new JsonPrimitive(log.getLevel().getUpperCaseName()));
        object.add("message", new JsonPrimitive(log.getMessageAsString()));
        object.add("timestamp", new JsonPrimitive(DATE_TIME_FORMATTER.format(log.getTimestamp())));
        if (log.getError() != null) {
            object.add("error", new JsonPrimitive(Exceptions.toString(Exceptions.getRoot(log.getError()))));
        }
        return GSON.toJson(object);
    }


    @Override
    public void accept(Log log) {
        final var logger = LoggerFactory.getLogger(log.getLoggerName());
        if (log.getLevel() == Level.trace && logger.isTraceEnabled()) {
            LambdaRuntime.getLogger().log(format(log) + '\n');
        }
        if (log.getLevel() == Level.debug && logger.isDebugEnabled()) {
            LambdaRuntime.getLogger().log(format(log) + '\n');
        }
        if (log.getLevel() == Level.info && logger.isInfoEnabled()) {
            LambdaRuntime.getLogger().log(format(log) + '\n');
        }
        if (log.getLevel() == Level.error && logger.isErrorEnabled()) {
            LambdaRuntime.getLogger().log(format(log) + '\n');
        }
        if (log.getLevel() == Level.warn && logger.isWarnEnabled()) {
            LambdaRuntime.getLogger().log(format(log) + '\n');
        }
    }
}
