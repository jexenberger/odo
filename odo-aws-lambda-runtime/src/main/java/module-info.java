import net.odoframework.annotations.FrameworkModule;
import net.odoframework.awslambda.OdoAwsLambdaModule;
import net.odoframework.container.Module;

@FrameworkModule
module odo.aws.lambda.runtime {

    requires jakarta.inject;
    requires odo.core;
    requires odo.container;
    requires odo.service;

    requires org.slf4j;

    requires transitive aws.lambda.java.core;
    requires transitive lombok;

    exports net.odoframework.awslambda;
    exports net.odoframework.awslambda.util;
    exports net.odoframework.awslambda.runtime;
    exports net.odoframework.awslambda.web;
    exports net.odoframework.awslambda.logging;

    opens net.odoframework.awslambda.util;
    opens net.odoframework.awslambda;
    opens net.odoframework.awslambda.logging;


    provides Module with OdoAwsLambdaModule;

}