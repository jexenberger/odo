package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.container.injection.Container;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

public class LambdaMapHandlerRequestTest {

    static {
        Container.setContainerInstance(new TestApplication().getContainer());
    }


    @Test
    void handleRequest() {
        var handler = new LambdaRequestMapHandler();
        Map<String, Object> map = Map.of("test", "mycoolmap");
        var result = handler.handleRequest(map, mock(Context.class));
        assertTrue(TestApplication.CALLED.get().contains("mycoolmap"));
        assertEquals(result.get("count"), TestApplication.COUNT.get());
    }

}
