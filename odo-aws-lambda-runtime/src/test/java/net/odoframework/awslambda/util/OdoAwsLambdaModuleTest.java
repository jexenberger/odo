package net.odoframework.awslambda.util;

import net.odoframework.awslambda.OdoAwsLambdaModule;
import net.odoframework.container.injection.BeanDefinition;
import net.odoframework.container.injection.Container;
import net.odoframework.container.sql.SimpleDataSource;
import net.odoframework.util.configuration.Configuration;
import net.odoframework.util.configuration.SimpleMapSource;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

public class OdoAwsLambdaModuleTest {

    @Test
    void testDataSourceAlreadyPresent() {
        var p = new SimpleMapSource()
                .add(SimpleDataSource.DRIVER, "driver")
                .add(SimpleDataSource.URL, "url")
                .toConfig();
        var module = new OdoAwsLambdaModule();

        var container = new Container(p);
        container.register(BeanDefinition.bean(DataSource.class).with(() -> mock(DataSource.class)));
        module.apply(container, p);

        var ds = container.resolve(DataSource.class);
        assertTrue(ds.isPresent());
        assertFalse(ds.get() instanceof SimpleDataSource);
    }

    @Test
    void testDataSource() {
        var module = new OdoAwsLambdaModule();
        final var configuration = new Configuration();
        var container = new Container(configuration);
        module.apply(container, configuration);
        container.resolve(DataSource.class);
    }


    @Test
    void testDataSourcePropertyPresent() {
        var p = new SimpleMapSource().toConfig();
        var module = new OdoAwsLambdaModule();
        var container = new Container(p);
        module.apply(container, p);

        var ds = container.resolve(DataSource.class);
        assertFalse(ds.isPresent());
    }

    @Test
    void testDataSourcePropertyPresentDriver() {
        final var p = new SimpleMapSource()
                .add(SimpleDataSource.DRIVER, "driver")
                .toConfig();
        var module = new OdoAwsLambdaModule();
        var container = new Container(p);
        module.apply(container, p);

        var ds = container.resolve(DataSource.class);
        assertFalse(ds.isPresent());
    }

    @Test
    void testDataSourcePropertyPresentUrl() {
        final var p = new SimpleMapSource()
                .add(SimpleDataSource.URL, "url")
                .toConfig();
        var module = new OdoAwsLambdaModule();
        var container = new Container(p);
        module.apply(container, p);

        var ds = container.resolve(DataSource.class);
        assertFalse(ds.isPresent());
    }
}
