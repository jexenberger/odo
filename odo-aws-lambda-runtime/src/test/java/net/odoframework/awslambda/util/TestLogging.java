package net.odoframework.awslambda.util;

import net.odoframework.container.events.EventPublisher;
import net.odoframework.container.events.Log;
import net.odoframework.container.events.Log.Level;
import net.odoframework.container.injection.ConfigurationProperties;
import net.odoframework.service.logging.Slf4jLoggerFactory;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestLogging {

    static {
        ConfigurationProperties.resetGlobal();
        Slf4jLoggerFactory.setDefaultLevel(Level.info);
    }



    @Test
    void slf4jTest() {
        Logger logger = LoggerFactory.getLogger(TestLogging.class);
        final var called = new AtomicInteger(0);
        EventPublisher.handler(Log.class, (Consumer<Log>) it -> {
            called.incrementAndGet();
            System.out.println(it.getMessage());
        });

        logger.info("qwerty");
        assertTrue(called.get() > 0);
    }
}
