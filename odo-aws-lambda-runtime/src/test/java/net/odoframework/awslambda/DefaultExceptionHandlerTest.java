package net.odoframework.awslambda;

import net.odoframework.container.GsonJson;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.web.WebFunction;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

public class DefaultExceptionHandlerTest {

    private WebFunction mock;
    private GsonJson json;

    @Test
    void apply_AsWebFunction() {
        mock = mock(WebFunction.class);
        json = new GsonJson();
        var result = new DefaultExceptionHandler(mock, json).apply(mock(InvocationContext.class), new RuntimeException("Borked"));
        assertTrue(result instanceof Map);
        var data = (Map<String, Object>) result;
        assertTrue(data.containsKey("body"));
        assertEquals(500, data.get("statusCode"));
        assertEquals(false, data.get("isBase64Encoded"));
    }
}
