package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.container.injection.Container;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class LambdaRequestStreamingHandlerTest {


    static {
        Container.setContainerInstance(new TestApplication().getContainer());
    }


    @Test
    void handleRequest() throws Exception {
        var handler = new LambdaRequestStreamingHandler();
        final var output = new ByteArrayOutputStream();
        final var hello = new ByteArrayInputStream("hello".getBytes(Charset.defaultCharset()));
        TestApplication.COUNT.set(0);
        handler.handleRequest(hello, output, mock(Context.class));
        assertEquals("hello", TestApplication.CALLED.get());
        assertEquals(1, TestApplication.COUNT.get());
    }
}
