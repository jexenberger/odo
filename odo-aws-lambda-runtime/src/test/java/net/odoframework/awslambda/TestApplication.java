package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.container.Application;
import net.odoframework.service.InvocationContext;
import net.odoframework.service.ServiceFunction;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;

public class TestApplication extends Application {

    public static final AtomicReference<String> CALLED = new AtomicReference<>("");
    public static final AtomicInteger COUNT = new AtomicInteger(0);

    @Override
    public void build() {

        BiFunction<Object, InvocationContext<Context>, Map<String, Object>> function = (s, invocation) -> {
            CALLED.set(s.toString());
            return Map.of("count", COUNT.incrementAndGet());
        };
        provides(ServiceFunction.NAME).with(() -> function);
    }
}
