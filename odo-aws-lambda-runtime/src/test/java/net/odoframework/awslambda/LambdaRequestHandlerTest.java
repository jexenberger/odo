package net.odoframework.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import net.odoframework.container.injection.Container;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class LambdaRequestHandlerTest {
    static {
        Container.setContainerInstance(new TestApplication().getContainer());
    }


    @Test
    void handleRequest() {
        var handler = new LambdaRequestHandler<String, Map<String, Object>>() {
        };
        var result = handler.handleRequest("hello", mock(Context.class));
        assertEquals("hello", TestApplication.CALLED.get());
        assertEquals(result.get("count"), TestApplication.COUNT.get());
    }
}
