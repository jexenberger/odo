package net.odoframework.beans;

import java.util.Map;

public class MapVariable<K> extends MapValue<K> implements Variable<Map<String, ?>, K> {
    public MapVariable(String propertyName) {
        super(propertyName);
    }


    @Override
    @SuppressWarnings("unchecked")
    public void set(Map<String, ?> instance, K value) {
        ((Map) instance).put(getPropertyName(), value);
    }
}
