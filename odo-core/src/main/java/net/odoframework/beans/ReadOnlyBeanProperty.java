package net.odoframework.beans;

import lombok.Getter;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

@SuppressWarnings("unchecked")
public class ReadOnlyBeanProperty<T,K> implements Value<T,K> {
    @Getter
    protected final Function<T,K> getter;

    public ReadOnlyBeanProperty(Function<T, K> getter) {
        this.getter = Objects.requireNonNull(getter);
    }


    public K get(T instance) {
        return getter.apply(Objects.requireNonNull(instance));
    }

    public Function<T,K> getGetter() {
        return this.getter;
    }
}
