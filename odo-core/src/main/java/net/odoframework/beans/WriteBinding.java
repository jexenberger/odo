package net.odoframework.beans;

import java.util.function.Function;

@SuppressWarnings("unchecked")
public class WriteBinding<T,K,Z,J> extends ReadBinding<T,K,Z,J> implements BiDirectionalBinding<T,Z>{

    private Function<J, K> writeConverter;

    public WriteBinding(Variable<T, K> value, Variable<Z, J> target, Function<K, J> converter, Function<J, K> writeConverter) {
        super(value, target, converter);
        this.writeConverter = writeConverter;
    }

    public WriteBinding(Variable<T, K> value, Variable<Z, J> target) {
        super(value, target);
        this.writeConverter = (it) -> (K) it;
    }

    @Override
    public void bindBack(T source, Z target) {
        var read = this.getTarget().get(target);
        var converted = this.writeConverter.apply(read);
        ((Variable<T,K>) this.getValue()).set(source, converted);
    }
}
