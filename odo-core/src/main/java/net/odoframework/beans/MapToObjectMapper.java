package net.odoframework.beans;

import net.odoframework.util.Pair;
import net.odoframework.util.Strings;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class MapToObjectMapper<T> {

    private final Map<String, Variable<T, ?>> bindings;

    @SafeVarargs
    public MapToObjectMapper(Pair<String, Variable<T, ?>>... properties) {
        bindings = new LinkedHashMap<>();
        for (Pair<String, Variable<T, ?>> property : properties) {
            add(property.getLeft(), property.getRight());
        }
    }

    public MapToObjectMapper<T> add(String key, Variable<T, ?> variable) {
        bindings.put(Strings.requireNotBlank(key), Objects.requireNonNull(variable));
        return this;
    }
}
