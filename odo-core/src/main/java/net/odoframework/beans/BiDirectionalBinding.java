package net.odoframework.beans;

public interface BiDirectionalBinding<T,K> extends Binding<T,K>{

    void bindBack(T source, K target);

}
