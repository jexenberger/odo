package net.odoframework.beans;

public interface Variable<T, K> extends Value<T, K> {
    void set(T instance, K value);
}
