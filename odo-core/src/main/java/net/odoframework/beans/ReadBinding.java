package net.odoframework.beans;

import lombok.Getter;

import java.util.Objects;
import java.util.function.Function;

@SuppressWarnings("unchecked")
public class ReadBinding<T,K,Z,J> implements Binding<T, Z> {

    @Getter
    private final Value<T,K> value;
    @Getter
    private final Variable<Z,J> target;
    @Getter
    private final Function<K,J> converter;

    public ReadBinding(Value<T, K> value, Variable<Z,J> target, Function<K, J> converter) {
        this.value = Objects.requireNonNull(value);
        this.target = Objects.requireNonNull(target);
        this.converter = Objects.requireNonNull(converter);
    }

    public ReadBinding(Value<T, K> value, Variable<Z,J> target) {
        this(value, target, (it) -> (J) it);
    }

    @Override
    public void bind(T source, Z target) {
        var readValue = (K) value.get(source);
        var converted = this.converter.apply(readValue);
        this.target.set(target, converted);
    }

    public Function<K, J> getConverter() {
        return converter;
    }
}
