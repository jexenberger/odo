package net.odoframework.beans;

public interface Binding<T,K> {

    void bind(T source, K target);


}
