package net.odoframework.beans;


import lombok.Getter;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class BeanProperty<T, K> extends ReadOnlyBeanProperty<T,K> implements Variable<T, K> {

    @Getter
    protected final BiConsumer<T, K> setter;

    public BeanProperty(Function<T, K> getter, BiConsumer<T, K> setter) {
        super(getter);
        this.setter = setter;
    }

    public BeanProperty(Function<T, K> getter) {
        this(getter, null);
    }

    public BeanProperty(BiConsumer<T, K> setter) {
        this(null, setter);
    }

    public static <T, K> Variable<T, K> binding(BiConsumer<T, K> setter) {
        return new BeanProperty<>(null, setter);
    }

    public static <T, K> Variable<T, K> binding(Function<T, K> getter,BiConsumer<T, K> setter) {
        return new BeanProperty<>(getter, setter);
    }

    @Override
    public void set(T instance, K value) {
        Objects.requireNonNull(setter, "no setter for this property binding")
                .accept(Objects.requireNonNull(instance), value);
    }

}
