package net.odoframework.beans;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;

@SuppressWarnings("unchecked")
public class CompositeReadBinding<T,K> implements Binding<T,K>
{


    @SuppressWarnings("unchecked")
    private final Set<Binding> bindings;

    public CompositeReadBinding() {
        bindings = new LinkedHashSet<>();
    }

    public <Z,J> CompositeReadBinding<T,K> add(Value<Z, T> value, Variable<J,K> target, Function<T, K> converter) {
       return add(new ReadBinding<>(value, target, converter));
    }

    public CompositeReadBinding<T,K> add(Binding<?,?> binding) {
        bindings.add(binding);
        return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void bind(T source, K target) {
        for (var it : bindings) {
            it.bind(source, target);
        }
    }
}
