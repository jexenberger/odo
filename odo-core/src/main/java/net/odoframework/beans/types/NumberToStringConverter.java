package net.odoframework.beans.types;

import java.util.function.Function;

public class NumberToStringConverter<T extends Number> implements Function<T, String> {
    @Override
    public String apply(T t) {
        if (t == null) {
            return null;
        }
        return t.toString();
    }
}
