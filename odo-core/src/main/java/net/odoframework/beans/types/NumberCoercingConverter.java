package net.odoframework.beans.types;

import net.odoframework.util.Pair;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static java.util.Arrays.asList;

public class NumberCoercingConverter<T extends Number, K extends Number> implements Function<T, K> {


    private static final Collection<Class<? extends Number>> NUMBERS = asList(
            Byte.class,
            Short.class,
            Float.class,
            Integer.class,
            Long.class,
            Double.class,
            BigInteger.class,
            BigDecimal.class
    );


    private final Class<K> target;

    public NumberCoercingConverter(Class<K> target) {
        this.target = Objects.requireNonNull(target);
    }

    @Override
    @SuppressWarnings("unchecked")
    public K apply(T t) {
        if (t == null) {
            return null;
        }
        var targetName = target.getSimpleName().toLowerCase();
        switch (targetName) {

            case "byte":
                return (K) (Byte) t.byteValue();
            case "short":
                return (K) (Short) t.shortValue();
            case "integer":
                return (K) (Integer) t.intValue();
            case "float":
                return (K) (Float) t.floatValue();
            case "long":
                return (K) (Long) t.longValue();
            case "double":
                return (K) (Double) t.doubleValue();
            case "biginteger":
                return (K) new BigInteger(t.toString());
            case "bigdecimal":
                return (K) new BigDecimal(t.toString());

            default:
                throw new IllegalStateException(targetName + " is not a supported number type");
        }
    }

    public static Map<Pair<Class<?>, Class<?>>, Function<?, ?>> numberCoercions() {
        Map<Pair<Class<?>, Class<?>>, Function<?, ?>> types = new HashMap<>();
        for (Class<? extends Number> number : NUMBERS) {
            for (Class<? extends Number> targetNumber : NUMBERS) {
                if (number.equals(targetNumber)) {
                    continue;
                }
                types.put(new Pair<>(number, targetNumber), new NumberCoercingConverter<>(targetNumber));
            }
        }
        return types;
    }
}
