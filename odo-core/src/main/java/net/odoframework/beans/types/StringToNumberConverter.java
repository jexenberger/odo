package net.odoframework.beans.types;

import net.odoframework.util.Strings;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;
import java.util.function.Function;

public class StringToNumberConverter<T extends Number> implements Function<String, T> {


    private final Function<Number, T> lastMileConverter;

    public StringToNumberConverter(Function<Number, T> lastMileConverter) {
        this.lastMileConverter = Objects.requireNonNull(lastMileConverter);
    }


    @Override
    public T apply(String s) {
        if (Strings.isBlank(s)) {
            return null;
        }
        var number = (s.trim().indexOf('.') > -1)
                ? new BigDecimal(s)
                : new BigInteger(s);
        return lastMileConverter.apply(number);
    }
}
