package net.odoframework.beans.types;

import net.odoframework.util.Pair;
import net.odoframework.util.Resource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Optional.of;

@SuppressWarnings("unchecked")
public class ConverterRegistry {

    private static final Map<Pair<Class<?>, Class<?>>, Function<?,?>> CONVERTERS;

    static {
        CONVERTERS = new HashMap<>();
        add(Integer.class, String.class, Converters.INT_TO_STRING);
        add(String.class, Integer.class, Converters.STRING_TO_INT);
        add(Double.class, String.class, Converters.DOUBLE_TO_STRING);
        add(String.class, Double.class, Converters.STRING_TO_DOUBLE);
        add(Long.class, String.class, Converters.LONG_TO_STRING);
        add(String.class, Long.class, Converters.STRING_TO_LONG);
        add(Float.class, String.class, Converters.FLOAT_TO_STRING);
        add(String.class, Float.class, Converters.STRING_TO_FLOAT);
        add(Byte.class, String.class, Converters.BYTE_TO_STRING);
        add(String.class, Byte.class, Converters.STRING_TO_BYTE);
        add(Short.class, String.class, Converters.SHORT_TO_STRING);
        add(String.class, Short.class, Converters.STRING_TO_SHORT);
        add(String.class, BigInteger.class, Converters.STRING_TO_BIGINTEGER);
        add(BigInteger.class, String.class, Converters.BIGINTEGER_TO_STRING);
        add(String.class, BigDecimal.class, Converters.STRING_TO_BIGDECIMAL);
        add(BigDecimal.class, String.class, Converters.BIGDECIMAL_TO_STRING);
        add(Boolean.class, String.class, Converters.BOOLEAN_TO_STRING);
        add(String.class, Boolean.class, Converters.STRING_TO_BOOLEAN);
        add(LocalDate.class, String.class, Converters.ISO_DATE_TO_STRING);
        add(String.class, LocalDate.class, Converters.STRING_TO_ISO_DATE);
        add(LocalDateTime.class, String.class, Converters.ISO_DATETIME_TO_STRING);
        add(String.class, LocalDateTime.class, Converters.STRING_TO_ISO_DATETIME);

        CONVERTERS.putAll(NumberCoercingConverter.numberCoercions());
        CONVERTERS.put(new Pair<>(String.class, Resource.class), new StringToResourceConverter());

    }

    public static <T,K> Optional<Function<T,K>> get(Class<T> a, Class<K> b) {
        if (a.getName().equals(b.getName())) {
            return of((it) -> (K) it);
        }
        return Optional.ofNullable((Function<T, K>) CONVERTERS.get(Pair.cons(a, b)));
    }

    public static <T,K> void add(Class<T> from, Class<K> to, Function<T,K> converter){
        CONVERTERS.put(
                new Pair(Objects.requireNonNull(from), Objects.requireNonNull(to)),
                Objects.requireNonNull(converter)
        );
    }

}
