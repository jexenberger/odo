package net.odoframework.beans.types;

import net.odoframework.util.ClasspathResource;
import net.odoframework.util.FileResource;
import net.odoframework.util.Resource;
import net.odoframework.util.Strings;

import java.util.function.Function;

public class StringToResourceConverter implements Function<String, Resource> {
    @Override
    public Resource apply(String s) {
        if (Strings.isNotBlank(s)) {
            return null;
        }
        if (s.startsWith("classpath:")) {
            return new ClasspathResource(s.substring(10));
        }
        if (s.startsWith("file:")) {
            return new FileResource(s.substring(5));
        }
        return new FileResource(s);
    }
}
