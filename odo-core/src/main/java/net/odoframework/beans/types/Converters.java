package net.odoframework.beans.types;

import net.odoframework.util.Resource;
import net.odoframework.util.Strings;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.Function;

public class Converters {

    public static Function<Integer, String> INT_TO_STRING = new NumberToStringConverter<>();
    public static Function<String, Integer> STRING_TO_INT = new StringToNumberConverter<>(Number::intValue);

    public static Function<Double, String> DOUBLE_TO_STRING = new NumberToStringConverter<>();
    public static Function<String, Double> STRING_TO_DOUBLE = new StringToNumberConverter<>(Number::doubleValue);

    public static Function<Long, String> LONG_TO_STRING = new NumberToStringConverter<>();
    public static Function<String, Long> STRING_TO_LONG = new StringToNumberConverter<>(Number::longValue);

    public static Function<Float, String> FLOAT_TO_STRING = new NumberToStringConverter<>();
    public static Function<String, Float> STRING_TO_FLOAT = new StringToNumberConverter<>(Number::floatValue);

    public static Function<Byte, String> BYTE_TO_STRING = new NumberToStringConverter<>();
    public static Function<String, Byte> STRING_TO_BYTE = new StringToNumberConverter<>(Number::byteValue);

    public static Function<Short, String> SHORT_TO_STRING = new NumberToStringConverter<>();
    public static Function<String, Short> STRING_TO_SHORT = new StringToNumberConverter<>(Number::shortValue);

    public static Function<BigDecimal, String> BIGDECIMAL_TO_STRING = new NumberToStringConverter<>();
    public static Function<String, BigDecimal> STRING_TO_BIGDECIMAL = new StringToNumberConverter<>(it -> (BigDecimal) it);

    public static Function<BigInteger, String> BIGINTEGER_TO_STRING = new NumberToStringConverter<>();
    public static Function<String, BigInteger> STRING_TO_BIGINTEGER = new StringToNumberConverter<>(it -> (BigInteger) it);

    public static Function<Boolean, String> BOOLEAN_TO_STRING = (it) -> (it != null) ? it.toString() : null;
    public static Function<String, Boolean> STRING_TO_BOOLEAN = (it) -> (Strings.isNotBlank(it)) ? Boolean.valueOf(it.toLowerCase()) : null;

    public static Function<String, LocalDate> STRING_TO_ISO_DATE = (it) -> (Strings.isNotBlank(it)) ? LocalDate.parse(it.trim()) : null;
    public static Function<LocalDate, String> ISO_DATE_TO_STRING = (it) -> (it != null) ? it.toString() : null;

    public static Function<String, LocalDateTime> STRING_TO_ISO_DATETIME = (it) -> (Strings.isNotBlank(it)) ? LocalDateTime.parse(it.trim()) : null;
    public static Function<LocalDateTime, String> ISO_DATETIME_TO_STRING = (it) -> (it != null) ? it.toString() : null;

    public static Function<String, Resource> STRING_TO_RESOURCE = new StringToResourceConverter();


}
