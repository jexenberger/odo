package net.odoframework.beans;

import lombok.Getter;
import net.odoframework.util.Strings;

import java.util.Map;

public class MapValue<K> implements Value<Map<String, ?>, K>{

    @Getter
    private final String propertyName;

    public MapValue(String propertyName) {
        this.propertyName = Strings.requireNotBlank(propertyName, "property name cannot be empty");
    }

    @Override
    @SuppressWarnings("unchecked")
    public K get(Map<String, ?> instance) {
        return (K) instance.get(propertyName);
    }
}
