package net.odoframework.beans;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;

public class CompositeWriteBinding<T,K> implements BiDirectionalBinding<T,K>{

    @SuppressWarnings("unchecked")
    private final Set<BiDirectionalBinding> bindings;


    public CompositeWriteBinding() {
        bindings = new LinkedHashSet<>();
    }

    @SuppressWarnings("unchecked")
    public <Z,J> CompositeWriteBinding<T,K> add(Variable<T, K> value, Variable<Z,J> target, Function<K, J> converter, Function<J, K> convertBack) {
        return add(new WriteBinding(value, target, converter, convertBack));
    }

    public CompositeWriteBinding<T,K> add(BiDirectionalBinding<?,?> binding) {
        bindings.add(binding);
        return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void bind(T source, K target) {
        for (var it : bindings) {
            it.bind(source, target);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void bindBack(T source, K target) {
        for (var it : bindings) {
            it.bindBack(source, target);
        }
    }


}
