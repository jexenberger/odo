package net.odoframework.beans;

@FunctionalInterface
public interface Value<T, K> {
    K get(T instance);
}
