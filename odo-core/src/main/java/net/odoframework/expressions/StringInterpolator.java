package net.odoframework.expressions;

import java.util.function.Function;

public class StringInterpolator {

    private final String theString;

    public StringInterpolator(String theString) {
        this.theString = theString;
    }

    public String parse(Function<String, Object> handleVariable) {
        var chars = theString.toCharArray();
        var builder = new StringBuilder();
        boolean startVariable = false;
        var variableBuilder = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
             var theChar = chars[i];

             if (theChar == '\\') {
                 var isVariable = peek(chars, i, '$');
                 var isVariableEnd = peek(chars, i, '}');
                 var isEnd = isNextEnd(chars, i);
                 if (isEnd) {
                     builder.append(theChar);
                     continue;
                 }
                 if (isVariable || isVariableEnd) {
                     //skip ahead and read the next value
                     builder.append(chars[++i]);
                     continue;
                 }
             }
             if (theChar == '$') {
                 var isVarPar = peek(chars, i, '{');
                 if (isVarPar && startVariable) {
                     throw new IllegalStateException("already in variable");
                 }
                 if (isVarPar && isNextEnd(chars, i) ) {
                    builder.append(theChar);
                    continue;
                 }
                 if (isVarPar) {
                     startVariable = true;
                     //skip ahead
                     i++;
                     continue;
                 }
             }
             if (theChar == '}' && startVariable) {
                 startVariable = false;
                 final var s = variableBuilder.toString();
                 var result = handleVariable.apply(s);
                 builder.append(result);
                 variableBuilder = new StringBuilder();
                 continue;
             }
             if (startVariable) {
                 variableBuilder.append(theChar);
                 if (i+1 == chars.length) {
                     throw new IllegalStateException("no ending brace for "+variableBuilder);
                 }
                 continue;
             }
             builder.append(theChar);

        }
        return builder.toString();
    }


    private boolean peek(char[] arr, int offset, char expectedChar) {
        if (isNextEnd(arr, offset)) return false;
        return arr[offset+1] == expectedChar;
    }


    private boolean isNextEnd(char[] arr, int offset) {
        return offset + 1 == arr.length;
    }


    public static StringInterpolator interpolate(String theString) {
        return new StringInterpolator(theString);
    }
}
