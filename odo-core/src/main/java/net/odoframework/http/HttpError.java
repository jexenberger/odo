package net.odoframework.http;

import java.io.InputStream;
import java.util.Objects;

public class HttpError extends BaseHttpResponse {

    private final String message;

    public HttpError(int code, String message, InputStream body) {
        super(code, body);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HttpError httpError = (HttpError) o;
        return Objects.equals(message, httpError.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), message);
    }

    @Override
    public String toString() {
        return "HttpError{" +
                "code=" + code +
                ", body=" + body +
                ", message='" + message + '\'' +
                '}';
    }
}
