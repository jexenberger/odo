package net.odoframework.http;

import net.odoframework.util.Either;

public interface Http {

    Either<HttpResponse, HttpError> execute(HttpRequest request);

}
