package net.odoframework.http;

import net.odoframework.util.IO;

import java.io.InputStream;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public class BaseHttpResponse {
    protected int code;
    protected InputStream body;

    public BaseHttpResponse(int code, InputStream body) {
        this.code = code;
        this.body = body;
    }

    public int getCode() {
        return code;
    }

    public Optional<InputStream> body() {
        return Optional.ofNullable(body);
    }

    public Optional<String> bodyAsString() {
        return body().map(IO::streamToString);
    }

    public <T> Optional<T> bodyAs(Function<String, T> converter) {
        return bodyAsString().map(converter);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseHttpResponse that = (BaseHttpResponse) o;
        return code == that.code && Objects.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, body);
    }


}
