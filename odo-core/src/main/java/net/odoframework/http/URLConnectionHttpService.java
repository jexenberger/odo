package net.odoframework.http;

import net.odoframework.util.Either;
import net.odoframework.util.IO;
import net.odoframework.util.Pair;
import net.odoframework.util.Strings;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.net.HttpURLConnection;

public class URLConnectionHttpService implements Http {
    @Override
    public Either<HttpResponse, HttpError> execute(HttpRequest request) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) request.getUrl().openConnection();
            connection.setRequestMethod(request.getMethod());
            connection.setDoInput(true);
            connection.setConnectTimeout(request.getTimeout());
            connection.setReadTimeout(request.getTimeout());
            for (var header : request.getHeaders().entrySet()) {
                connection.setRequestProperty(header.getKey(), String.join(", ", header.getValue()));
                if (request.getCredentials() != null) {
                    for (Pair<String, String> authHeader : request.getCredentials().toHeader()) {
                        connection.setRequestProperty(authHeader.getLeft(), authHeader.getRight());
                    }
                }
            }
            if (!Strings.isOneOf(request.getMethod(), "GET", "HEAD")) {
                connection.setDoOutput(true);
                connection.setChunkedStreamingMode(0);
                IO.pipe(request.getBody(), connection.getOutputStream());
            }
            var bis = new PushbackInputStream(connection.getInputStream());
            var buffer = bis.readAllBytes();
            return Either.left(new HttpResponse(connection.getResponseCode(), new ByteArrayInputStream(buffer)));

        } catch (FileNotFoundException e) {
            var result = IO.streamToString(connection.getErrorStream());
            try {
                return Either.right(new HttpError(404, connection.getResponseMessage(), new ByteArrayInputStream(result.getBytes(request.getCharSet()))));
            } catch (IOException ioException) {
                throw new IllegalStateException(e);
            }
        } catch (IOException e) {
            try {
                return Either.right(new HttpError(connection.getResponseCode(), e.getMessage(), new ByteArrayInputStream(e.getMessage().getBytes(request.getCharSet()))));
            } catch (IOException ioException) {
                throw new IllegalStateException("Unable to get response status");
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
