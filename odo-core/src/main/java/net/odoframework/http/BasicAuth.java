package net.odoframework.http;

import net.odoframework.util.Pair;
import net.odoframework.util.Strings;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;

public class BasicAuth implements Credentials {

    private final String user;
    private final String password;

    public BasicAuth(String user, String password) {
        this.user = Strings.requireNotBlank(user, "user is a required parameter").trim();
        this.password = Strings.requireNotBlank(password, "password is a required parameter").trim();
    }

    public static BasicAuth basicAuth(String user, String password) {
        return new BasicAuth(user, password);
    }

    public String authString() {
        return String.join(":", user, password);
    }

    public String authStringEncoded() {
        return String.join(" ", "Basic", Base64.getEncoder().encodeToString(authString().getBytes(StandardCharsets.UTF_8)));
    }

    @Override
    public Collection<Pair<String, String>> toHeader() {
        return Collections.singleton(Pair.cons(DEFAULT_HEADER, authStringEncoded()));
    }
}
