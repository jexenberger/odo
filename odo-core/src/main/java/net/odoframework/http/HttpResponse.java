package net.odoframework.http;

import java.io.InputStream;

public class HttpResponse extends BaseHttpResponse {

    public HttpResponse(int code, InputStream body) {
        super(code, body);
    }

}
