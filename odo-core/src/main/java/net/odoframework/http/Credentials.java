package net.odoframework.http;

import net.odoframework.util.Pair;

import java.util.Collection;

public interface Credentials {

    String DEFAULT_HEADER = "Authorization";

    static Credentials basicAuth(String user, String password) {
        return new BasicAuth(user, password);
    }

    static Credentials token(String token) {
        return new TokenAuthentication(token);
    }

    Collection<Pair<String, String>> toHeader();


}
