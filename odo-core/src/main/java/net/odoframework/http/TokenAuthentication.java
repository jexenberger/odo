package net.odoframework.http;

import net.odoframework.util.Pair;
import net.odoframework.util.Strings;

import java.util.Collection;
import java.util.Collections;

import static net.odoframework.util.Pair.cons;

public class TokenAuthentication implements Credentials {

    private final String token;

    public TokenAuthentication(String token) {
        this.token = Strings.requireNotBlank(token, "token cannot be blank");
    }

    @Override
    public Collection<Pair<String, String>> toHeader() {
        return Collections.singletonList(cons(Credentials.DEFAULT_HEADER, String.join(" ", "Bearer", this.token)));
    }
}
