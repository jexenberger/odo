package net.odoframework.http;

import net.odoframework.util.Pair;

import java.net.URL;
import java.util.Objects;
import java.util.Set;

public class Get {


    private final URL url;
    private Set<Pair<String, String>> queryParameters;


    public Get(URL url) {
        this.url = Objects.requireNonNull(url);
    }

    static Get get(URL url) {
        return new Get(url);
    }


}
