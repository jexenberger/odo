package net.odoframework.http;

import net.odoframework.util.Strings;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class HttpRequest {

    private String method = "GET";
    private Map<String, List<String>> headers;
    private final URI uri;
    private Credentials credentials;
    private InputStream body;
    private Charset charSet = StandardCharsets.UTF_8;
    private int timeout = 30000;

    public HttpRequest(URI uri) {
        this.uri = Objects.requireNonNull(uri, "uri is a required parameter");
    }

    public static final HttpRequest get(URI uri) {
        return new HttpRequest(uri);
    }

    public static final HttpRequest post(URI uri) {
        return new HttpRequest(uri).method("POST");
    }

    public static final HttpRequest put(URI uri) {
        return new HttpRequest(uri).method("PUT");
    }

    public static final HttpRequest delete(URI uri) {
        return new HttpRequest(uri).method("DELETE");
    }

    public static final HttpRequest head(URI uri) {
        return new HttpRequest(uri).method("HEAD");
    }

    public HttpRequest header(String name, String... values) {
        var valuesList = Arrays.asList(values);
        getHeaders().put(Strings.requireNotBlank(name), valuesList);
        return this;
    }

    public HttpRequest credentials(Credentials credentials) {
        this.credentials = Objects.requireNonNull(credentials, "credentials cannot be null");
        return this;
    }

    public HttpRequest body(InputStream inputStream) {
        this.body = Objects.requireNonNull(inputStream, "inputstream cannot be null");
        return this;
    }

    public HttpRequest method(String method) {
        this.method = Strings.requireNotBlank(method, "method cannot be null");
        return this;
    }

    public HttpRequest timeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    public HttpRequest charSet(Charset charSet) {
        this.charSet = Objects.requireNonNull(charSet);
        return this;
    }

    public HttpRequest body(String body) {
        return this.body(new ByteArrayInputStream(Strings.requireNotBlank(body).getBytes(this.charSet)));
    }

    public Map<String, List<String>> getHeaders() {
        if (headers == null) {
            headers = new LinkedHashMap<>();
        }
        return headers;
    }

    public String getMethod() {
        return method;
    }

    public URI getUri() {
        return uri;
    }

    public URL getUrl() {
        try {
            return getUri().toURL();
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public InputStream getBody() {
        return body;
    }

    public Charset getCharSet() {
        return charSet;
    }

    public int getTimeout() {
        return timeout;
    }
}
