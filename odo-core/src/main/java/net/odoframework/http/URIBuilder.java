package net.odoframework.http;

import net.odoframework.util.Pair;
import net.odoframework.util.Strings;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

public class URIBuilder {

    private static final Map<String, DateTimeFormatter> FORMATS = new HashMap<>();


    private String protocol = "http";
    private String user = null;
    private String password = null;
    private String server;
    private int port = -1;
    private Set<Pair<String, String>> queryParameters;
    private String path;

    public static URIBuilder uri(String server) {
        return new URIBuilder().server(server);
    }

    public static URIBuilder uri(String protocol, String server) {
        return new URIBuilder().protocol(protocol).server(server);
    }

    private Set<Pair<String, String>> queryParameters() {
        if (queryParameters == null) {
            queryParameters = new LinkedHashSet<>();
        }
        return queryParameters;
    }

    public URIBuilder queryParam(String param, Object value) {
        queryParameters().add(Pair.cons(Strings.requireNotBlank(param, "parameters is a required parameter"), requireNonNull(value).toString()));
        return this;
    }

    public URIBuilder server(String server) {
        this.server = Strings.requireNotBlank(server, "server is a required parameter");
        return this;
    }

    public URIBuilder path(String path) {
        this.path = path;
        return this;
    }

    public URIBuilder protocol(String protocol) {
        this.protocol = Strings.requireNotBlank(protocol, "protocol is a required parameter");
        return this;
    }

    public URIBuilder user(String user) {
        this.user = Strings.requireNotBlank(user, "user is a required parameter");
        return this;
    }

    public URIBuilder password(String password) {
        this.password = Strings.requireNotBlank(password, "user is a required parameter");
        return this;
    }

    public URIBuilder port(int port) {
        if (port < 1) {
            throw new IllegalArgumentException("port must be a positive number");
        }
        this.port = port;
        return this;
    }

    public URIBuilder queryParam(String param, Temporal value, String format) {
        DateTimeFormatter formatter = null;
        if (!FORMATS.containsKey(Strings.requireNotBlank(format, "format is a required parameter"))) {
            synchronized (FORMATS) {
                formatter = FORMATS.put(format, DateTimeFormatter.ofPattern(format));
            }
        } else {
            formatter = FORMATS.get(format);
        }
        return queryParam(param, formatter.format(value));
    }

    public Set<Pair<String, String>> getQueryParameters() {
        return queryParameters();
    }

    @Override
    public String toString() {

        var credentials = Stream.of(user, password)
                .filter(Strings::isNotBlank)
                .collect(Collectors.joining(":"));
        var serverAddress = (port > 0)
                ? String.join(":", server, Integer.toString(port))
                : server;

        var urn = (Strings.isNotBlank(path))
                ? String.join("/", serverAddress, path)
                : serverAddress;
        var authority = Stream.of(credentials, urn)
                .filter(Strings::isNotBlank)
                .collect(Collectors.joining("@"));
        var queryString = queryParameters()
                .stream()
                .map(it -> it.getLeft() + "=" + it.getRight())
                .collect(Collectors.joining("&"));
        var url = protocol.trim() + "://" + authority;

        return Stream.of(url, queryString)
                .filter(Strings::isNotBlank)
                .collect(Collectors.joining("?"));
    }

    public URI toURI() {
        try {
            return new URI(toString());
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }

    public URL toURL() {
        try {
            return toURI().toURL();
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }
}
