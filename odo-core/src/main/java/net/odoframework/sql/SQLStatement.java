package net.odoframework.sql;

import net.odoframework.util.Strings;

import java.util.function.Supplier;

public class SQLStatement extends BaseDBStatement {

    private final String sql;

    public SQLStatement(String sql) {
        this.sql = Strings.requireNotBlank(sql);
    }


    public static SQLStatement sql(String sql) {
        return new SQLStatement(sql);
    }

    public <T> SQLStatement bind(T value) {
        addBinding(value);
        return this;
    }

    public <T> SQLStatement bind(Supplier<T> value) {
        addBinding(value);
        return this;
    }

    @Override
    public String getSql() {
        return sql;
    }

}
