package net.odoframework.sql;

import lombok.EqualsAndHashCode;
import net.odoframework.util.Strings;

import java.sql.ResultSet;

public abstract class ColumnReference {

    public abstract <T> T read(ResultSet rs);

    public abstract <T> void write(ResultSet rs, T value);

    @EqualsAndHashCode(callSuper = false)
    private static class IndexReference extends ColumnReference {

        private final int index;

        public IndexReference(int index) {
            if (index < 1) {
                throw new IllegalArgumentException("index must be > 0");
            }
            this.index = index;
        }

        @Override
        public <T> T read(ResultSet rs) {
            return SQLUtils.getColumn(rs, index);
        }

        @Override
        public <T> void write(ResultSet rs, T value) {
            SQLUtils.setColumn(rs, index, value);
        }
    }

    @EqualsAndHashCode(callSuper = false)
    private static class NameReference extends ColumnReference {

        private final String columnName;

        public NameReference(String columnName) {
            this.columnName = Strings.requireNotBlank(columnName, "columnName cannot be null");
        }

        @Override
        public <T> T read(ResultSet rs) {
            return SQLUtils.getColumn(rs, columnName);
        }

        @Override
        public <T> void write(ResultSet rs, T value) {
            SQLUtils.setColumn(rs, columnName, value);
        }
    }

    public static ColumnReference index(int index) {
        return new IndexReference(index);
    }

    public static ColumnReference name(String name) {
        return new NameReference(name);
    }

}
