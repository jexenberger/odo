package net.odoframework.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface SQLConsumer<T> {

    void accept(ResultSet rs) throws SQLException;

}
