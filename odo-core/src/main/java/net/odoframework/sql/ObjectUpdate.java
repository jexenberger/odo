package net.odoframework.sql;

import net.odoframework.util.Strings;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ObjectUpdate<T> implements DBStatement{

    private final String tableName;
    private final T instance;
    private Map<String, Function<T, ?>> mappers;
    private Map<String, Function<T, ?>> whereClause;

    public ObjectUpdate(String tableName, T instance) {
        this.tableName = Strings.requireNotBlank(tableName, "tableName is required");
        this.instance = Objects.requireNonNull(instance, "instance cannot be null");
    }

    public static <T> ObjectUpdate<T> update(String tableName, T instance) {
        return new ObjectUpdate<>(tableName, instance);
    }

    public ObjectUpdate<T> addField(String name, Function<T,?> mapper) {
        if (mappers == null) {
            mappers = new LinkedHashMap<>();
        }
        mappers.put(
                Strings.requireNotBlank(name, "name is required"),
                Objects.requireNonNull(mapper, "mapper is required")
        );
        return this;
    }

    public ObjectUpdate<T> where(String name, Function<T,?> mapper) {
        if (whereClause == null) {
            whereClause = new LinkedHashMap<>();
        }
        whereClause.put(
                Strings.requireNotBlank(name, "name is required"),
                Objects.requireNonNull(mapper, "mapper is required")
        );
        return this;
    }

    @Override
    public String getSql() {
        if (mappers == null || mappers.isEmpty()) {
            throw new IllegalStateException("must pass at least one field to update");
        }

        String sql = "update " + tableName + " set";
        sql += mappers.keySet().stream()
                .map(it -> "\n\t" + it + " = ?")
                .collect(Collectors.joining(","));
        if (whereClause != null && !whereClause.isEmpty()) {
            sql += "\nwhere";
            sql += whereClause.keySet().stream()
                    .map(it -> "\n\t" + it + " = ?" )
                    .collect(Collectors.joining(" and "));
        }
        return sql;
    }

    @Override
    public Map<Integer, Object> getBindings() {
        final var cnt = new AtomicInteger(1);
        Map<Integer, Object> map = new LinkedHashMap<>();
        mappers.values().forEach(it ->
                map.put(cnt.getAndIncrement(), it.apply(this.instance))
        );
        if (whereClause != null){
            whereClause.values().forEach(it -> map.put(cnt.getAndIncrement(), it.apply(this.instance)));
        }
        return map;
    }
}
