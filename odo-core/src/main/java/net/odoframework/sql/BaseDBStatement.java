package net.odoframework.sql;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;

public abstract class BaseDBStatement implements DBStatement {
    private final Map<Integer, Object> bindings = new LinkedHashMap<>();
    private int cnt = 1;


    public void addBinding(Object value) {
        bindings.put(cnt++, value);
    }

    public <T> void addBinding(Supplier<T> value) {
        addBinding(value);
    }

    @Override
    public Map<Integer, Object> getBindings() {
        return Collections.unmodifiableMap(this.bindings);
    }
}
