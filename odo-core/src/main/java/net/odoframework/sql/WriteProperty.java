package net.odoframework.sql;

import java.sql.ResultSet;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class WriteProperty<T,K, Z> {

    private final ColumnReference ref;
    private final BiConsumer<T, K> setter;
    private final Function<Z, K> converter;

    @SuppressWarnings("unchecked")
    public WriteProperty(ColumnReference ref, BiConsumer<T, K> setter, Function<Z, K> converter) {
        this.ref = Objects.requireNonNull(ref, "columnReference cannot be null");
        this.setter = Objects.requireNonNull(setter, "setter is required");
        this.converter = (converter != null)
                ? converter
                : it -> (K) it;
    }

    public void read(ResultSet rs, T instance) {
        var unconverted = (Z) ref.read(rs);
        var converted = (unconverted != null)
                ? converter.apply(unconverted)
                : null;
        setter.accept(instance, converted);
    }
}
