package net.odoframework.sql;

import net.odoframework.util.Strings;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Update extends BaseUpdateStatement{

    private Map<String, Supplier<?>> whereClause;

    public Update(String table) {
        super(table);
    }

    public static Update update(String table) {
        return new Update(table);
    }

    public Update value(String name, Object value) {
        return value(name, () -> value);
    }

    public Update value(String name, Supplier<?> value) {
        addField(name, value);
        return this;
    }

    @Override
    public String getSql() {


        String sql = "update " + getTable() + " set";
        sql += getFields().keySet().stream()
                .map(it -> "\n\t" + it + " = ?")
                .collect(Collectors.joining(","));
        if (whereClause != null && !whereClause.isEmpty()) {
            sql += "\nwhere";
            sql += whereClause.keySet().stream()
                    .map(it -> "\n\t" + it + " = ?" )
                    .collect(Collectors.joining(" and "));
        }
        return sql;
    }

    public Update where(String name, Object value) {
        return where(name, () -> value);
    }

    public Update where(String name, Supplier<?> value) {
        if (whereClause == null) {
            whereClause = new LinkedHashMap<>();
        }
        whereClause.put(
                Strings.requireNotBlank(name, "name is required"),
                Objects.requireNonNull(value, "value is required")
        );
        return this;
    }

    @Override
    public Map<Integer, Object> getBindings() {
        final var cnt = new AtomicInteger(1);
        Map<Integer, Object> map = new LinkedHashMap<>();
        getFields().values().forEach(it ->
                map.put(cnt.getAndIncrement(), it.get())
        );
        if (whereClause != null){
            whereClause.values().forEach(it -> map.put(cnt.getAndIncrement(), it.get()));
        }
        return map;
    }
}
