package net.odoframework.sql;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class DefaultSQLTemplate implements SQLTemplate {

    private final DataSource dataSource;


    public DefaultSQLTemplate(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource, "dataSource cannot be null");
    }

    @Override
    public void execute(DBStatement sql, SQLConsumer<ResultSet> rsConsumer) {
        try (var query = new Query(sql, dataSource, false)) {
            rsConsumer.accept(query.getResultSet());
        } catch (SQLException throwables) {
            throw new IllegalStateException(throwables);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return this.dataSource.getConnection();
    }

    @Override
    public <T> T execute(DBStatement sql, SQLFunction<ResultSet, T> mapper) {
        try (var query = new Query(sql, dataSource, false)) {
            return mapper.apply(query.getResultSet());
        } catch (SQLException throwables) {
            throw new IllegalStateException(throwables);
        }
    }


    private void createStatement(DBStatement sql, PreparedStatement statement) throws SQLException {
        final var bindings = sql.getBindings();
        if (!bindings.isEmpty()) {
            for (Map.Entry<Integer, Object> entry : bindings.entrySet()) {
                var value = entry.getValue();
                if (value instanceof Supplier) {
                    value = ((Supplier) value).get();
                }
                statement.setObject(entry.getKey(), value);
            }
        }
    }

    @Override
    public int execute(DBStatement sql) {
        try (var conn = getConnection()) {
            try (var statement = conn.prepareStatement(sql.getSql())) {
                createStatement(sql, statement);
                return statement.executeUpdate();
            }
        } catch (SQLException throwables) {
            throw new SQLWrappedException(throwables);
        }
    }

    @Override
    public <T> Stream<T> stream(DBStatement sql, SQLFunction<ResultSet, T> row) {
        return StreamSupport
                .stream(new ResultSetSpliterator(new Query(sql, dataSource, true)), false)
                .map(it -> {
                    try {
                        return row.apply(it);
                    } catch (SQLException throwables) {
                        throw new SQLWrappedException(throwables);
                    }
                });
    }

    @Override
    public <T> T doInTransaction(Supplier<T> supplier) {
        Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            var result = supplier.get();
            conn.commit();
            return result;
        } catch (Exception throwables) {
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException e) {
                    throw new SQLWrappedException(e);
                }
            }
            if (throwables instanceof SQLException) {
                throw new SQLWrappedException((SQLException) throwables);
            } else {
                throw new IllegalStateException(throwables);
            }
        } finally {
            safeClose(conn);
        }
    }


    private void safeClose(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException throwables) {
                //break in the fabric of space time
            }
        }
    }
}
