package net.odoframework.sql;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;

public class Query implements AutoCloseable {

    private final DBStatement dbStatement;
    private final boolean closeConnection;
    private final DataSource dataSource;
    private Connection conn;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public Query(DBStatement dbStatement, DataSource dataSource, boolean closeConnection) {
        this.dbStatement = Objects.requireNonNull(dbStatement);
        this.dataSource = Objects.requireNonNull(dataSource);
        this.closeConnection = closeConnection;
    }

    private void createStatement(DBStatement sql, PreparedStatement statement) throws SQLException {
        final var bindings = sql.getBindings();
        if (!bindings.isEmpty()) {
            for (Map.Entry<Integer, Object> entry : bindings.entrySet()) {
                statement.setObject(entry.getKey(), entry.getValue());
            }
        }
    }

    public ResultSet getResultSet() {
        try {
            if (resultSet == null) {
                conn = dataSource.getConnection();
                preparedStatement = conn.prepareStatement(dbStatement.getSql());
                createStatement(dbStatement, preparedStatement);
                resultSet = preparedStatement.executeQuery();
            }
            return resultSet;
        } catch (SQLException e) {
            throw new SQLWrappedException(e);
        }
    }

    @Override
    public void close() throws SQLException {
        if (resultSet != null) {
            resultSet.close();
        }
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (closeConnection) {
            conn.close();
        }
    }
}
