package net.odoframework.sql;

import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Delete extends BaseUpdateStatement implements DBStatement {

    public Delete(String table) {
        super(table);
    }


    public Delete where(String name, Supplier<?> value) {
        addField(name, value);
        return this;
    }

    public Delete where(String name, Object value) {
        return where(name, () -> value);
    }

    public static Delete deleteFrom(String name) {
        return new Delete(name);
    }

    @Override
    public String getSql() {
        var sql = "delete from " + getTable();
        if (!getFields().isEmpty()) {
            sql += "\nwhere\n\t";
            sql += getFields().keySet().stream().map(it -> it + " = ?").collect(Collectors.joining("\nand\n\t"));
        }
        return sql;
    }

}
