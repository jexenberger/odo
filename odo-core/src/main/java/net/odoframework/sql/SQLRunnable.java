package net.odoframework.sql;

import java.sql.SQLException;

@FunctionalInterface
public interface SQLRunnable {

    void run() throws SQLException;

}
