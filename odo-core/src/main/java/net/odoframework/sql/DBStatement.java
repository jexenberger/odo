package net.odoframework.sql;

import java.util.Map;

public interface DBStatement {

    String getSql();

    Map<Integer, Object> getBindings();
}
