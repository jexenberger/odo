package net.odoframework.sql;

import net.odoframework.util.NullSafe;

import java.io.InputStream;
import java.io.Reader;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static java.util.Objects.requireNonNull;

public class ResultSetWrapper {

    private final ResultSet resultSet;

    public ResultSetWrapper(ResultSet resultSet) {
        this.resultSet = requireNonNull(resultSet, "resultSet is required");
    }

    @SuppressWarnings("unchecked")
    public <T> T get(int col) {
        return (T) SQLWrappedException.map(col, resultSet::getObject);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String col) {
        return (T) SQLWrappedException.map(col, resultSet::getObject);
    }

    public int getInt(int col) {
        return SQLWrappedException.map(col, resultSet::getInt);
    }

    public int getInt(String col) {
        return SQLWrappedException.map(col, resultSet::getInt);
    }

    public Long getLong(int col) {
        return SQLWrappedException.map(col, resultSet::getLong);
    }

    public Float getFloat(int col) {
        return SQLWrappedException.map(col, resultSet::getFloat);
    }

    public Float getFloat(String col) {
        return SQLWrappedException.map(col, resultSet::getFloat);
    }

    public Double getDouble(int col) {
        return SQLWrappedException.map(col, resultSet::getDouble);
    }

    public Double getDouble(String col) {
        return SQLWrappedException.map(col, resultSet::getDouble);
    }

    public Long getLong(String col) {
        return SQLWrappedException.map(col, resultSet::getLong);
    }

    public Short getShort(int col) {
        return SQLWrappedException.map(col, resultSet::getShort);
    }

    public Short getShort(String col) {
        return SQLWrappedException.map(col, resultSet::getShort);
    }

    public Byte getByte(String col) {
        return SQLWrappedException.map(col, resultSet::getByte);
    }

    public Byte getByte(int col) {
        return SQLWrappedException.map(col, resultSet::getByte);
    }

    public Boolean getBoolean(String col) {
        return SQLWrappedException.map(col, resultSet::getBoolean);
    }

    public byte[] getBytes(int col) {
        return SQLWrappedException.map(col, resultSet::getBytes);
    }

    public byte[] getBytes(String col) {
        return SQLWrappedException.map(col, resultSet::getBytes);
    }

    public String getString(int col) {
        return SQLWrappedException.map(col, resultSet::getString);
    }

    public String getString(String col) {
        return SQLWrappedException.map(col, resultSet::getString);
    }

    public Array getArray(String col) {
        return SQLWrappedException.map(col, resultSet::getArray);
    }

    public Array getArray(int col) {
        return SQLWrappedException.map(col, resultSet::getArray);
    }

    public String getNString(int col) {
        return SQLWrappedException.map(col, resultSet::getNString);
    }

    public String getNString(String col) {
        return SQLWrappedException.map(col, resultSet::getNString);
    }

    public Clob getClob(String col) {
        return SQLWrappedException.map(col, resultSet::getClob);
    }

    public Clob getClob(int col) {
        return SQLWrappedException.map(col, resultSet::getClob);
    }

    public Blob getBlob(int col) {
        return SQLWrappedException.map(col, resultSet::getBlob);
    }

    public Blob getBlob(String col) {
        return SQLWrappedException.map(col, resultSet::getBlob);
    }

    public InputStream getAsciiStream(String col) {
        return SQLWrappedException.map(col, resultSet::getAsciiStream);
    }

    public InputStream getAsciiStream(int col) {
        return SQLWrappedException.map(col, resultSet::getAsciiStream);
    }

    public InputStream getBinaryStream(int col) {
        return SQLWrappedException.map(col, resultSet::getBinaryStream);
    }

    public InputStream getBinaryStream(String col) {
        return SQLWrappedException.map(col, resultSet::getBinaryStream);
    }

    public Reader getCharacterStream(String col) {
        return SQLWrappedException.map(col, resultSet::getCharacterStream);
    }

    public Reader getCharacterStream(int col) {
        return SQLWrappedException.map(col, resultSet::getCharacterStream);
    }

    public Reader getNCharacterStream(String col) {
        return SQLWrappedException.map(col, resultSet::getNCharacterStream);
    }

    public Reader getNCharacterStream(int col) {
        return SQLWrappedException.map(col, resultSet::getNCharacterStream);
    }

    public RowId getRowId(int col) {
        return SQLWrappedException.map(col, resultSet::getRowId);
    }

    public RowId getRowId(String col) {
        return SQLWrappedException.map(col, resultSet::getRowId);
    }

    public int getRow() {
        return SQLWrappedException.get(resultSet::getRow);
    }

    public LocalTime getTime(int col) {
        final var time = SQLWrappedException.map(col, resultSet::getTime);
        return NullSafe.safe(time::toLocalTime);
    }

    public LocalTime getTime(String col) {
        final var time = SQLWrappedException.map(col, resultSet::getTime);
        return NullSafe.safe(time::toLocalTime);
    }

    public LocalDate getDate(String col) {
        final var time = SQLWrappedException.map(col, resultSet::getDate);
        return NullSafe.safe(time::toLocalDate);
    }

    public LocalDate getDate(int col) {
        final var time = SQLWrappedException.map(col, resultSet::getDate);
        return NullSafe.safe(time::toLocalDate);
    }

    public LocalDateTime getDateTime(int col) {
        final var time = SQLWrappedException.map(col, resultSet::getTimestamp);
        return NullSafe.safe(time::toLocalDateTime);
    }

    public LocalDateTime getDateTime(String col) {
        final var time = SQLWrappedException.map(col, resultSet::getTimestamp);
        return NullSafe.safe(time::toLocalDateTime);
    }

    public boolean next() {
        return SQLWrappedException.get(resultSet::next);
    }


}
