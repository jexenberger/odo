package net.odoframework.sql;

import net.odoframework.WrappedException;

import java.sql.SQLException;
import java.util.Objects;

public class SQLWrappedException extends WrappedException {


    public SQLWrappedException(String message, SQLException e) {
        super(message, e);
    }

    public SQLWrappedException(SQLException e) {
        super(e);
    }

    public SQLWrappedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, SQLException e) {
        super(message, cause, enableSuppression, writableStackTrace, e);
    }

    public static void run(SQLRunnable sqlRunnable) {
        try {
            Objects.requireNonNull(sqlRunnable, "sqlRunable is a required parameter").run();
        } catch (SQLException e) {
            throw new SQLWrappedException(e);
        }
    }

    public static <T> T get(SQLSupplier<T> sqlRunnable) {
        try {
            return Objects.requireNonNull(sqlRunnable, "sqlRunable is a required parameter").get();
        } catch (SQLException e) {
            throw new SQLWrappedException(e);
        }
    }

    public static <T, K> K map(T value, SQLFunction<T, K> function) {
        try {
            final var theFunction = Objects.requireNonNull(function, "function is a required parameter");
            return theFunction.apply(value);
        } catch (SQLException e) {
            throw new SQLWrappedException(e);
        }
    }

    @Override
    public SQLException getException() {
        return (SQLException) super.getException();
    }
}
