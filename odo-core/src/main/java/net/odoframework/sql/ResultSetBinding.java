package net.odoframework.sql;

import net.odoframework.beans.Variable;

import java.sql.ResultSet;

import static java.util.Objects.requireNonNull;
import static net.odoframework.sql.SQLUtils.getColumn;
import static net.odoframework.sql.SQLUtils.setColumn;
import static net.odoframework.util.Strings.requireNotBlank;

public class ResultSetBinding<T, K> implements Variable<T, K> {

    private final ResultSet resultSet;
    private final String column;


    public ResultSetBinding(ResultSet resultSet, String column) {
        this.resultSet = requireNonNull(resultSet, "resultSet is required");
        this.column = requireNotBlank(column, "column is required");
    }

    @Override
    public K get(T instance) {
        return getColumn(resultSet, column);
    }

    @Override
    public void set(T instance, K value) {
        setColumn(resultSet, column, value);
    }
}
