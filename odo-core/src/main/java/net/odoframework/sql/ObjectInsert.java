package net.odoframework.sql;

import net.odoframework.util.Strings;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ObjectInsert<T> implements DBStatement {

    private final String sqlCache = "";
    private final String tableName;
    private final T instance;
    private Map<String, Function<T, ?>> mappers;

    public ObjectInsert(String tableName, T instance) {
        this.tableName = Strings.requireNotBlank(tableName, "tableName is required");
        this.instance = Objects.requireNonNull(instance, "instance cannot be null");
    }

    public static <T> ObjectInsert<T> insert(String tableName, T instance) {
        return new ObjectInsert<>(tableName, instance);
    }

    public ObjectInsert<T> addField(String name, Function<T, ?> mapper) {
        if (mappers == null) {
            mappers = new LinkedHashMap<>();
        }
        mappers.put(
                Strings.requireNotBlank(name, "name is required"),
                Objects.requireNonNull(mapper, "mapper is required")
        );
        return this;
    }


    @Override
    public String getSql() {
        if (mappers == null || mappers.isEmpty()) {
            throw new IllegalStateException("must pass at least one field to update");
        }
        //double checked locking
        String sql = "insert into " + tableName + "\n\t(";
        sql += String.join(", ", mappers.keySet());
        sql += ")\nvalues\n\t(";
        sql += mappers.keySet().stream().map(it -> "? ").collect(Collectors.joining(", "));
        sql += ")";


        return sql;
    }

    @Override
    public Map<Integer, Object> getBindings() {
        final var cnt = new AtomicInteger(1);
        Map<Integer, Object> map = new LinkedHashMap<>();
        mappers.values().forEach(it ->
                map.put(cnt.getAndIncrement(), it.apply(this.instance))
        );
        return map;
    }
}
