package net.odoframework.sql;

import net.odoframework.util.ListBackedMap;

import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;

import static net.odoframework.util.NullSafe.safe;

public class SQLUtils {


    private static final Map<String, Map<String, ColumnIndex>> QUERY_INDICES = new LinkedHashMap<>();

    public static Map<String, ColumnIndex> getIndex(String sql) {
        return QUERY_INDICES.get(sql);
    }

    public static void set(String sql, Map<String, ColumnIndex> index) {
        QUERY_INDICES.put(sql, index);
    }

    public static Map<String, ColumnIndex> getIndex(String sql, ResultSet rs) {
        return QUERY_INDICES.computeIfAbsent(sql, (key) -> indexResultSet(rs));
    }

    @SuppressWarnings("unchecked")
    public static <T> T getColumn(ResultSet rs, int index) {
        return SQLWrappedException.get(() -> (T) rs.getObject(index));
    }

    @SuppressWarnings("unchecked")
    public static <T> T getColumn(ResultSet rs, String column) {
        return SQLWrappedException.get(() -> (T) rs.getObject(column));
    }

    @SuppressWarnings("unchecked")
    public static void setColumn(ResultSet rs, String column, Object value) {
        SQLWrappedException.run(() -> rs.updateObject(column, value));
    }

    public static void setColumn(ResultSet rs, int column, Object value) {
        SQLWrappedException.run(() -> rs.updateObject(column, value));
    }


    public static Map<String, ColumnIndex> indexResultSet(ResultSet rs) {

        var index = new LinkedHashMap<String, ColumnIndex>();
        SQLWrappedException.run(() -> {
            var metaData = rs.getMetaData();
            int cols = metaData.getColumnCount();
            for (int i = 1; i <= cols; i++) {
                var table = metaData.getTableName(i).toLowerCase();
                var column = metaData.getColumnName(i).toLowerCase();
                var indexTable = index.computeIfAbsent(table, (key) -> new ColumnIndex());
                indexTable.put(column, i);
            }
        });
        return index;
    }

    public static void dumpRow(Object[] row) {
        for (Object o : row) {
            System.out.print(safe(o::toString, null));
            System.out.print(" ");
        }
        System.out.println();
    }

    public static Object[] row(ResultSet rs) {
        return SQLWrappedException.get(() -> {
            var metaData = rs.getMetaData();
            int cols = metaData.getColumnCount();
            var row = new Object[cols];
            for (int i = 1; i <= cols; i++) {
                row[i - 1] = rs.getObject(i);
            }
            return row;
        });
    }

    public static void toMap(ResultSet rs, Consumer<Map<String, ?>> rowHandler) {
        SQLWrappedException.run(() -> {
            var metaData = rs.getMetaData();
            int cols = metaData.getColumnCount();
            var row = new ListBackedMap<String, Object>(cols);
            while (rs.next()) {
                for (int i = 1; i <= cols; i++) {
                    row.put(metaData.getColumnLabel(i), rs.getObject(i));
                }
                rowHandler.accept(row);
            }
        });
    }

    public static void toArray(ResultSet rs, Consumer<Object[]> rowHandler) {
        SQLWrappedException.run(() -> {
            var metaData = rs.getMetaData();
            int cols = metaData.getColumnCount();
            var row = new Object[cols];
            while (rs.next()) {
                for (int i = 1; i <= cols; i++) {
                    row[i - 1] = rs.getObject(i);
                }
                rowHandler.accept(row);
            }
        });
    }


}
