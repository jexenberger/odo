package net.odoframework.sql;

import java.sql.SQLException;

@FunctionalInterface
public interface SQLSupplier<T> {

    T get() throws SQLException;

}
