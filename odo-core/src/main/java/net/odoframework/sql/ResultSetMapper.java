package net.odoframework.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class ResultSetMapper<T> implements SQLFunction<ResultSet, T>{


    private final Function<ResultSet, T> instanceConstructor;
    private Map<ColumnReference, BiConsumer<T, ?>> setters;

    public ResultSetMapper(Function<ResultSet, T> instanceConstructor) {
        this.instanceConstructor = Objects.requireNonNull(instanceConstructor);
    }

    public ResultSetMapper(T instance) {
        this(() -> instance);
    }

    public ResultSetMapper(Supplier<T> instance) {
        this.instanceConstructor = (rs) -> instance.get();
    }

    public static <T> ResultSetMapper<T> into(Function<ResultSet, T> instanceConstructor) {
        return new ResultSetMapper<>(instanceConstructor);
    }

    public static <T> ResultSetMapper<T> into(T instance) {
        return new ResultSetMapper<>(instance);
    }

    public static <T> ResultSetMapper<T> into(Supplier<T> instance) {
        return new ResultSetMapper<>(instance);
    }

    public ResultSetMapper<T> field(int columnReference, BiConsumer<T, ?> handler) {
        return field(ColumnReference.index(columnReference), handler);
    }

    public ResultSetMapper<T> field(String columnReference, BiConsumer<T, ?> handler) {
        return field(ColumnReference.name(columnReference), handler);
    }

    public ResultSetMapper<T> field(ColumnReference columnReference, BiConsumer<T, ?> handler) {
        if (setters == null) {
            setters = new LinkedHashMap<>();
        }
        setters.put(
                Objects.requireNonNull(columnReference),
                Objects.requireNonNull(handler)
        );
        return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T apply(final ResultSet value) throws SQLException {
        var instance = instanceConstructor.apply(value);
        if (setters != null) {
            setters.forEach((k, v) -> {
                var val = k.read(value);
                var func = (BiConsumer<T, Object>) v;
                func.accept(instance, val);
            });
        }
        return instance;
    }
}
