package net.odoframework.sql;

import java.util.LinkedHashMap;
import java.util.Set;

public class ColumnIndex extends LinkedHashMap<String, Integer> {

    public ColumnIndex() {

    }

    public ColumnIndex extract(String... columns) {
        return extract(Set.of(columns));
    }

    public ColumnIndex extract(Set<String> columns) {
        var result = new ColumnIndex();
        for (String column : columns) {
            if (!this.containsKey(column)) {
                throw new IllegalArgumentException(column + " does not exist in this index");
            }
            result.put(column, get(column));
        }
        return result;
    }


}
