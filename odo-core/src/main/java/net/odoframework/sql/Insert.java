package net.odoframework.sql;

import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Insert extends BaseUpdateStatement {


    public Insert(String table) {
        super(table);
    }

    public Insert value(String name, Object value) {
        return value(name, () -> value);
    }

    public Insert value(String name, Supplier<?> value) {
        addField(name, value);
        return this;
    }

    public static Insert insertInto(String name) {
        return new Insert(name);
    }

    @Override
    public String getSql() {
        String sql = "insert into " + getTable() + " (";
        sql += String.join(", ", getFields().keySet());
        sql += ") values (";
        sql += getFields().keySet().stream().map(it -> "? ").collect(Collectors.joining(", ")).trim();
        sql += ")";
        return sql;
    }


}
