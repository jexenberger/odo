package net.odoframework.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Supplier;
import java.util.stream.Stream;

public interface SQLTemplate {

    int execute(DBStatement sql);

    void execute(DBStatement sql, SQLConsumer<ResultSet> rsConsumer);

    Connection getConnection() throws SQLException;

    <T> T execute(DBStatement sql, SQLFunction<ResultSet, T> mapper);

    <T> Stream<T> stream(DBStatement sql, SQLFunction<ResultSet, T> row);

    <T> T doInTransaction(Supplier<T> supplier);


}
