package net.odoframework.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ResultSetSpliterator implements Spliterator<ResultSet> {


    private final Query query;

    public ResultSetSpliterator(Query query) {
        this.query = query;
    }

    @Override
    public boolean tryAdvance(Consumer<? super ResultSet> action) {
        try {
            var rs = this.query.getResultSet();
            return SQLWrappedException.get(() -> {
                if (rs.next()) {
                    action.accept(rs);
                    return true;
                } else {
                    try {
                        query.close();
                    } catch (SQLException e) {
                        throw new SQLWrappedException(e);
                    }
                    return false;
                }
            });
        } catch (SQLWrappedException sqe) {
            try {
                query.close();
                throw sqe;
            } catch (SQLException e) {
                throw new SQLWrappedException(e);
            }
        }
    }

    @Override
    public Spliterator<ResultSet> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return 0;
    }

    @Override
    public int characteristics() {
        return 0;
    }
}
