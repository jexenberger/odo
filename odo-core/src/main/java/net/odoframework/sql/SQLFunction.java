package net.odoframework.sql;

import java.sql.SQLException;

@FunctionalInterface
public interface SQLFunction<T, K> {

    K apply(T value) throws SQLException;

}
