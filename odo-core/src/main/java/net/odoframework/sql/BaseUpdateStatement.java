package net.odoframework.sql;

import net.odoframework.util.Strings;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public abstract class BaseUpdateStatement implements DBStatement {
    private final String table;
    private Map<String, Supplier<?>> fields;

    public BaseUpdateStatement(String table) {
        this.table = Strings.requireNotBlank(table, "table cannot be blank");
    }

    public void addField(String name, Supplier<?> value) {
        if (fields == null) {
            fields = new LinkedHashMap<>();
        }
        fields.put(Strings.requireNotBlank(name, "name is a required parameter"), Objects.requireNonNull(value, "value is required"));

    }

    Map<String, Supplier<?>> getFields() {
        if (fields == null) {
            fields = new LinkedHashMap<>();
        }
        return fields;
    }

    public  Map<Integer, Object> getBindings() {
        if (fields == null) {
            return Collections.emptyMap();
        }
        var items = new LinkedHashMap<Integer, Object>(fields.size() + 1);
        var cnt = new AtomicInteger(1);
        fields.values().forEach(it -> items.put(cnt.getAndIncrement(), it.get()));
        return items;
    }

    public String getTable() {
        return table;
    }
}
