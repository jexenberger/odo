package net.odoframework;

import java.util.Objects;

public class WrappedException extends RuntimeException {

    private final Exception e;

    public WrappedException(String message, Exception e) {
        super(message);
        this.e = Objects.requireNonNull(e);
    }


    public WrappedException(Exception e) {
        super(e);
        this.e = Objects.requireNonNull(e);
    }

    public WrappedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Exception e) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.e = Objects.requireNonNull(e);
    }

    public Exception getException() {
        return e;
    }
}
