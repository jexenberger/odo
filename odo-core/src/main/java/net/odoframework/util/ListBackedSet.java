package net.odoframework.util;

import java.util.*;

import static java.util.Objects.requireNonNull;

public class ListBackedSet<E> extends AbstractSet<E> {

    private final List<E> values;

    public ListBackedSet(int size) {
        this.values = new ArrayList<>(size);
    }

    public ListBackedSet(List<E> values) {
        this.values = requireNonNull(values, "values is a required parameter");
    }

    @Override
    public Iterator<E> iterator() {
        return values.iterator();
    }

    @Override
    public boolean add(E e) {
        for (int i = 0; i < this.values.size(); i++) {
            var value = this.values.get(i);
            if (Objects.equals(e, value)) {
                this.values.set(i, e);
                return false;
            }
        }
        this.values.add(e);
        return true;
    }

    @Override
    public int size() {
        return values.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListBackedSet)) return false;
        if (!super.equals(o)) return false;
        ListBackedSet<?> that = (ListBackedSet<?>) o;
        return Objects.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), values);
    }
}
