package net.odoframework.util;

import java.io.*;
import java.util.Objects;
import java.util.Properties;

public class IO {


    public static void pipe(InputStream is, OutputStream os) {
        try {
            Objects.requireNonNull(is).transferTo(Objects.requireNonNull(os));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    public static void checkedLoad(ClasspathResource resource, Properties targetProperties) {
        var stream = resource.getStream();
        if (stream == null) {
            throw new IllegalStateException(resource.getPath()+" not found on classpath");
        }
        loadProperties(targetProperties, stream);
    }

    public static void uncheckedLoad(ClasspathResource resource, Properties targetProperties) {
        var stream = resource.getStream();
        if (stream == null) {
            return;
        }
        loadProperties(targetProperties, stream);
    }

    private static void loadProperties(Properties targetProperties, InputStream stream) {
        try {
            targetProperties.load(stream);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static char read(Reader reader) {
        try {
            final var read = reader.read();
            if (read == -1) {
                throw new EOFException();
            }
            return (char) read;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void unread(PushbackReader reader, char c) {
        try {
           reader.unread(c);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    public static String streamToString(InputStream is) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(is));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

}
