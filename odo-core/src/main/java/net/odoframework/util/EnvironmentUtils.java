package net.odoframework.util;

import java.util.Optional;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

public class EnvironmentUtils {

    public static Optional<String> resolve(String env, String sysProp) {
        String envVal = System.getenv(env);
        if (Strings.isBlank(envVal)) {
            return ofNullable(System.getProperty(sysProp));
        }
        return of(envVal);
    }

}
