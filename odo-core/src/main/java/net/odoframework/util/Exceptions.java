package net.odoframework.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Exceptions {

    public static final String toString(Throwable exception) {
        final var stringWriter = new StringWriter();
        exception.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public static final Throwable getRoot(Throwable exception) {
        Throwable root = exception;
        while (root.getCause() != null) {
            root = root.getCause();
        }
        return root;
    }

}
