package net.odoframework.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class Node<T> {

    private Node<T> parent;
    private final T value;
    private List<Node<T>> children;

    public Node(T value) {
        this.value = value;
    }

    public Node(T value, List<Node<T>> children) {
        this.value = value;
        this.children = children;
    }

    public Node(Node<T> parent, T value, List<Node<T>> children) {
        this.parent = parent;
        this.value = value;
        this.children = children;
    }

    public List<Node<T>> getChildren() {
        if (children == null) {
            children = new ArrayList<>();
        }
        return children;
    }


    public Node<T> add(T value) {
        getChildren().add(new Node<>(this, value, null));
        return this;
    }

    public Node<T> append(Node<T> node) {
        getChildren().add(node.parent(this));
        return this;
    }

    public Node<T> parent(Node<T> node) {
        this.parent = node;
        return this;
    }

    public Node<T> prune() {
        if (!this.hasChildren()) {
            return this;
        }
        final var newChildren = new ArrayList<>(this.children);
        var itr = newChildren.iterator();
        while (itr.hasNext()) {
            var next = itr.next();
            if (!next.hasChildren()) {
                next.prune();
            } else {
                itr.remove();
            }

        }
        return new Node<>(this.value, newChildren);
    }

    public List<Node<T>> getLeafNodes() {
        if (!hasChildren()) {
            return Collections.singletonList(this);
        }
        List<Node<T>> ret = new ArrayList<>();
        for (Node<T> child : children) {
            ret.addAll(child.getLeafNodes());
        }
        return ret;
    }

    public List<T> getLeaves() {
        if (!hasChildren()) {
            return Collections.singletonList(this.getValue());
        }
        List<T> ret = new ArrayList<T>();
        for (Node<T> child : children) {
            ret.addAll(child.getLeaves());
        }
        return ret;
    }

    public void visitDepthFirst(Consumer<T> visitor) {
        visitor.accept(this.value);
        for (Node<T> child : children) {
            child.visitDepthFirst(visitor);
        }
    }

    protected void visitBreadthFirst(Consumer<T> visitor) {
        visitBreadthFirst(true, visitor);
    }

    protected void visitBreadthFirst(boolean visitRoot, Consumer<T> visitor) {
        if (visitRoot) {
            visitor.accept(this.value);
        }
        for (Node<T> child : children) {
            visitor.accept(child.value);
        }
        for (Node<T> child : children) {
            child.visitBreadthFirst(false, visitor);
        }
    }

    public boolean hasChildren() {
        return this.children != null && !this.children.isEmpty();
    }

    public T getValue() {
        return value;
    }
}
