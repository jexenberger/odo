package net.odoframework.util;

@FunctionalInterface
public interface Encoder<T, K, Z> {

    K encode(T value, Z context);

}
