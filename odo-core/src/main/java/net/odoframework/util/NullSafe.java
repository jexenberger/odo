package net.odoframework.util;

import java.util.function.Supplier;

public class NullSafe {


    public static <T> T safe(Supplier<T> supplier) {
        try {
            return supplier.get();
        } catch (NullPointerException e) {
            return null;
        }
    }

    public static <T> T safe(Supplier<T> supplier, T defaultValue) {
        try {
            return supplier.get();
        } catch (NullPointerException e) {
            return defaultValue;
        }
    }

    public static <T> T ifNullReturn(T a, T elseValue) {
        if (a == null) {
            return elseValue;
        }
        return a;
    }

}
