package net.odoframework.util;

import java.util.Collection;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class Assertions {

    public static <T> T notNull(T instance, String field) {
        return requireNonNull(instance, field + " is required");
    }

    public static String notBlank(String instance, String field) {
        return Strings.requireNotBlank(instance, field + " cannot be null or blank");
    }

    public static <Z, T extends Collection<Z>> T notEmpty(T instance, String field) {
        return ofNullable(instance)
                .filter(it -> !it.isEmpty())
                .orElseThrow(() -> new IllegalArgumentException(field + " cannot be null or empty"));
    }

    public static <T> T[] notEmptyArray(T[] instance, String field) {
        return ofNullable(instance)
                .filter(it -> it.length > 0)
                .orElseThrow(() -> new IllegalArgumentException(field + " cannot be null or empty"));
    }

    public static IllegalArgumentException notExisting(String field) {
        return new IllegalArgumentException(field + " does not exist");
    }

    public static <T extends Number> T assertPositive(T number, String field) {
        if (number.doubleValue() < 1.0) {
            throw new IllegalArgumentException(field + " must be greater than 1");
        }
        return number;
    }

}
