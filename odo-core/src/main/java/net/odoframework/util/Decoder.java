package net.odoframework.util;

@FunctionalInterface
public interface Decoder<T, K, Z> {

    K decode(T format, Z context);

}
