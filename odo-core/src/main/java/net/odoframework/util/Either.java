package net.odoframework.util;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public abstract class Either<T, K> {


    public static final <T, K> Either<T, K> left(T value) {
        return new Left<>(value);
    }

    public static final <T, K> Either<T, K> right(K value) {
        return new Right<>(value);
    }

    public boolean isLeft() {
        return this instanceof Left;
    }

    public T left() {
        return (isLeft())
                ? ((Left<T, K>) this).getValue()
                : null;
    }

    public K right() {
        return (!isLeft())
                ? ((Right<T, K>) this).getValue()
                : null;
    }

    public <Z> Optional<Z> mapLeft(Function<T, Z> handler) {
        return (isLeft())
                ? Optional.ofNullable(((Left<T, K>) this).map(handler))
                : Optional.empty();
    }

    public <Z> Optional<Z> mapRight(Function<K, Z> handler) {
        return (!isLeft())
                ? Optional.ofNullable(((Right<T, K>) this).map(handler))
                : Optional.empty();
    }

    private static final class Left<T, K> extends Either<T, K> implements Monad<T> {

        private final T value;

        private Left(T value) {
            this.value = Objects.requireNonNull(value);
        }

        public T getValue() {
            return value;
        }

        @Override
        public <K> K map(Function<T, K> handler) {
            return handler.apply(value);
        }

        @Override
        public String toString() {
            return "Left{" +
                    value +
                    '}';
        }
    }

    private static final class Right<T, K> extends Either<T, K> implements Monad<K> {

        private final K value;

        private Right(K value) {
            this.value = Objects.requireNonNull(value);
        }

        public K getValue() {
            return value;
        }

        @Override
        public <Z> Z map(Function<K, Z> handler) {
            return handler.apply(this.value);
        }

        @Override
        public String toString() {
            return "Right{" +
                    value +
                    '}';
        }
    }


}
