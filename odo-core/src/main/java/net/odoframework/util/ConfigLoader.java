package net.odoframework.util;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.of;

public class ConfigLoader {


    public static final String CONFIG_OR = "\\|";

    public static Properties loadProperties(String... files) {
        return loadProperties(ConfigLoader.class, files);
    }

    public static Properties loadProperties(Class<?> source, String... files) {
        Properties properties = new Properties();
        for (String file : files) {
            load(source, file).ifPresent(properties::putAll);
        }
        System.getenv().forEach((key, value) -> properties.put("env." + key, value));
        properties.putAll(System.getProperties());
        return interpolate(properties);
    }

    private static Properties interpolate(Properties p) {
        var newProperties = new Properties();
        p.forEach((key, value) -> {
            var theValue = (value != null) ? value.toString().trim() : "";
            if (theValue.startsWith("${") && theValue.endsWith("}")) {
                theValue = theValue.substring(2);
                theValue = theValue.substring(0, theValue.length()-1);
                if (theValue.contains("|")) {
                    var parts = theValue.split(CONFIG_OR);
                    if (parts.length > 1 && !p.containsKey(parts[0].trim())) {
                        theValue = p.getProperty(parts[1].trim(), parts[1].trim());
                    } else {
                        theValue = p.getProperty(parts[0],"").trim();
                    }
                } else {
                    theValue = p.getProperty(theValue, "");
                }
            }
            newProperties.setProperty(key.toString(), theValue);
        });
        return newProperties;
    }

    private static Optional<Properties> load(Class<?> sourceClass, String name) {
        if (sourceClass.getModule() == null) {
            return loadFile(sourceClass, name);
        } else {
            return resolve(name, sourceClass.getModule());
        }
    }

    private static Optional<Properties> loadFile(Class<?> sourceClass, String file) {
        try {
            var properties = new Properties();
            final var resourceAsStream = sourceClass.getResourceAsStream(file);
            if (resourceAsStream == null) {
                return Optional.empty();
            }
            properties.load(resourceAsStream);
            return of(properties);
        } catch (IOException e) {
            throw new IllegalArgumentException(file + " could not be loaded");
        }
    }

    public static Optional<Properties> resolve(String name, Module sourceModule) {
        try {
            var resource = sourceModule.getResourceAsStream(name);
            if (resource != null) {
                var props = new Properties();
                props.load(resource);
                return of(props);
            }
        } catch (IOException e) {
            throw new IllegalStateException("unable to load " + name + ": " + e.getMessage());
        }
        var properties = new Properties();
        ModuleLayer
                .boot()
                .modules()
                .stream()
                .filter(ConfigLoader::filterModule)
                .map(it -> {
                    try {
                        return it.getResourceAsStream(name);
                    } catch (IOException e) {
                        throw new IllegalStateException("unable to load " + name + ": " + e.getMessage());
                    }
                })
                .filter(Objects::nonNull)
                .map(it -> {
                    var props = new Properties();
                    try {
                        props.load(it);
                    } catch (IOException e) {
                        throw new IllegalStateException("unable to load " + name + ": " + e.getMessage());
                    }
                    return props;
                }).forEach(properties::putAll);
        return of(properties);
    }

    private static boolean filterModule(Module it) {
        return !it.getName().startsWith("java")
                && !it.getName().startsWith("javax")
                && !it.getName().startsWith("jdk")
                && !it.getName().startsWith("jakarta")
                && !it.getName().startsWith("kotlin")
                && !it.getName().startsWith("org.graalvm")
                && !it.getName().startsWith("lombok")
                && !it.getName().startsWith("com.google")
                && !it.getName().startsWith("org.slf4j")
                && !it.getName().startsWith("org.eclipse")
                && !it.getName().startsWith("org.apache");
    }


    public static Properties findByPrefix(Properties p, String prefix, boolean stripPrefix) {
        var result = new Properties();
        result.putAll(requireNonNull(p, "properties is a required parameter")
                .entrySet()
                .stream()
                .filter(it -> it.getKey().toString().startsWith(prefix))
                .filter(it -> it.getValue() != null)
                .collect(Collectors.toMap(entry -> {
                    if (stripPrefix) {
                        return stripPrefix(entry.getKey().toString(), prefix);
                    } else {
                        return entry.getKey();
                    }
                }, Map.Entry::getValue)));
        return result;
    }

    public static String stripPrefix(String str, String prefix) {
        if (Strings.isBlank(str) || Strings.isBlank(prefix)) {
            return prefix;
        }
        if (!str.startsWith(prefix)) {
            throw new IllegalArgumentException(str + " does not start with the the prefix: " + prefix);
        }
        //add one to strip of leading '.'
        var remainder = str.substring(prefix.length());
        if (remainder.startsWith(".") && remainder.length() > 1) {
            return remainder.substring(1).trim();
        } else if (remainder.startsWith(".")) {
            return "";
        } else {
            return remainder.trim();
        }
    }


}
