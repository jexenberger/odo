package net.odoframework.util.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ConfigurationKey {

    public static final String DEFAULT_SOURCE = "properties";

    private String source;
    private String key;
    private String defaultValue;


    public static ConfigurationKey parse(String string) {
        String[] parts = string.split("://");
        int idx = (parts.length == 1)
                ? 0
                : 1;
        String source = (idx == 0)
                ? DEFAULT_SOURCE
                : parts[0];
        String[] valueDefault = parts[idx].split("\\|");
        String defaultValue = (valueDefault.length > 1)
                ? valueDefault[valueDefault.length - 1]
                : null;
        int arrSize = (valueDefault.length > 1)
                ? valueDefault.length - 1
                : 1;
        var target = new String[arrSize];
        System.arraycopy(valueDefault, 0, target, 0, arrSize);
        String value = String.join(":", target);
        return new ConfigurationKey(source, value, defaultValue);
    }

    @Override
    public String toString() {
        return source + "://" + key + ((defaultValue != null) ? ":" + defaultValue : "");
    }
}
