package net.odoframework.util.configuration;

import net.odoframework.util.ClasspathResource;
import net.odoframework.util.IO;
import net.odoframework.util.Strings;

import java.util.Optional;
import java.util.Properties;

public class ApplicationPropertiesConfigurationSource implements ConfigurationSource {

    public static final String CONFIG_PREFIX = "application";
    public static final String SYSTEM_ENV_PREFIX = "odo.env";
    public static final String ENV_ENV_PREFIX = "ODO_ENV";

    private final String configFilePrefix;
    private final String systemEnvironmentPrefix;
    private final String environmentEnvironmentPrefix;
    private final Properties properties;


    public ApplicationPropertiesConfigurationSource(String configFilePrefix, String systemEnvironmentPrefix, String environmentEnvironmentPrefix) {
        this.configFilePrefix = configFilePrefix;
        this.systemEnvironmentPrefix = systemEnvironmentPrefix;
        this.environmentEnvironmentPrefix = environmentEnvironmentPrefix;
        this.properties = new Properties();
        load();
    }

    public ApplicationPropertiesConfigurationSource() {
        this(CONFIG_PREFIX, SYSTEM_ENV_PREFIX, ENV_ENV_PREFIX);
    }

    @Override
    public String getIdentifier() {
        return ConfigurationKey.DEFAULT_SOURCE;
    }

    @Override
    public Optional<String> getConfigurationValue(String key) {
        return Optional.ofNullable(properties.getProperty(key));
    }

    private String getEnvironment() {
        var env = System.getenv(environmentEnvironmentPrefix);
        if (Strings.isBlank(env)) {
            return System.getProperty(systemEnvironmentPrefix);
        }
        return env;
    }

    private void load() {
        final var fileName = configFilePrefix + ".properties";
        final var resource = new ClasspathResource(fileName);
        IO.uncheckedLoad(resource, properties);

        //override with environment settings
        final var environment = getEnvironment();
        if (environment != null) {
            final var envFile = configFilePrefix + "-" + environment + ".properties";
            IO.checkedLoad(new ClasspathResource(envFile), properties);
        }

        //override with system properties
        properties.putAll(System.getProperties());

        //register environment variables
        System.getenv()
                .forEach((k, v) -> properties.put("env." + k, v));
    }
}
