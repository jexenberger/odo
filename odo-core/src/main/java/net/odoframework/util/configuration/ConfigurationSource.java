package net.odoframework.util.configuration;

import java.util.Optional;

public interface ConfigurationSource {

    String getIdentifier();

    Optional<String> getConfigurationValue(String key);

}
