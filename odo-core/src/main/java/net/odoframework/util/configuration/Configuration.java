package net.odoframework.util.configuration;

import net.odoframework.util.Strings;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static net.odoframework.expressions.StringInterpolator.interpolate;

public class Configuration {

    private Map<String, ConfigurationSource> configurationSourceMap;


    public Configuration() {
        addSource(new SimpleMapSource());
    }

    public Configuration(ConfigurationSource defaultConfigurationSource) {
        addSource(defaultConfigurationSource);
    }


    private String resolve(ConfigurationKey key) {
        Optional<String> result = resolveKey(key);
        return result.orElseGet(() -> {
            if (key.getDefaultValue() == null) {
                throw new IllegalStateException("unknown configuration value " + key);
            }
            return key.getDefaultValue();
        });
    }

    private Optional<String> resolveKey(ConfigurationKey key) {
        var source = Optional
                .ofNullable(configurationSourceMap.get(key.getSource()))
                .orElseThrow(() -> new IllegalStateException("unknown configuration source " + key.getSource()));

        return source.getConfigurationValue(key.getKey());
    }

    public  String value(String sKey) {
        return value(sKey, null);
    }

    public  String value(String sKey, String defaultValue) {
        Strings.requireNotBlank(sKey, "key cannot be null");
        var key = ConfigurationKey.parse(sKey);
        return resolveKey(key)
                .map(it -> interpolate(it).parse(variable -> resolve(ConfigurationKey.parse(variable))))
                .orElse(defaultValue);
    }

    public  Boolean valueAsBoolean(String sKey) {
        return valueAsBoolean(sKey, null);
    }

    public  Integer valueAsInt(String sKey) {
        return valueAsInt(sKey, null);
    }

    public  Long valueAsLong(String sKey) {
        return valueAsLong(sKey, null);
    }

    public  Double valueAsDouble(String sKey) {
        return valueAsDouble(sKey, null);
    }

    public  Boolean valueAsBoolean(String sKey, Boolean defaultValue) {
        return Optional.ofNullable(value(sKey)).map(Boolean::valueOf).orElse(defaultValue);
    }

    public  Integer valueAsInt(String sKey, Integer defaultValue) {
        return Optional.ofNullable(value(sKey)).map(Integer::valueOf).orElse(defaultValue);
    }

    public  Long valueAsLong(String sKey, Long defaultValue) {
        return Optional.ofNullable(value(sKey)).map(Long::valueOf).orElse(defaultValue);
    }

    public  Double valueAsDouble(String sKey, Double defaultValue) {
        return Optional.ofNullable(value(sKey)).map(Double::valueOf).orElse(defaultValue);
    }

    public Configuration addSource(ConfigurationSource source) {
        if (configurationSourceMap == null) {
            configurationSourceMap = new LinkedHashMap<>();
        }
        configurationSourceMap.put(source.getIdentifier(), source);
        return this;
    }
}
