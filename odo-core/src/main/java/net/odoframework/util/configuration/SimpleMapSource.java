package net.odoframework.util.configuration;

import net.odoframework.util.Pair;

import java.util.*;

public class SimpleMapSource extends AbstractMap<String, String> implements ConfigurationSource{


    private Map<String, String> config;

    public SimpleMapSource() {
        this(Collections.emptyMap());
    }

    public SimpleMapSource(Map<String, String> config) {
        this.config = new LinkedHashMap<>();
        this.config.putAll(config);
    }



    @Override
    public String getIdentifier() {
        return ConfigurationKey.DEFAULT_SOURCE;
    }

    @Override
    public Optional<String> getConfigurationValue(String key) {
        return Optional.ofNullable(config.get(key));
    }

    @Override
    public Set<Entry<String, String>> entrySet() {
        return this.config.entrySet();
    }

    @Override
    public String put(String key, String value) {
        final var old = this.config.get(key);
        this.config.put(key, value);
        return old;
    }

    public SimpleMapSource add(String key, String value) {
        put(key, value);
        return this;
    }

    public  Configuration toConfig() {
        return new Configuration(this);

    }

}
