package net.odoframework.util;

import java.util.*;

public class TrackedMap<K, V> implements Map<K, V> {

    private final Map<K, V> backingMap;
    private final Set<K> changedKeys;

    public TrackedMap(Map<K, V> backingMap) {
        this.backingMap = Objects.requireNonNull(backingMap, "backing map is a required parameter");
        changedKeys = new LinkedHashSet<>();
    }

    public TrackedMap() {
        this(new HashMap<>());
    }

    @Override
    public int size() {
        return backingMap.size();
    }

    @Override
    public boolean isEmpty() {
        return backingMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return backingMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return backingMap.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return backingMap.get(key);
    }

    @Override
    public V put(K key, V value) {
        changedKeys.add(key);
        return backingMap.put(key, value);
    }

    @Override
    public V remove(Object key) {
        changedKeys.add((K) key);
        return backingMap.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        m.forEach(this::put);
    }

    @Override
    public void clear() {
        changedKeys.addAll(backingMap.keySet());
        backingMap.clear();
    }

    @Override
    public Set<K> keySet() {
        return backingMap.keySet();
    }

    @Override
    public Collection<V> values() {
        return backingMap.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return backingMap.entrySet();
    }

    public Map<K, V> getBackingMap() {
        return Collections.unmodifiableMap(backingMap);
    }

    public Set<K> getChangedKeys() {
        return Collections.unmodifiableSet(changedKeys);
    }

}
