package net.odoframework.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Objects;

public class ClasspathResource implements Resource{

    private final String path;
    private final ClassLoader loader;



    public ClasspathResource(String path) {
        this(path, Thread.currentThread().getContextClassLoader());
    }

    public ClasspathResource(String path, ClassLoader loader) {
        this.path = Strings.requireNotBlank(path, "path is a required parameter");
        this.loader = Objects.requireNonNull(loader, "loader is a required parameter");
    }

    public String getPath() {
        return path;
    }

    @Override
    public InputStream getStream() {
        return loader.getResourceAsStream(path);
    }


}
