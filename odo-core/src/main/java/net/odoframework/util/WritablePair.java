package net.odoframework.util;

import java.util.Map;

public class WritablePair<T, K> extends Pair<T, K> implements Map.Entry<T, K> {

    public WritablePair(T left, K right) {
        super(left, right);
    }

    @Override
    public T getKey() {
        return getLeft();
    }

    @Override
    public K getValue() {
        return getRight();
    }

    @Override
    public K setValue(K value) {
        return this.right = value;
    }

}
