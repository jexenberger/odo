package net.odoframework.util;

import java.util.function.Function;

public interface Monad<T> {

    <K> K map(Function<T, K> handler);

}
