package net.odoframework.util;

import java.util.Arrays;

public class Strings {

    public static boolean isBlank(String str) {
        return (str == null || str.trim().equals(""));
    }

    public static boolean isNotBlank(String str) {
        return str != null && !str.isBlank();
    }

    public static String blankIfNull(String str) {
        return (str != null) ? str : "";
    }


    public static String requireNotBlank(String str, String message) {
        if (isBlank(str)) {
            throw new IllegalArgumentException(message);
        }
        return str;
    }

    public static String requireNotBlank(String str) {
        return requireNotBlank(str, "Should have been a non-null, non-blank String");
    }

    public static boolean isOneOf(String actual, String... possibleValues) {
        return Arrays.asList(possibleValues).contains(actual);
    }

    public static String times(String str, int times) {
        var builder = new StringBuilder();
        for (int i = 0; i < times; i++) {
            builder = builder.append(str);
        }
        return builder.toString();
    }

    public static String defaultIfBlank(String str, String def) {
        if (isBlank(str)) {
            return def;
        }
        return str;
    }
}
