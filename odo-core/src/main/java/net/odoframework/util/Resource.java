package net.odoframework.util;

import java.io.InputStream;

public interface Resource {

    InputStream getStream();

    default String getString() {
        return IO.streamToString(getStream());
    }

}
