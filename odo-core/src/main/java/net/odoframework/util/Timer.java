package net.odoframework.util;

public class Timer {

    private final long start;

    private Timer() {
        start = System.currentTimeMillis();
    }

    public static Timer start() {
        return new Timer();
    }

    public static long timeTaken(Runnable runnable) {
        var timer = Timer.start();
        runnable.run();
        return timer.timeTaken();
    }

    public long timeTaken() {
        return System.currentTimeMillis() - start;
    }

}
