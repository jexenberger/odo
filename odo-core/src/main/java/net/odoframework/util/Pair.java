package net.odoframework.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class Pair<T, K> {

    T left;
    K right;

    public Pair(T left, K right) {
        this.left = Objects.requireNonNull(left);
        this.right = right;
    }

    public static <A, B> Pair<A, B> cons(A left, B right) {
        return new Pair<>(left, right);
    }

    @SafeVarargs
    public static <T, K> Map<T, K> toMap(Pair<T, K>... pairs) {
        var map = new LinkedHashMap<T, K>();
        for (var tkEntry : pairs) {
            map.put(tkEntry.getLeft(), tkEntry.getRight());
        }
        return map;
    }

    public T getLeft() {
        return left;
    }

    public K getRight() {
        return right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return left.equals(pair.left) && Objects.equals(right, pair.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }
}
