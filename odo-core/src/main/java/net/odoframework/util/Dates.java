package net.odoframework.util;

import java.time.ZoneId;
import java.time.chrono.ChronoLocalDateTime;
import java.util.Date;

public class Dates {

    public static Date toDate(ChronoLocalDateTime<?> dateToConvert) {
        return java.util.Date
                .from(dateToConvert.atZone(ZoneId.systemDefault())
                        .toInstant());
    }

}
