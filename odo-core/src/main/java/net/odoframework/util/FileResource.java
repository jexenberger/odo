package net.odoframework.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class FileResource implements Resource {

    private final String path;

    public FileResource(String path) {
        this.path = Strings.requireNotBlank(path, "path is a required parameter");
    }

    @Override
    public InputStream getStream() {
        try {
            return new FileInputStream(this.path);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
