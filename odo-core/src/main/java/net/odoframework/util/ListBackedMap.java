package net.odoframework.util;

import java.util.*;
import java.util.stream.Collectors;

public class ListBackedMap<T, K> extends AbstractMap<T, K> {

    private final List<WritablePair<T, K>> value;


    public ListBackedMap(int size) {
        value = new ArrayList<>(size);
    }

    public ListBackedMap() {
        value = new LinkedList<>();
    }

    public ListBackedMap(T key, K value) {
        this();
        this.value.add(new WritablePair<>(key, value));
    }


    @Override
    public Set<Entry<T, K>> entrySet() {
        return value
                .stream()
                .map(it -> (Entry<T, K>) it)
                .collect(Collectors.toCollection(() -> new ListBackedSet<>(value.size())));
    }

    @Override
    public K put(T key, K value) {
        for (var o : this.value) {
            if (Objects.equals(o.getKey(), key)) {
                var old = o.getValue();
                o.setValue(value);
                return old;
            }
        }
        this.value.add(new WritablePair<>(key, value));
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListBackedMap)) return false;
        if (!super.equals(o)) return false;
        ListBackedMap<?, ?> that = (ListBackedMap<?, ?>) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), value);
    }
}
