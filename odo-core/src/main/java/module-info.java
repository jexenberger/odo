import net.odoframework.annotations.FrameworkModule;

@FrameworkModule
module odo.core {
    requires java.sql;
    requires java.naming;

    requires static lombok;

    exports net.odoframework.annotations;
    exports net.odoframework.util;
    exports net.odoframework.http;
    exports net.odoframework.sql;
    exports net.odoframework.beans;

    opens net.odoframework.util;
    opens net.odoframework.http;
    opens net.odoframework.sql;
    opens net.odoframework.beans;
    exports net.odoframework.beans.types;
    opens net.odoframework.beans.types;
    opens net.odoframework.expressions;
    exports net.odoframework.util.configuration;
    opens net.odoframework.util.configuration;

}