package net.odoframework.expressions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringInterpolatorTest {

    private static Map<String, String> DATA = Map.of(
            "a", "1",
            "test", "test"
    );

    @Test
    void simpleForm() {

        var expression = "this is a ${test}";
        testResult(expression, "this is a test");

        expression = "this is ${a} ${test}";
        testResult(expression, "this is 1 test");

    }

    @Test
    void testUnbalancedVariable() {

        Assertions.assertThrows(IllegalStateException.class, () -> {
            var expression = "this is a ${test";
            testResult(expression, "this is a test");

        });

    }

    @Test
    void testNestedVariable() {

        Assertions.assertThrows(IllegalStateException.class, () -> {
            var expression = "this is a ${test${a}}";
            testResult(expression, "this is a test");
        });

    }

    @Test
    void testEscapes() {
        var expression = "this is a \\${test}";
        testResult(expression, "this is a ${test}");

         expression = "this is \\a ${test}";
        testResult(expression, "this is \\a test");

    }

    private void testResult(String expression, String expected) {
        var result = new StringInterpolator(expression).parse(it -> DATA.get(it));
        assertEquals(expected, result);
    }
}
