package net.odoframework.util;

import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

public class ConfigLoaderTest {

    @Test
    void loadProperties() {
        var properties = ConfigLoader.loadProperties("test.properties", "test-override.properties");
        System.out.println(properties);
        assertEquals("1", properties.getProperty("a"));
        assertEquals("2", properties.getProperty("b"));
        assertEquals("999", properties.getProperty("c"));
        assertEquals("2", properties.getProperty("d"));
        assertEquals("2", properties.getProperty("e"));
        assertEquals("", properties.getProperty("f"));
        assertEquals("5", properties.getProperty("g"));
    }

    @Test
    void findByPrefix() {
        var p = new Properties();
        p.put("test.1", "test1");
        p.put("test.2", "test2");
        p.put("x.1", "x1");
        var result = ConfigLoader.findByPrefix(p, "test", false);
        assertEquals(2, result.size());
        assertEquals("test1", result.getProperty("test.1"));
        assertEquals("test2", result.getProperty("test.2"));
        assertFalse(result.containsKey("x.1"));
    }

    @Test
    void findByPrefixStripped() {
        var p = new Properties();
        p.put("test.1", "test1");
        p.put("test.2", "test2");
        p.put("x.1", "x1");
        var result = ConfigLoader.findByPrefix(p, "test", true);
        assertEquals(2, result.size());
        assertEquals("test1", result.getProperty("1"));
        assertEquals("test2", result.getProperty("2"));
        assertFalse(result.containsKey("x.1"));
    }

    @Test
    void testStripPrefix() {
        var test = "test.prefix.remainder";
        assertEquals("remainder", ConfigLoader.stripPrefix(test, "test.prefix"));
        assertEquals("", ConfigLoader.stripPrefix("test.prefix", "test.prefix"));
        assertEquals("remainder", ConfigLoader.stripPrefix("test.prefixremainder", "test.prefix"));
        assertThrows(IllegalArgumentException.class, () -> ConfigLoader.stripPrefix("qwerty.fredperte", "test.prefix"));

    }
}
