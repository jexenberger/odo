package net.odoframework.util;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TrackedMapTest {

    @Test
    void testChangePut() {
        var trackingMap = new TrackedMap<String, String>();
        trackingMap.put("1", "a");
        trackingMap.put("2", "b");
        trackingMap.put("3", "c");
        assertEquals(3, trackingMap.getChangedKeys().size());
        assertTrue(trackingMap.getChangedKeys().contains("1"));
        assertTrue(trackingMap.getChangedKeys().contains("2"));
        assertTrue(trackingMap.getChangedKeys().contains("3"));
    }

    @Test
    void testDeleted() {
        var backing = new HashMap<String, String>();
        backing.put("1", "a");
        backing.put("2", "b");
        backing.put("3", "c");

        var trackingMap = new TrackedMap<>(backing);
        trackingMap.remove("3");
        assertTrue(trackingMap.getChangedKeys().contains("3"));

        trackingMap.put("3", "test");
        trackingMap.remove("3");
        assertTrue(trackingMap.getChangedKeys().contains("3"));

    }

}
