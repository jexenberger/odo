package net.odoframework.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ExceptionsTest {

    @Test
    void testToString() {
        var result = Exceptions.toString(new RuntimeException("borked"));
        System.out.println(result);
        assertNotNull(result);
    }
}
