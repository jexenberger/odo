package net.odoframework.util.configuration;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ConfigurationKeyTest {

    @Test
    void parseAll() {
        String value = "test://a.test.key|hello world";
        final var key = ConfigurationKey.parse(value);
        assertEquals("test", key.getSource());
        assertEquals("a.test.key", key.getKey());
        assertEquals("hello world", key.getDefaultValue());
    }

    @Test
    void parseNoSource() {
        String value = "a.test.key|hello world";
        final var key = ConfigurationKey.parse(value);
        assertEquals(ConfigurationKey.DEFAULT_SOURCE, key.getSource());
        assertEquals("a.test.key", key.getKey());
        assertEquals("hello world", key.getDefaultValue());
    }


    @Test
    void parseSourceNoDefault() {
        String value = "test://a.test.key";
        final var key = ConfigurationKey.parse(value);
        assertEquals("test", key.getSource());
        assertEquals("a.test.key", key.getKey());
        assertNull(key.getDefaultValue());
    }

    @Test
    void parseNoDefault() {
        String value = "a.test.key";
        final var key = ConfigurationKey.parse(value);
        assertEquals(ConfigurationKey.DEFAULT_SOURCE, key.getSource());
        assertEquals("a.test.key", key.getKey());
        assertNull(key.getDefaultValue());
    }

    @Test
    void parseNestedDefaultsDefault() {
        String value = "a.test.key:a:b|c";
        final var key = ConfigurationKey.parse(value);
        assertEquals(ConfigurationKey.DEFAULT_SOURCE, key.getSource());
        assertEquals("a.test.key:a:b", key.getKey());
        assertEquals("c", key.getDefaultValue());
    }
}
