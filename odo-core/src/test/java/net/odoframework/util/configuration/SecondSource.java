package net.odoframework.util.configuration;

import java.util.Map;

public class SecondSource extends SimpleMapSource{
    public SecondSource(Map<String, String> config) {
        super(config);
    }

    @Override
    public String getIdentifier() {
        return "second";
    }
}
