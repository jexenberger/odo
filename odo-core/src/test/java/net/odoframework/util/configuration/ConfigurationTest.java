package net.odoframework.util.configuration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConfigurationTest {

    private Configuration configuration;

    @BeforeEach
    void setUp() {
        final var source = new SimpleMapSource(Map.of(
                "one", "1",
                "two", "2",
                "three", "${one}:${two}",
                "four", "hello ${second://qwerty}",
                "five", "hello ${second://not.exist|aDefaultValue}",
                "six","true",
                "seven","1000.0"
        ));
        final var second = new SecondSource(Map.of(
                "qwerty", "fredperte"
        ));

        configuration = new Configuration();
        configuration.addSource(source);
        configuration.addSource(second);

        System.clearProperty("odo.env");
    }

    @Test
    void shouldResolveSimpleValue() {
        var lookup = configuration.value("one");
        assertEquals("1", lookup);
    }

    @Test
    void shouldResolveInterpolatedValue() {
        var lookup = configuration.value("three");
        assertEquals("1:2", lookup);
    }

    @Test
    void shouldResolveSecondSourceInterpolatedValue() {
        var lookup = configuration.value("four");
        assertEquals("hello fredperte", lookup);
    }

    @Test
    void shouldResolveSecondSourceInterpolatedValueWithDefault() {
        var lookup = configuration.value("five");
        assertEquals("hello aDefaultValue", lookup);
    }

    @Test
    void testDefault() {
        var newConfig = new Configuration(new ApplicationPropertiesConfigurationSource());
        assertEquals("hello world", newConfig.value("test"));
    }

    @Test
    void testValueAsInt() {
        var lookup = configuration.valueAsInt("one");
        assertEquals(1, lookup);

        lookup = configuration.valueAsInt("DOES.NOT.EXIST", 100);
        assertEquals(100, lookup);
    }

    @Test
    void testValueAsLong() {
        var lookup = configuration.valueAsInt("one");
        assertEquals(1, lookup);

        lookup = configuration.valueAsInt("DOES.NOT.EXIST", 100);
        assertEquals(100, lookup);
    }

    @Test
    void testValueAsDouble() {
        var lookup = configuration.valueAsDouble("seven");
        assertEquals(1000.0, lookup);

        lookup = configuration.valueAsDouble("DOES.NOT.EXIST", 100.0);
        assertEquals(100.0, lookup);
    }

    @Test
    void testValueAsBoolean() {
        var lookup = configuration.valueAsBoolean("six");
        assertTrue(lookup);

        lookup = configuration.valueAsBoolean("DOES.NOT.EXIST", true);
        assertTrue(lookup);
    }
}
