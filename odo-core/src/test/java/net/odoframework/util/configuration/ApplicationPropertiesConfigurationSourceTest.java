package net.odoframework.util.configuration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ApplicationPropertiesConfigurationSourceTest {


    private ApplicationPropertiesConfigurationSource source;

    @BeforeEach
    void setUp() {
        System.setProperty("odo.env", "override");
        source = new ApplicationPropertiesConfigurationSource("test","odo.env","odo_env");
    }

    @Test
    void shouldLoadWithOverride() {
        assertEquals("1", source.getConfigurationValue("a").orElseThrow());
        assertEquals("999", source.getConfigurationValue("c").orElseThrow());
    }

    @Test
    void shouldLoadWithNoOverride() {
        var source = new ApplicationPropertiesConfigurationSource("test","odo.env.does.not.exist","odo_env");
        assertEquals("3", source.getConfigurationValue("c").orElseThrow());

    }

    @Test
    void shouldLoadSystemAndEnvironmentVariables() {
        var envKey = System.getenv().keySet().stream().findFirst().orElseThrow();
        var envValue = System.getenv(envKey);

        assertEquals(envValue, source.getConfigurationValue("env."+envKey).orElseThrow());
        assertEquals("override", source.getConfigurationValue("odo.env").orElseThrow());
    }
}
