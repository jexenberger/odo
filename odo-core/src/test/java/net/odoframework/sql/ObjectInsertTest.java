package net.odoframework.sql;

import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ObjectInsertTest {

    private static JDBCDataSource ds;
    private static DefaultSQLTemplate template;

    @BeforeAll
    static void setUp() {
        ds = new JDBCDataSource();
        ds.setUrl("jdbc:hsqldb:mem:object_updates");
        ds.setUser("sa");
        ds.setPassword("");
        template = new DefaultSQLTemplate(ds);
        template.execute(SQLStatement.sql("create table test_ins ( test_id numeric(10), test varchar(255), name varchar(255))"));
    }

    @Test
    void testGetSQL() {
        var anObject = new AnObject(1L, "test", "name");
        var stmt = new ObjectInsert<>("test_ins", anObject)
                .addField("test_id", AnObject::getId)
                .addField("test", AnObject::getTest)
                .addField("name", AnObject::getName);
        var sql = stmt.getSql();
        var result = template.execute(stmt);
        assertEquals(1, result);

    }
}
