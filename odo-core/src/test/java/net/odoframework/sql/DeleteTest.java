package net.odoframework.sql;

import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeleteTest {

    private static JDBCDataSource ds;
    private static DefaultSQLTemplate template;

    @BeforeAll
    static void setUp() {
        ds = new JDBCDataSource();
        ds.setUrl("jdbc:hsqldb:mem:deletes");
        ds.setUser("sa");
        ds.setPassword("");
        template = new DefaultSQLTemplate(ds);
        template.execute(SQLStatement.sql("create table test ( test_id numeric(10), test varchar(255), name varchar(255))"));
        template.execute(SQLStatement.sql("insert into test values (?, ?, ?)")
                .bind(1L)
                .bind("xx")
                .bind("xxx")
        );
        template.execute(SQLStatement.sql("insert into test values (?, ?, ?)")
                .bind(2L)
                .bind("xx")
                .bind("xxx")
        );
    }

    @Test
    void testGetSQL() {
        var anObject = new AnObject(1L, "xx", "xxx");
        var stmt = new Delete("test")
                .where("test", anObject.getTest())
                .where("name", anObject.getName());
        var sql = stmt.getSql();
        System.out.println(sql);
        var result = template.execute(stmt);
        assertEquals(2, result);

    }
}
