package net.odoframework.sql;

import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.stream.Collectors;

import static net.odoframework.sql.Insert.insertInto;
import static net.odoframework.sql.ResultSetMapper.into;
import static net.odoframework.sql.SQLStatement.sql;
import static net.odoframework.sql.Update.update;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UpdateTest {

    private static JDBCDataSource ds;
    private static DefaultSQLTemplate template;

    @BeforeAll
    static void setUp() {
        ds = new JDBCDataSource();
        ds.setUrl("jdbc:hsqldb:mem:updates");
        ds.setUser("sa");
        ds.setPassword("");
        template = new DefaultSQLTemplate(ds);
        template.execute(SQLStatement.sql("create table test ( test_id numeric(10), test varchar(255), name varchar(255))"));
        template.execute(SQLStatement.sql("insert into test (test_id, test, name) values (?, ?, ?)")
                .bind(1L)
                .bind("xx")
                .bind("xxx")
        );
        template.execute(SQLStatement.sql("insert into test  (test_id, test, name) values (?, ?, ?)")
                .bind(2L)
                .bind("xx")
                .bind("xxx")
        );
    }

    @Test
    void testGetSQL() {
        var stmt = update("test")
                .value("test", "test_val")
                .value("name", "test_name")
                .where("test_id", 1L);

        var result = template.execute(stmt);
        assertEquals(1, result);

        var into = into(AnObject::new)
                .field(1, (it, val) -> it.setId(((BigDecimal) val).longValue()))
                .field(2, (it, val) -> it.setTest(val.toString()))
                .field(3, (it, val) -> it.setName(val.toString()));

        var items = template.stream(sql("select * from test where test_id = ?").bind(1L), into).collect(Collectors.toList());
        assertEquals(1, items.size());
        assertEquals(1L, items.get(0).getId());
        assertEquals("test_val", items.get(0).getTest());
        assertEquals("test_name", items.get(0).getName());

    }
}
