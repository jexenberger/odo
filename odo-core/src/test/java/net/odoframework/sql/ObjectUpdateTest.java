package net.odoframework.sql;

import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ObjectUpdateTest {

    private static JDBCDataSource ds;
    private static DefaultSQLTemplate template;

    @BeforeAll
    static void setUp() {
        ds = new JDBCDataSource();
        ds.setUrl("jdbc:hsqldb:mem:object_updates");
        ds.setUser("sa");
        ds.setPassword("");
        template = new DefaultSQLTemplate(ds);
        template.execute(SQLStatement.sql("create table test ( test_id numeric(10), test varchar(255), name varchar(255))"));
        template.execute(SQLStatement.sql("insert into test (test_id, test, name) values (?, ?, ?)")
                .bind(1L)
                .bind("xx")
                .bind("xxx")
        );
        template.execute(SQLStatement.sql("insert into test  (test_id, test, name) values (?, ?, ?)")
                .bind(2L)
                .bind("xx")
                .bind("xxx")
        );
    }

    @Test
    void testGetSQL() {
        var anObject = new AnObject(1L, "test", "name");
        var stmt = new ObjectUpdate<>("test", anObject)
                .addField("test", AnObject::getTest)
                .addField("name", AnObject::getName)
                .where("test_id", AnObject::getId);
        var sql = stmt.getSql();
        var result = template.execute(stmt);
        assertEquals(1, result);

    }
}
