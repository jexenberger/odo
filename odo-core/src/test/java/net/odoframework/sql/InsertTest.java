package net.odoframework.sql;

import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.stream.Collectors;

import static net.odoframework.sql.Insert.insertInto;
import static net.odoframework.sql.ResultSetMapper.into;
import static net.odoframework.sql.SQLStatement.sql;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class InsertTest {

    private static JDBCDataSource ds;
    private static DefaultSQLTemplate template;

    @BeforeAll
    static void setUp() {
        ds = new JDBCDataSource();
        ds.setUrl("jdbc:hsqldb:mem:inserts");
        ds.setUser("sa");
        ds.setPassword("");
        template = new DefaultSQLTemplate(ds);
        template.execute(sql("create table test_ins ( test_id numeric(10), test varchar(255), name varchar(255))"));
    }

    @Test
    void testGetSQL() {
        var stmt = insertInto("test_ins")
                .value("test_id", 1L)
                .value("test", "test_val")
                .value("name", "test_name");

        var result = template.execute(stmt);
        assertEquals(1, result);

        var into = into(AnObject::new)
                .field(1, (it, val) -> it.setId(((BigDecimal) val).longValue()))
                .field(2, (it, val) -> it.setTest(val.toString()))
                .field(3, (it, val) -> it.setName(val.toString()));

        var items = template.stream(sql("select * from test_ins"), into).collect(Collectors.toList());
        assertEquals(1, items.size());
        assertEquals(1L, items.get(0).getId());
        assertEquals("test_val", items.get(0).getTest());
        assertEquals("test_name", items.get(0).getName());

    }
}
