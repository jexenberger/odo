package net.odoframework.sql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnObject {

    private Long id;
    private String test;
    private String name;

}
