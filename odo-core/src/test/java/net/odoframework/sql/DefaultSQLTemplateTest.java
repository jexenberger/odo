package net.odoframework.sql;

import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class DefaultSQLTemplateTest {

    private static JDBCDataSource ds;
    private static DefaultSQLTemplate template;

    @BeforeAll
    static void setUp() {
        ds = new JDBCDataSource();
        ds.setUrl("jdbc:hsqldb:mem:test");
        ds.setUser("sa");
        ds.setPassword("");
        template = new DefaultSQLTemplate(ds);
        template.execute(SQLStatement.sql("create table test ( test varchar(255))"));
        template.execute(SQLStatement.sql("create table test_ii ( test varchar(255))"));
    }

    @Test
    void execute() {
        var result = template.execute(SQLStatement.sql("insert into test(test) values  'test'"));
        assertEquals(1, result);
    }


    @Test
    void stream() {
        for (int i = 0; i < 100; i++) {
            template.execute(SQLStatement.sql("insert into test_ii(test) values  'test" + i + "'"));
        }
        var output = template
                .stream(SQLStatement.sql("select * from test_ii"), rs -> rs.getString(1))
                .collect(Collectors.toList());

        assertEquals(100, output.size());
        for (String s : output) {
            assertTrue(s.startsWith("test"));
        }

    }

    @AfterEach
    void tearDown() throws Exception {
        template.execute(SQLStatement.sql("delete from test"));
        ds.getConnection().close();
    }

    @Test
    void doInTransaction() {

        template.doInTransaction(() -> {
            template.execute(SQLStatement.sql("insert into test(test) values  'test'"));
            return template.execute(SQLStatement.sql("insert into test(test) values  'test2'"));
        });
    }

    @Test
    void doInTransactionRollback() {
        assertThrows(IllegalStateException.class, () -> template.doInTransaction(() -> {
            template.execute(SQLStatement.sql("insert into test(test) values  'test'"));
            template.execute(SQLStatement.sql("insert into test(test) values  'test2'"));
            throw new IllegalStateException("rollback");
        }));

    }
}
