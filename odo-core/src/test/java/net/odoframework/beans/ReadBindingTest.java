package net.odoframework.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReadBindingTest {

    private BeanProperty<ATest, String> value;
    private BeanProperty<BTest, Integer> variable;
    private ReadBinding<ATest, String, BTest, Integer> binding;

    @BeforeEach
    void setUp() {
        value = new BeanProperty<>(ATest::getA, ATest::setA);
        variable = new BeanProperty<>(BTest::getBValue, BTest::setBValue);
        binding = new ReadBinding<>(value, variable, Integer::valueOf);
    }

    @Test
    void shouldRead() {
        var bTest = new BTest();
        bTest.setBValue(100);
        var aTest = new ATest();
        aTest.setA("2000");

        binding.bind(aTest, bTest);
        assertEquals(2000, bTest.getBValue());
    }


}
