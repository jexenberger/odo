package net.odoframework.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompositeWriteBindingTest {
    private BeanProperty<ATest, String> a;
    private BeanProperty<BTest, Integer> aValue;
    private WriteBinding<ATest, String, BTest, Integer> aBinding;

    private BeanProperty<ATest, String> b;
    private BeanProperty<BTest, Integer> bValue;
    private WriteBinding<ATest, String, BTest, Integer> bBinding;
    private CompositeWriteBinding<ATest, BTest> binding;

    @BeforeEach
    void setUp() {
        a = new BeanProperty<>(ATest::getA, ATest::setA);
        aValue = new BeanProperty<>(BTest::getAValue, BTest::setAValue);
        aBinding = new WriteBinding<>(a, aValue , Integer::valueOf, Object::toString);
        b = new BeanProperty<>(ATest::getB, ATest::setB);
        bValue = new BeanProperty<>(BTest::getBValue, BTest::setBValue);
        bBinding = new WriteBinding<>(b, bValue, Integer::valueOf, Object::toString);
        binding = new CompositeWriteBinding<>();
        binding.add(aBinding).add(bBinding);
    }

    @Test
    void shouldBind() {
        var bTest = new BTest();
        bTest.setBValue(100);

        var aTest = new ATest();
        aTest.setA("2000");
        aTest.setB("9000");

        binding.bind(aTest, bTest);
        assertEquals(2000, bTest.getAValue());
        assertEquals(9000, bTest.getBValue());
    }


    @Test
    void shouldBindBack() {
        var bTest = new BTest();
        bTest.setBValue(100);
        bTest.setAValue(900);

        var aTest = new ATest();
        aTest.setA("2000");
        aTest.setB("9000");

        binding.bind(aTest, bTest);
        binding.bindBack(aTest, bTest);
        assertEquals(2000, bTest.getAValue());
        assertEquals(9000, bTest.getBValue());
    }

}
