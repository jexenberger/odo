package net.odoframework.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeanPropertyTest {

    private BeanProperty<ATest, String> binding;

    @BeforeEach
    void setUp() {
        binding = new BeanProperty<>(ATest::getA, ATest::setA);
    }

    @Test
    void set() {
        var testC = new ATest("qwerty","b");
        assertEquals("qwerty", testC.getA());
        binding.set(testC, "hello world");
        assertEquals("hello world", testC.getA());
    }

    @Test
    void get() {
        var testC = new ATest("qwerty","b");
        assertEquals("qwerty", binding.get(testC));
    }

}
