package net.odoframework.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompositeReadBindingTest {
    private BeanProperty<ATest, String> a;
    private BeanProperty<BTest, Integer> aValue;
    private ReadBinding<ATest, String, BTest, Integer> aBinding;

    private BeanProperty<ATest, String> b;
    private BeanProperty<BTest, Integer> bValue;
    private ReadBinding<ATest, String, BTest, Integer> bBinding;
    private CompositeReadBinding<Object, Object> binding;

    @BeforeEach
    void setUp() {
        a = new BeanProperty<>(ATest::getA, ATest::setA);
        aValue = new BeanProperty<>(BTest::getAValue, BTest::setAValue);
        aBinding = new ReadBinding<>(a, aValue , Integer::valueOf);
        b = new BeanProperty<>(ATest::getB, ATest::setB);
        bValue = new BeanProperty<>(BTest::getBValue, BTest::setBValue);
        bBinding = new ReadBinding<>(b, bValue, Integer::valueOf);
        binding = new CompositeReadBinding<>();
        binding.add(aBinding).add(bBinding);
    }

    @Test
    void shouldRead() {
        var bTest = new BTest();
        bTest.setBValue(100);

        var aTest = new ATest();
        aTest.setA("2000");
        aTest.setB("9000");

        binding.bind(aTest, bTest);
        assertEquals(2000, bTest.getAValue());
        assertEquals(9000, bTest.getBValue());
    }
}
