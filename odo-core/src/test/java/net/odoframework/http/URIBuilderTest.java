package net.odoframework.http;

import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class URIBuilderTest {


    @Test
    public void urlQuery() throws Exception {
        var url = URIBuilder
                .uri("www.google.com")
                .path("search")
                .queryParam("q", "test")
                .toURL();

        assertEquals(new URL("http://www.google.com/search?q=test"), url);
    }

    @Test
    public void basicUrl() throws Exception {
        var url = URIBuilder
                .uri("www.google.com")
                .path("search")
                .toURL();

        assertEquals(new URL("http://www.google.com/search"), url);
    }

    @Test
    public void basicUrlWithPort() throws Exception {
        var url = URIBuilder
                .uri("www.google.com")
                .port(8080)
                .path("search")
                .toURL();

        assertEquals(new URL("http://www.google.com:8080/search"), url);
    }

    @Test
    public void basicUrlWithProtocol() throws Exception {
        var url = URIBuilder
                .uri("www.google.com")
                .protocol("https")
                .path("search")
                .toURL();

        assertEquals(new URL("https://www.google.com/search"), url);
    }

    @Test
    public void basicUrlWithUser() throws Exception {
        var url = URIBuilder
                .uri("www.google.com")
                .user("user")
                .path("search")
                .toURL();

        assertEquals(new URL("http://user@www.google.com/search"), url);
    }

    @Test
    public void basicUrlWithUserAndPassword() throws Exception {
        var url = URIBuilder
                .uri("www.google.com")
                .user("user")
                .password("password")
                .path("search")
                .toURL();

        assertEquals(new URL("http://user:password@www.google.com/search"), url);
    }

    @Test
    public void basicUrlWithProtocolNoPath() throws Exception {
        var url = URIBuilder
                .uri("www.google.com")
                .protocol("https")
                .toURL();

        assertEquals(new URL("https://www.google.com"), url);
    }
}
