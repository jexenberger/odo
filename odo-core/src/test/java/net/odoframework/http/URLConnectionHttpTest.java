package net.odoframework.http;

import org.junit.jupiter.api.Test;

import static net.odoframework.http.HttpRequest.get;
import static net.odoframework.http.URIBuilder.uri;
import static org.junit.jupiter.api.Assertions.*;

public class URLConnectionHttpTest {

    @Test
    void execute() {
        var service = new URLConnectionHttpService();
        var builder = uri("https", "restcountries.com")
                .path("/v2/regionalbloc/eu");
        var request = get(builder.toURI());
        var result = service.execute(request);
        assertTrue(result.isLeft());
        assertEquals(200, result.left().getCode());
        var body = result.left().bodyAsString();
        assertTrue(body.isPresent());
    }

    @Test
    void executeFailure() {
        var service = new URLConnectionHttpService();
        var builder = uri("https", "restcountries.com")
                .path("/v2/xxx/xxx");
        var request = get(builder.toURI());
        var result = service.execute(request);
        assertFalse(result.isLeft());
        var right = result.right();
        System.out.println(right);
        assertEquals(404, right.getCode());
        var body = right.bodyAsString();
        var message = right.getMessage();

        assertTrue(body.isPresent());
        System.out.println(body);
        System.out.println(right);
    }
}
