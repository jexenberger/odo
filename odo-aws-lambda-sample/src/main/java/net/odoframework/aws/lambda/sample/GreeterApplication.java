package net.odoframework.aws.lambda.sample;

import net.odoframework.container.Application;
import net.odoframework.service.ServiceFunction;

public class GreeterApplication extends Application {


    @Override
    public void build() {
        provides(Greeting.class).with(it -> new Greeting(it.value("greeting")));
        provides(Greeter.class).with(it -> new Greeter(it.references(Greeting.class)));
        provides(ServiceFunction.NAME).with(it -> new LambdaRuntimeFunction(it.references(Greeter.class)));
    }
}
