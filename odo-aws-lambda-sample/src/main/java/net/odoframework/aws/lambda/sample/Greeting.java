package net.odoframework.aws.lambda.sample;


public class Greeting {

    private final String salutation;

    public Greeting(String salutation) {
        this.salutation = salutation;
    }

    public String getSalutation() {
        return salutation;
    }
}
