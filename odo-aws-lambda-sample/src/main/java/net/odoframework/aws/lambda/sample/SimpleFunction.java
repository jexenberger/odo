package net.odoframework.aws.lambda.sample;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.Map;

public class SimpleFunction implements RequestHandler<Map<String, Object>, Map<String, Object>> {

    private static final GreeterApplication APP = new GreeterApplication();
    private final Greeter greeter;

    public SimpleFunction() {
        greeter = APP.getContainer().resolve(Greeter.class).orElseThrow();
    }

    @Override
    public Map<String, Object> handleRequest(Map<String, Object> input, Context context) {
        var result =  greeter.greet(input);
        context.getLogger().log(result.toString());
        return result;
    }

}
