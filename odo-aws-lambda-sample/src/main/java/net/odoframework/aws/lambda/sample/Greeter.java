package net.odoframework.aws.lambda.sample;

import net.odoframework.container.util.Json;
import net.odoframework.util.Strings;

import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class Greeter {

    private final Greeting greeting;


    public Greeter(Greeting p) {
        this.greeting = Objects.requireNonNull(p);
    }

    public Map<String, Object> greet(Map<String, Object> name) {
        var theName = name.get("name");
        var result = greeting.getSalutation() + " " + theName;
        return Map.of("greeting", result);
    }

}
