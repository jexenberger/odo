package net.odoframework.aws.lambda.sample;

import net.odoframework.service.InvocationContext;

import java.util.Map;
import java.util.function.BiFunction;

public class LambdaRuntimeFunction implements BiFunction<Map<String, Object>, String, Map<String, Object>> {

    private final Greeter greeter;

    public LambdaRuntimeFunction(Greeter greeter) {
        this.greeter = greeter;
    }

    @Override
    public Map<String,Object> apply(Map<String, Object> payload, String context) {
        return greeter.greet(payload);
    }
}
